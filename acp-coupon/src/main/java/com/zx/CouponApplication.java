package com.zx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class CouponApplication
{
    public static void main( String[] args )
    {
        SpringApplication.run(CouponApplication.class,args);
    }
}
