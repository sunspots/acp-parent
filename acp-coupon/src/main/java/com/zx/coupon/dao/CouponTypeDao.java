package com.zx.coupon.dao;

import com.zx.coupon.entity.CouponTypeEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CouponTypeDao {
    List<CouponTypeEntity> findAll();
}
