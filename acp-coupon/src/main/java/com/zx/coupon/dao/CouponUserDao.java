package com.zx.coupon.dao;

import com.zx.coupon.entity.CouponUserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author xz
 * @email sunlightcs@gmail.com
 * @date 2020-09-17 16:20:16
 */
@Mapper
public interface CouponUserDao extends BaseMapper<CouponUserEntity> {
	
}
