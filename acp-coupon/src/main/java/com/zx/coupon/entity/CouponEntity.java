package com.zx.coupon.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author xz
 * @email sunlightcs@gmail.com
 * @date 2020-09-17 16:20:16
 */
@Data
@TableName("acp_coupon")
public class CouponEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 优惠券id
	 */
	@TableId
	private Integer couponId;
	/**
	 * 优惠卷类型（1->全场赠券；2->购物赠券；3->注册赠券）
	 */
	private Integer couponType;
	/**
	 * 优惠券图片
	 */
	private String couponImg;
	/**
	 * 优惠券名字
	 */
	private String couponName;
	/**
	 * 优惠券数量
	 */
	private Integer couponNum;
	/**
	 * 金额
	 */
	private BigDecimal couponAmount;
	/**
	 * 每人限领张数
	 */
	private Integer couponPerLimit;
	/**
	 * 使用门槛
	 */
	private BigDecimal couponMinPoint;
	/**
	 * 开始时间
	 */
	private Date couponStartTime;
	/**
	 * 结束时间
	 */
	private Date couponEndTime;
	/**
	 * 适用类型（0->全场通用；1->指定类型；2->指定商品）
	 */
	private Integer couponUseType;
	/**
	 * 备注
	 */
	private String couponNote;
	/**
	 * 发行数量
	 */
	private Integer couponPublishCount;
	/**
	 * 已使用数量
	 */
	private Integer couponUseCount;
	/**
	 * 领取数量
	 */
	private Integer couponReceiveCount;
	/**
	 * 可以领取的开始日期
	 */
	private Date enableStartTime;
	/**
	 * 可以领取的结束日期
	 */
	private Date enableEndTime;
	/**
	 * 优惠码
	 */
	private String couponCode;
	/**
	 * 发布状态（0->未发布，1->已发布）
	 */
	private Integer couponPublish;
	/**
	 * 状态（0->未删除；1->删除）
	 */
	private Integer status;
	/**
	 * 新增时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 预留字段1
	 */
	private String text1;
	/**
	 * 预留字段2
	 */
	private String text2;
	/**
	 * 预留字段3
	 */
	private String text3;

}
