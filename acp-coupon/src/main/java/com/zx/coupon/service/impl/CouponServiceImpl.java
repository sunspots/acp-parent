package com.zx.coupon.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zx.coupon.common.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


import com.zx.coupon.dao.CouponDao;
import com.zx.coupon.entity.CouponEntity;
import com.zx.coupon.service.CouponService;


@Service("couponService")
public class CouponServiceImpl implements CouponService {
    @Autowired
    private CouponDao couponDao;


    @Override
    public IPage<CouponEntity> getPage(Page<CouponEntity> page, QueryWrapper queryWrapper) {
        IPage iPage = couponDao.getPage(page,queryWrapper);
        return iPage;
    }

    @Override
    public CouponEntity getById(Integer couponId) {
        return couponDao.getById(couponId);
    }

    @Override
    public Integer save(CouponEntity coupon) {
        Integer i = couponDao.save(coupon);
        return i;
    }

    @Override
    public Integer updateById(CouponEntity coupon) {
        Integer i = couponDao.update(coupon);
        return i;
    }

    @Override
    public Integer removeByIds(Integer couponId) {
        Integer i = couponDao.del(couponId);
        return i;
    }

}