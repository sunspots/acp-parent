package com.zx.coupon.service.impl;

import com.zx.coupon.common.utils.PageUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.zx.coupon.dao.CouponUserDao;
import com.zx.coupon.entity.CouponUserEntity;
import com.zx.coupon.service.CouponUserService;


@Service("couponUserService")
public class CouponUserServiceImpl extends ServiceImpl<CouponUserDao, CouponUserEntity> implements CouponUserService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        return null;
    }

}