package com.zx.coupon.controller;

import java.util.*;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zx.coupon.common.utils.CommonResult;
import com.zx.coupon.utils.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import com.zx.coupon.entity.CouponEntity;
import com.zx.coupon.service.CouponService;


/**
 * 
 *
 * @author xz
 * @email sunlightcs@gmail.com
 * @date 2020-09-17 16:20:16
 */
@RestController
@RequestMapping("/coupon")
public class CouponController {
    @Autowired
    private CouponService couponService;

    /**
     * 查询全部信息
     * @return
     */
    @GetMapping("/list")
    public PageResult list(){
        Page<CouponEntity> page1 = new Page<>(1,10);
        IPage<CouponEntity> page2 = couponService.getPage(page1,null);
//        List<CouponEntity> list = page2.getRecords();
        return PageResult.<CouponEntity>builder().data(page2.getRecords()).code(0).count(page2.getTotal()).build();
    }

    /**
     * 模糊查询
     */
    @PostMapping("/someList")
    public CommonResult some(@RequestBody CouponEntity couponEntity,Model model){
        Page<CouponEntity> page = new Page<>(1,10);
        return CommonResult.ok();
    }


    /**
     * 根据id查询信息
     */
    @GetMapping("/info/{couponId}")
    public CouponEntity info(@PathVariable("couponId") Integer couponId){
		CouponEntity coupon = couponService.getById(couponId);
        return coupon;
    }

    /**
     * 新增
     */
    @PostMapping("/SaveOrUpdate")
    public CommonResult save(@RequestBody CouponEntity coupon){
        if (StringUtils.isEmpty(coupon.getCouponId())){
            Integer i = couponService.save(coupon);
            HashMap map = new HashMap<>();
            if (i!=0){
                map.put("msg","新增成功");
                map.put("data",i);
                map.put("code",0);
            }else {
                map.put("msg","新增失败");
                map.put("code",1);
            }
            return CommonResult.ok(map);
        }else {
            Integer i = couponService.updateById(coupon);
            HashMap map = new HashMap<>();
            if (i!=0){
                map.put("msg","修改成功");
                map.put("data",i);
                map.put("code",0);
            }else {
                map.put("msg","修改失败");
                map.put("code",1);
            }
            return CommonResult.ok(map);
        }

    }

    /**
     * 修改
     */
//    @PutMapping("/update")
//    public CommonResult update(@RequestBody CouponEntity coupon){
//		Integer i = couponService.updateById(coupon);
//        HashMap map = new HashMap<>();
//        if (i!=0){
//            map.put("msg","修改成功");
//            map.put("data",i);
//            map.put("code",200);
//        }else {
//            map.put("msg","修改失败");
//            map.put("code",500);
//        }
//        return CommonResult.ok(map);
//    }

    /**
     * 删除
     */
    @DeleteMapping("/delete/{couponId}")
    public CommonResult delete(@PathVariable("couponId") Integer couponId){
		Integer i = couponService.removeByIds(couponId);
        HashMap map = new HashMap<>();
        if (i!=0){
            map.put("msg","删除成功");
            map.put("data",i);
            map.put("code",200);
        }else {
            map.put("msg","删除失败");
            map.put("code",500);
        }
        return CommonResult.ok(map);
    }
}
