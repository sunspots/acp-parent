package com.zx.coupon.controller;

import java.util.Arrays;
import java.util.Map;

import com.zx.coupon.common.utils.CommonResult;
import com.zx.coupon.common.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zx.coupon.entity.CouponUserEntity;
import com.zx.coupon.service.CouponUserService;




/**
 *
 *
 * @author xz
 * @email sunlightcs@gmail.com
 * @date 2020-09-17 16:20:16
 */
@RestController
@RequestMapping("coupon/couponuser")
public class CouponUserController {
    @Autowired
    private CouponUserService couponUserService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = couponUserService.queryPage(params);

        return CommonResult.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public CommonResult info(@PathVariable("id") Long id){
		CouponUserEntity couponUser = couponUserService.getById(id);

        return CommonResult.ok().put("couponUser", couponUser);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public CommonResult save(@RequestBody CouponUserEntity couponUser){
		couponUserService.save(couponUser);

        return CommonResult.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public CommonResult update(@RequestBody CouponUserEntity couponUser){
		couponUserService.updateById(couponUser);
        return CommonResult.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public CommonResult delete(@RequestBody Long[] ids){
		couponUserService.removeByIds(Arrays.asList(ids));
        return CommonResult.ok();
    }

}
