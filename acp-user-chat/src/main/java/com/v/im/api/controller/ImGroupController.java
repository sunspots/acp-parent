package com.v.im.api.controller;

import com.v.im.common.utils.CommonResult;
import com.v.im.user.entity.ImGroup;
import com.v.im.user.entity.ImUser;
import com.v.im.user.service.IImGroupService;
import com.v.im.user.service.IImUserFriendService;
import com.v.im.user.service.IImUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/group")
@RefreshScope
public class ImGroupController {

    private final Logger logger = LoggerFactory.getLogger(ImUserController.class);

    @Autowired
    @Qualifier(value = "imGroupService")
    private IImGroupService iImGroupService;
    @Autowired
    @Qualifier(value = "imUserService")
    private IImUserService imUserService;

    @Resource
    @Qualifier(value = "imUserFriendService")
    private IImUserFriendService imUserFriendService;

//    /**
//     * 初始化用户分组
//     **/
//    @PostMapping("/initGroup")
//    public CommonResult installGroup(ImUser imUser){
//        imUserService.initGroup(imUser);
//        return CommonResult.success();
//    }

    /**
     * 获取好友分组
     */
    @PostMapping("/getGroup")
    public CommonResult getGroup(String id) {
        return iImGroupService.getGroup(id);
    }


    /**
     * 添加好友分组
     **/
//    @PostMapping("/addGroup")
//    public int addGroup(ImGroup imGroup) {
//        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
//        ImUser user = imUserService.getByLoginName(username);
//        return iImGroupService.addGroup(imGroup, user.getId());
//    }
    @PostMapping("/addGroup")
    public CommonResult addGroup(@RequestParam String name,@RequestParam String userName, String[] userList) {
        ImGroup imGroup = new ImGroup();
        if (userList.length > 0) {
            imGroup.setUserList(new ArrayList<>());
            for (int i = 0; i < userList.length; i++) {
                ImUser imUser = new ImUser();
                imUser.setMobile(userList[i]);
                imGroup.getUserList().add(imUser);
            }
        }
        imGroup.setName(name);
        imGroup.setUserName(userName);

        return CommonResult.success(iImGroupService.addGroup(imGroup));
    }

    /**
     * 删除好友分组
     */
    @PostMapping("/delGroup")
    public CommonResult delGroup(String groupId, String userId) {
        //获取好友列表
        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        ImUser user = imUserService.getByLoginName(username);
        //imUserFriendService.getUserFriends(user.getId());
        //修改friend_group_id
        //iImGroupService.updateFriendGroupId(userId);
        //删除分组
        int i=iImGroupService.delGroup(groupId,user.getDefaultGroupId(),user.getId());
        return CommonResult.success(i,"删除成功");
    }


    /**
     * 修改好友分组
     **/
    @PostMapping("/updateGroup")
    public Object[] updateGroup(ImGroup imGroup) {
        Object[] obj = {1, 2};
        //获取好友信息
        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        ImUser user = imUserService.getByLoginName(username);
        imUserFriendService.getUserFriends(user.getId());
        obj[1] = iImGroupService.updateGroup(imGroup.getId(), imGroup.getName(), user.getId());
        obj[0] = imUserFriendService.getUserFriends(user.getId());
        return obj;
    }

    @PostMapping("/getMembers")
    public CommonResult getMembers(String id,String userId){
        return CommonResult.success(imUserFriendService.getMembers(id,userId));
    }
}
