package com.v.im.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.v.im.common.utils.CommonResult;
import com.v.im.user.entity.ImChatGroup;
import com.v.im.user.mapper.ImChatGroupMapper;
import com.v.im.user.mapper.ImChatGroupUserMapper;
import com.v.im.user.service.IImChatGroupService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 群 服务实现类
 * </p>
 *
 * @author 乐天
 * @since 2018-10-28
 */
@Service
@Qualifier("imChatGroupServiceImpl")
public class ImChatGroupServiceImpl extends ServiceImpl<ImChatGroupMapper, ImChatGroup> implements IImChatGroupService {
    @Resource
    private ImChatGroupMapper imChatGroupMapper;

    @Resource
    private ImChatGroupUserMapper imChatGroupUserMapper;

    /**
     * 创建群聊
     * @param imChatGroup
     * @return
     */
    @Override
    public int addChatGroup(ImChatGroup imChatGroup) {
        imChatGroup.setAvatar("/img/group-img.png");
        return imChatGroupMapper.insert(imChatGroup);
    }

    /**
     * 删除群聊
     * @param id
     * @return
     */
    @Override
    public CommonResult delChatGroup(String id) {
        imChatGroupMapper.delChatGroup(id);
        return CommonResult.success(id);
    }

    /**
     * 修改群组信息
      * @param imChatGroup
     * @return
     */
    @Override
    public CommonResult updateChatGroup(ImChatGroup imChatGroup) {
        imChatGroupMapper.updateChatGroup(imChatGroup);
        return CommonResult.success(imChatGroup);
    }

    /**
     * 获取群组信息
     * @param id
     * @return
     */
    @Override
    public List<ImChatGroup> searchChatGroup(String id) {
        List<ImChatGroup> imChatGroups=new ArrayList<>();
        ImChatGroup imChatGroup = imChatGroupMapper.selectById(id);
        imChatGroups.add(imChatGroup);
        return imChatGroups;
    }

    /**
     * 退出群聊
     * @param userId
     * @param isHost
     * @param chatId
     * @return
     */
    @Override
    public boolean exitGroupChat(String chatId, boolean isHost, String userId) {
        if(isHost){
            imChatGroupMapper.deleteById(chatId);
            imChatGroupUserMapper.deleteById(chatId);
        }else{
            Integer delete = imChatGroupUserMapper.deleteByUserId(userId, chatId);
        }
        return isHost;
    }

    /**
     * 加入群聊
     * @param chatGroupId
     * @param userId
     * @return
     */
    @Override
    public int enterChatGroup(String chatGroupId,String userId) {
        Integer peopleNum = imChatGroupMapper.selectById(chatGroupId).getPeopleNum();
       Integer i=imChatGroupUserMapper.selectByChatGroupId(chatGroupId);
        Date time=new Date();
        if((imChatGroupMapper.checkUserChatGroupState(chatGroupId,userId)).size()>0){
            return -1;
        }
        if(i>=peopleNum){
            return -1;
        }
        return imChatGroupMapper.enterChatGroup(chatGroupId,userId,time);
    }


}
