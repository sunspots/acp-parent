package com.v.im.user.service.impl;

import cn.hutool.core.img.Img;
import com.v.im.common.utils.CommonResult;
import com.v.im.user.entity.ImGroup;
import com.v.im.user.entity.ImUser;
import com.v.im.user.entity.ImUserFriend;
import com.v.im.user.mapper.ImGroupMapper;
import com.v.im.user.mapper.ImUserFriendMapper;
import com.v.im.user.mapper.ImUserMapper;
import com.v.im.user.service.IImUserFriendService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.lang.model.element.VariableElement;
import java.util.List;
import java.util.stream.Stream;

/**
 * <p>
 * 用户关系表
 * </p>
 *
 * @author jobob
 * @since 2018-12-31
 */
@Service
@Qualifier(value = "imUserFriendService")
public class ImUserFriendServiceImpl extends ServiceImpl<ImUserFriendMapper, ImUserFriend> implements IImUserFriendService {
    @Resource
    private ImUserFriendMapper imUserFriendMapper;

    @Resource
    private ImUserMapper imUserMapper;

    @Resource
    private ImGroupMapper imGroupMapper;

    /**
     * 根据用户的ID 获取 用户好友(双向用户关系)
     *
     * @param userId 用户ID
     * @return 好友分组的列表
     */
    public List<ImGroup> getUserFriends(String userId) {

        return this.baseMapper.getUserFriends(userId);
    }


    @Override
    public CommonResult addFriends(ImUserFriend imUserFriend) {

        String friendId = imUserFriend.getFriendName();
        String friendGroupId = imUserFriend.getFriendGroupId();
        if (imUserFriendMapper.checkRelation(imUserFriend.getUserName(), friendId) >= 1) {
            return CommonResult.success("您已经和该用户互为好友，不需在添加");
        }
        ImUser newFriends = imUserMapper.selectByLoginName(friendId);
        if (friendGroupId == null || friendGroupId == "" || friendGroupId == "-1") {
            imUserFriend.setFriendGroupId(newFriends.getDefaultGroupId());
        }
        int i = imUserFriendMapper.addFriends(imUserFriend);
//        String userId = imUserFriend.getUserId();
//        String imUserFriendFriendId = imUserFriend.getFriendId();
//        imUserFriend.setUserId(imUserFriendFriendId);
//        imUserFriend.setFriendId(userId);
//        String userGroupId = imUserFriend.getUserGroupId();
//        imUserFriend.setFriendGroupId(userGroupId);
//        imUserFriend.setUserGroupId(null);
//        int j=imUserFriendMapper.addFriends(imUserFriend);
        return CommonResult.success(i);
    }

    @Override
    public CommonResult delFriends(String userId) {
//        ImUserFriend imUserFriend=new ImUserFriend();
//        imUserFriend.setUserId(userId);
        imUserFriendMapper.delFriends(userId);
        return CommonResult.success(userId);
    }

    @Override
    public List<ImGroup> getFriendGroup(String id) {
        List<ImGroup> imGroups = imGroupMapper.selectByUserId(id);

        imGroups.forEach(imGroup -> {
            ImUserFriend imUserFriend = new ImUserFriend();
            imUserFriend.setUserName(imGroup.getUserName());
            imUserFriend.setUserGroupId(imGroup.getId());
            imGroup.setUserList(imUserFriendMapper.getMembers(imUserFriend));

        });

        return imGroups;
    }

    @Override
    public List<ImUser> getMembers(String id, String userId) {
        ImUserFriend imUserFriend = new ImUserFriend();
        imUserFriend.setUserGroupId(id);
        imUserFriend.setUserName(userId);
        return imUserFriendMapper.getMembers(imUserFriend);
    }
}
