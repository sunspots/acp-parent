package com.v.im.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.v.im.common.utils.CommonResult;
import com.v.im.user.entity.ImGroup;
import com.v.im.user.entity.ImUser;
import com.v.im.user.entity.ImUserFriend;
import com.v.im.user.mapper.ImGroupMapper;
import com.v.im.user.mapper.ImUserFriendMapper;
import com.v.im.user.mapper.ImUserMapper;
import com.v.im.user.service.IImGroupService;
import com.v.im.user.service.IImUserFriendService;
import com.v.im.user.service.IImUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 乐天
 * @since 2018-10-23
 */
@Service
@Qualifier("imGroupService")
public class ImGroupServiceImpl extends ServiceImpl<ImGroupMapper, ImGroup> implements IImGroupService {

    @Resource
    private ImUserFriendMapper iImUserFriendMapper;

    @Resource
    private ImGroupMapper imGroupMapper;

    @Autowired
    private ImUserMapper imUserMapper;

    @Resource
    @Qualifier(value = "imUserService")
    private IImUserService imUserService;

    @Resource
    @Qualifier(value = "imUserFriendService")
    private IImUserFriendService imUserFriendService;

    @Override
    public CommonResult getGroup(String id) {
        return imGroupMapper.getGroup(id);
    }

    //删除好友分组
    @Override
    public int delGroup(String groupId, String id,String s) {
        int i=imGroupMapper.breakGroup(groupId,id);
        return imGroupMapper.delGroup(groupId,s);
    }

    //新增好友分组
    @Override
    public int addGroup(ImGroup imGroup) {
        int i=0;
        if(imGroupMapper.selectByName(imGroup)==null||imGroupMapper.selectByName(imGroup).size()==0){
            i = imGroupMapper.insert(imGroup);
            if(i==0){
                return i;
            }
            String id=imGroupMapper.selectByName(imGroup).get(0).getId();
            List<ImUser> userList = imGroup.getUserList();
            if (userList != null && userList.size() > 0) {
                ImUserFriend imUserFriend=new ImUserFriend();
                imUserFriend.preInsert();
                for (int j = 0; j < userList.size(); j++) {
                    List<ImUser> imUsers = imUserMapper.SearchUsers(null,userList.get(j).getMobile());
                    imUserFriend.setUserName(imUsers.get(0).getId());
                    imUserFriend.setFriendName(imGroup.getUserName());
                    imUserFriend.setFriendGroupId(id);
                    imUserFriend.setUserGroupId(id);
                    iImUserFriendMapper.updateGroupForFriend(imUserFriend);
//                    imUserFriendMapper.updateById(imUsers.get(0));
                }
            }
        }


        return i;
    }

//    @Override
//    public int addGroup(ImGroup imGroup,String id) {
//
//        if (imGroup.getUserList()==null||imGroup.getUserList().size()==0) {
//
//        } else {
//        }
//        return imGroupMapper.addGroup(imGroup,id);
//    }

    @Override
    public int updateGroup(String id, String name, String userId) {

        return imGroupMapper.updateGroup(id, name, userId);
    }

    @Override
    public CommonResult updateFriendGroupId(String userId) {
        return imGroupMapper.updateFriendGroupId(userId);
    }


}
