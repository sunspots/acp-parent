package com.v.im.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.v.im.common.utils.CommonResult;
import com.v.im.user.entity.ImChatGroup;
import com.v.im.user.entity.ImChatGroupUser;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 群 Mapper 接口
 * </p>
 *
 * @author 乐天
 * @since 2018-10-28
 */
public interface ImChatGroupMapper extends BaseMapper<ImChatGroup> {
    //创建群聊
    int  addChatGroup(ImChatGroup imChatGroup);

    //删除群聊
    CommonResult delChatGroup(String id);

    //修改群聊信息
    CommonResult updateChatGroup(ImChatGroup imChatGroup);

    //查询群聊信息
    ImChatGroup searchChatGroup(String id);

    //退出群聊
    Integer exitGroupChat(String userId);

    //加入群聊
    int enterChatGroup(@Param("chatGroupId") String chatGroupId, @Param("userId") String userId, @Param("time") Date time);

    List<ImChatGroupUser> checkUserChatGroupState(@Param("chatGroupId") String chatGroupId, @Param("userId") String userId);
}
