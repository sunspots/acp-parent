package com.v.im.conf;

import com.netflix.client.config.IClientConfig;
import com.netflix.client.config.IClientConfigKey;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class IRibbon implements IClientConfig {
    @Bean
    public IRule ribbonRule(IClientConfig config) {
        // 随机算法
        return new RandomRule();
    }

    @Override
    public String getClientName() {
        return null;
    }

    @Override
    public String getNameSpace() {
        return null;
    }

    @Override
    public void loadProperties(String s) {

    }

    @Override
    public void loadDefaultValues() {

    }

    @Override
    public Map<String, Object> getProperties() {
        return null;
    }

    @Override
    public void setProperty(IClientConfigKey iClientConfigKey, Object o) {

    }

    @Override
    public Object getProperty(IClientConfigKey iClientConfigKey) {
        return null;
    }

    @Override
    public Object getProperty(IClientConfigKey iClientConfigKey, Object o) {
        return null;
    }

    @Override
    public boolean containsProperty(IClientConfigKey iClientConfigKey) {
        return false;
    }

    @Override
    public String resolveDeploymentContextbasedVipAddresses() {
        return null;
    }

    @Override
    public int getPropertyAsInteger(IClientConfigKey iClientConfigKey, int i) {
        return 0;
    }

    @Override
    public String getPropertyAsString(IClientConfigKey iClientConfigKey, String s) {
        return null;
    }

    @Override
    public boolean getPropertyAsBoolean(IClientConfigKey iClientConfigKey, boolean b) {
        return false;
    }

    @Override
    public <T> T get(IClientConfigKey<T> iClientConfigKey) {
        return null;
    }

    @Override
    public <T> T get(IClientConfigKey<T> iClientConfigKey, T t) {
        return null;
    }

    @Override
    public <T> IClientConfig set(IClientConfigKey<T> iClientConfigKey, T t) {
        return null;
    }
}
