package com.zx.thirdparty.common.alibaba;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.zx.thirdparty.config.AliConfig;
import org.springframework.stereotype.Component;

@Component
public class AliSMS {

    DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", AliConfig.accessKeyId, AliConfig.accessSecret);

    /**
     * 发送六位数随机验证码
     * @param mobile    手机号
     * @param code    随机数
     */
    public void sendSms(String mobile,String code) {

        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", mobile);
        request.putQueryParameter("SignName", "智能活动平台");
        request.putQueryParameter("TemplateCode", "SMS_203191313");
        request.putQueryParameter("TemplateParam", "{\"code\":\"" + code + "\"}");
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }
}
