package com.zx.thirdparty.common.baidu;

import com.baidu.aip.ocr.AipOcr;
import com.zx.thirdparty.config.BaiDuConfig;
import com.zx.thirdparty.utils.FileUtil;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;

@Component
public class BaiDuOCR {

    static AipOcr client = new AipOcr(BaiDuConfig.APP_ID, BaiDuConfig.API_KEY, BaiDuConfig.SECRET_KEY);

    /**
     * 身份证识别
     * multipartFile 传入的身份证
     * idCardSide 身份证正反面 front 正面 ， back  反面
     * @throws IOException
     */
    public JSONObject idCard(MultipartFile multipartFile, String idCardSide) throws IOException {
        /**
         * 传入可选参数调用接口
         *  detect_direction：是否检测图像朝向，默认不检测
         *  detect_risk：是否开启身份证风险类型(身份证复印件、临时身份证、身份证翻拍、修改过的身份证)功能，默认不开启
         */
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("detect_direction", "true");
        options.put("detect_risk", "false");
        byte[] file = FileUtil.readFileByBytes(multipartFile);
        try {
            JSONObject idcard = client.idcard(file, idCardSide, options);
            return idcard;
        }catch (Exception e) {
            System.out.println("超时");
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 银行卡识别 multipartFile 传入的银行卡
     *
     * @param multipartFile
     * @return
     */
    public JSONObject bankCard(MultipartFile multipartFile) throws IOException {
        // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<String, String>();
        byte[] file = FileUtil.readFileByBytes(multipartFile);
        return client.bankcard(file, options);
    }


    /**
     * 营业执照识别
     * @param multipartFile
     * @return
     * @throws IOException
     */
    public JSONObject businessLicense(MultipartFile multipartFile) throws IOException {
        // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<String, String>();
        byte[] file = FileUtil.readFileByBytes(multipartFile);
        return client.businessLicense(file, options);
    }

}
