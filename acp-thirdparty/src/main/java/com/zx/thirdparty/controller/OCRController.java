package com.zx.thirdparty.controller;

import com.zx.thirdparty.common.baidu.BaiDuOCR;
import com.zx.thirdparty.common.result.Result;
import com.zx.thirdparty.common.result.ResultCode;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/ocr")
public class OCRController {

    @Autowired
    private BaiDuOCR baiDuOCR;

    /**
     * 身份证识别
     * @param file 身份证
     * @param idCardSide front 正面 back 反面
     * @return
     * @throws IOException
     */
    @PostMapping("/idCard")
    public Result idCard(@RequestPart(value = "file", required = false) MultipartFile file,
                         @RequestParam("idCardSide")String idCardSide) throws IOException {
        JSONObject idCard = baiDuOCR.idCard(file, idCardSide);
        return new Result(ResultCode.SUCCESS,"获取成功",idCard.toString());
    }

    /**
     * 银行卡识别
     * @param file 银行卡
     * @return
     * @throws IOException
     */
    @PostMapping("/bankCard")
    public Result bankCard(@RequestParam(value = "file", required = false) MultipartFile file) throws IOException {
        JSONObject bankCard = baiDuOCR.bankCard(file);
        return new Result(ResultCode.SUCCESS,"获取成功",bankCard.toString());
    }

    /**
     * 营业执照识别
     * @param file 营业执照
     * @return
     * @throws IOException
     */
    @PostMapping("/businessLicense")
    public Result businessLicense(@RequestParam(value = "file", required = false) MultipartFile file) throws IOException {
        JSONObject businessLicense = baiDuOCR.businessLicense(file);
        return new Result(ResultCode.SUCCESS,"获取成功",businessLicense.toString());
    }
}
