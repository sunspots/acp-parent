package com.zx.ad.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zx.ad.entity.ComboEntity;
import com.zx.ad.vo.ComboSearchEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author lilei
 * @email mrstupide@163.com
 * @date 2020-09-17 14:27:02
 */
@Mapper
public interface ComboDao {

    IPage<ComboEntity> queryPage(Page<ComboEntity> page, @Param("title")String title, @Param("details")String details
            , @Param("priceMin")Double priceMin, @Param("priceMax")Double priceMax);

    void save(ComboEntity combo);

    void updateById(ComboEntity combo);

    void removeByIds(List<Long> asList);
}
