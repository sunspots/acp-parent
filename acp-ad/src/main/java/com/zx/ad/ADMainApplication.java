package com.zx.ad;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.zx.ad.dao")
public class ADMainApplication {
    public static void main(String[] args) {
        SpringApplication.run(ADMainApplication.class, args);
    }
}
