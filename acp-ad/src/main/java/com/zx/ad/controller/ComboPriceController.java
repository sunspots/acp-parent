package com.zx.ad.controller;

import java.util.Arrays;
import java.util.Map;

import com.zx.ad.entity.ComboPriceEntity;
import com.zx.ad.service.ComboPriceService;
import com.zx.ad.utils.common.CommonResult;
import com.zx.ad.utils.common.PageResult;
import com.zx.ad.utils.common.PageUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * 
 *
 * @author lilei
 * @email mrstupide@163.com
 * @date 2020-09-17 14:27:02
 */
@RestController
@Api(tags = "套餐价格模块api")
@Slf4j
@RequestMapping("ad/comboPrice")
public class ComboPriceController {
    @Autowired
    private ComboPriceService comboPriceService;

    /**
     * 列表
     */
    @RequestMapping("/findAll")
    @ApiOperation(value = "查询所有套餐价格")
    public PageResult<ComboPriceEntity> findAll(){
        return comboPriceService.findAll();
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    @ApiOperation(value = "查询所有套餐价格")
    // @RequiresPermissions("ad:comboprice:list")
    public CommonResult list(@RequestParam Map<String, Object> params){
        PageUtils page = comboPriceService.queryPage(params);

        return CommonResult.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{priceId}")
    @ApiOperation(value = "根据id查询套餐价格")
    // @RequiresPermissions("ad:comboprice:info")
    public CommonResult info(@PathVariable("priceId") Long priceId){
		ComboPriceEntity comboPrice = comboPriceService.getById(priceId);

        return CommonResult.ok().put("comboPrice", comboPrice);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @ApiOperation(value = "保存套餐价格")
    // @RequiresPermissions("ad:comboprice:save")
    public CommonResult save(@RequestBody ComboPriceEntity comboPrice){
        try {
            comboPriceService.save(comboPrice);
            return CommonResult.ok();
        } catch (Exception e) {
            log.error("ad-save-error", e);
            return CommonResult.error("操作失败");
        }
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @ApiOperation(value = "修改套餐价格")
    // @RequiresPermissions("ad:comboprice:update")
    public CommonResult update(@RequestBody ComboPriceEntity comboPrice){
        try {
            comboPriceService.updateById(comboPrice);
            return CommonResult.ok();
        } catch (Exception e) {
            log.error("ad-update-error", e);
            return CommonResult.error("操作失败");
        }
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @ApiOperation(value = "删除套餐价格")
    // @RequiresPermissions("ad:comboprice:delete")
    public CommonResult delete(@RequestBody Long[] priceIds){
        try {
            comboPriceService.removeByIds(Arrays.asList(priceIds));
            return CommonResult.ok();
        } catch (Exception e) {
            log.error("ad-delete-error", e);
            return CommonResult.error("操作失败");
        }
    }

}
