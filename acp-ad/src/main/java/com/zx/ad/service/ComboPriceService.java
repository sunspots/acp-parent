package com.zx.ad.service;

import com.zx.ad.entity.ComboPriceEntity;
import com.zx.ad.utils.common.PageResult;
import com.zx.ad.utils.common.PageUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service()
public interface ComboPriceService {
    void removeByIds(List<Long> asList);

    void updateById(ComboPriceEntity comboPrice);

    void save(ComboPriceEntity comboPrice);

    ComboPriceEntity getById(Long priceId);

    PageUtils queryPage(Map<String, Object> params);

    PageResult<ComboPriceEntity> findAll();
}
