package com.zx.ad.service;

import com.zx.ad.entity.ComboEntity;
import com.zx.ad.utils.common.PageResult;
import com.zx.ad.vo.ComboSearchEntity;

import java.util.List;

public interface ComboService {
    PageResult<ComboEntity> queryPage(ComboSearchEntity comboSearchEntity);

    ComboEntity getById(Long comboId);

    void save(ComboEntity combo);

    void updateById(ComboEntity combo);

    void removeByIds(List<Long> asList);
}
