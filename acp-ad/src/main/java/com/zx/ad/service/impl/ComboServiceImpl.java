package com.zx.ad.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zx.ad.dao.ComboDao;
import com.zx.ad.entity.ComboEntity;
import com.zx.ad.service.ComboService;
import com.zx.ad.utils.common.PageResult;
import com.zx.ad.vo.ComboSearchEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("comboService")
public class ComboServiceImpl implements ComboService {

    @Autowired
    ComboDao comboDao;

    @Override
    public PageResult<ComboEntity> queryPage(ComboSearchEntity comboSearchEntity) {
        Page<ComboEntity> page = new Page<>(comboSearchEntity.getPage(), comboSearchEntity.getLimit());
        IPage<ComboEntity> iPage = comboDao.queryPage(page, comboSearchEntity.getTitle(),comboSearchEntity.getDetails()
                , comboSearchEntity.getPriceMin(), comboSearchEntity.getPriceMax());
        List<ComboEntity> list = iPage.getRecords();
        long total = page.getTotal();
        return PageResult.<ComboEntity>builder().data(list).code(0).count(total).build();
    }

    @Override
    public ComboEntity getById(Long comboId) {
        return null;
    }

    @Override
    public void save(ComboEntity combo) {
        comboDao.save(combo);
    }

    @Override
    public void updateById(ComboEntity combo) {
        comboDao.updateById(combo);
    }

    @Override
    public void removeByIds(List<Long> asList) {
        comboDao.removeByIds(asList);
    }
}
