package com.zx.ad.vo;

import lombok.Data;

@Data
public class ComboSearchEntity {

    private String title;
    private String details;
    private Double priceMin;
    private Double priceMax;
    private Integer page;
    private Integer limit;
}
