package com.zx.ad.utils.exception;

import com.zx.ad.utils.common.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

/**
 * 集中处理所有检验异常
 */
@Slf4j
@RestControllerAdvice(basePackages = "com.zx.ad.controller")
public class ADExceptionControllerAdvice {

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public CommonResult handleValidException(MethodArgumentNotValidException e) {
        log.error("数据校验出现问题{}，异常类型：{}", e.getMessage(), e.getClass());
        BindingResult bindingResult = e.getBindingResult();
        Map<String, String> errorMap = new HashMap<>();
        bindingResult.getFieldErrors().forEach((filedError) -> {
            errorMap.put(filedError.getField(), filedError.getDefaultMessage());
        });
        return CommonResult.error(400, "数据校验出现问题").put("data", errorMap);
    }
}
