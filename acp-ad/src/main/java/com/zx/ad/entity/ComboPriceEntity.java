package com.zx.ad.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.zx.ad.utils.valid.AddGroup;
import com.zx.ad.utils.valid.DeleteGroup;
import com.zx.ad.utils.valid.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * 
 * 
 * @author lilei
 * @email mrstupide@163.com
 * @date 2020-09-17 14:27:02
 */
@Data
@TableName("acp_combo_price")
public class ComboPriceEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 广告价格主键id
	 */
	@Null(message = "新增priceId必须为空", groups = {AddGroup.class})
	@NotNull(message = "priceId不能为空", groups = {UpdateGroup.class, DeleteGroup.class})
	@TableId
	private Long priceId;
	/**
	 * 广告价格
	 */
	@NotBlank(message = "price不能为空", groups = {UpdateGroup.class, DeleteGroup.class})
	private Double price;
	/**
	 * 广告显示时间
	 */
	@NotBlank(message = "displayTime不能为空", groups = {UpdateGroup.class, DeleteGroup.class})
	private Date displayTime;
	/**
	 * 广告显示天数
	 */
	@NotBlank(message = "displayCount不能为空", groups = {UpdateGroup.class, DeleteGroup.class})
	private Integer displayCount;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除(0代表未删除，1代表已删除)
	 */
	@NotNull(message = "status不能为空", groups = {DeleteGroup.class})
	private Integer status;

}
