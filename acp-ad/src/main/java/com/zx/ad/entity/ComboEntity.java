package com.zx.ad.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.zx.ad.utils.valid.AddGroup;
import com.zx.ad.utils.valid.DeleteGroup;
import com.zx.ad.utils.valid.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * 
 * 
 * @author lilei
 * @email mrstupide@163.com
 * @date 2020-09-17 14:27:02
 */
@Data
@TableName("acp_combo")
public class ComboEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@Null(message = "新增comboId必须为空", groups = {AddGroup.class})
	@NotNull(message = "comboId不能为空", groups = {UpdateGroup.class, DeleteGroup.class})
	@TableId
	private Long comboId;
	/**
	 * 关联价格表
	 */
	@NotNull(message = "priceId不能为空", groups = {UpdateGroup.class, AddGroup.class})
	private Long priceId;
	/**
	 * 套餐标题
	 */
	@NotBlank(message = "title不能为空", groups = {UpdateGroup.class, AddGroup.class})
	private String title;
	/**
	 * 套餐图片
	 */
	@NotBlank(message = "img不能为空", groups = {UpdateGroup.class, AddGroup.class})
	private String img;
	/**
	 * 套餐详情
	 */
	@NotBlank(message = "details不能为空", groups = {UpdateGroup.class, AddGroup.class})
	private String details;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除(0代表未删除，1代表已删除)
	 */
	@NotNull(message = "status不能为空", groups = {DeleteGroup.class})
	private Integer status;

	private ComboPriceEntity comboPriceEntity;

}
