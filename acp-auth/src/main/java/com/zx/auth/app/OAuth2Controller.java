package com.zx.auth.app;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipaySystemOauthTokenRequest;
import com.alipay.api.response.AlipaySystemOauthTokenResponse;
import com.zx.auth.config.OAuth2Config;
import com.zx.auth.entity.*;
import com.zx.auth.feign.LoginFeignService;
import com.zx.auth.res.Result;
import com.zx.auth.service.FaceLoginService;
import com.zx.auth.utils.GsonUtils;
import com.zx.auth.utils.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @create 2020-09-21 11:31
 * @description: 第三方登录跳转controller
 */
@Slf4j
@Controller
public class OAuth2Controller {

    @Resource
    OAuth2Config oAuth2Config;

    @Resource
    LoginFeignService loginService;
    @Resource
    FaceLoginService faceLoginService;
    @Resource
    RedisTemplate<String,Object> redisTemplate;


    @RequestMapping("/oauth2.0/weibo/success")
    public String weibo(@RequestParam("code") String code) throws Exception{
        Map<String,String> header = new HashMap<>();
        Map<String,String> query = new HashMap<>();

        Map<String,String> map = new HashMap<>();
        map.put("client_id",oAuth2Config.getWB_CLIENT_ID());
        map.put("client_secret",oAuth2Config.getWB_CLIENT_SECRET());
        map.put("grant_type",oAuth2Config.getWB_GRANT_TYPE());
        map.put("redirect_uri",oAuth2Config.getWB_REDIRECT_URI());

        map.put("code",code);
        //1.根基code换去access_Token:
        HttpResponse response = HttpUtils.doPost("https://api.weibo.com", "/oauth2/access_token", "post", header, query, map);
        //2.处理access_Token
        if(response.getStatusLine().getStatusCode() == 200){
            //得到响应体内容
            String json = EntityUtils.toString(response.getEntity());
            SocialWeiBoEntity socialWeiBoUser = JSON.parseObject(json, SocialWeiBoEntity.class);
            log.info("得到的结果为:"+socialWeiBoUser.toString());

            //知道当前那个用户https://shimo.im/docs/yx8J3qywWWWKdgyH/
            //1)、当前用户如果第一次进入网站，自动注册进来(为当前社交用户生成一个会员信息账号，以后这个社交账户就对应指定的会员)
            //登录或者注册这个社交用户
            Result oauthLogin = loginService.oauthWeiboLogin(socialWeiBoUser);
            if (oauthLogin.getCode() == 200){
                Object data = oauthLogin.getData();
                Object userInfo = JSONObject.toJSON(data);
                redisTemplate.opsForValue().set("userInfo",userInfo);
                log.info("登录成功:用户:{}",userInfo);
                //3.登录成功后跳转到首页
                return  "success";
            }else {
                return  "login";
            }

        }else{
            return  "login";
        }
    }

    @RequestMapping("/oauth2.0/alipay/success")
    public String alipay(@RequestParam(value = "auth_code") String authCode) throws Exception{
        // 获取AuthCode 等，具体可以获取哪些数据参考支付宝官方接口文档，这里只需要使用auth_code
        log.info("authCode ==> "+authCode);
        if(StringUtils.isNotEmpty(authCode)) {

            // 这些参数均被抽取出来，参数说明见博客
            AlipayClient alipayClient = new DefaultAlipayClient(oAuth2Config.getAL_SERVER_URL(), oAuth2Config.getAL_APP_ID(),oAuth2Config.getAL_PRIVATE_KEY() , "json", "UTF-8", oAuth2Config.getAL_PUBLIC_KEY(), "RSA2");

            // 通过authCode获取accessToken
            AlipaySystemOauthTokenRequest request = new AlipaySystemOauthTokenRequest();
            request.setCode(authCode);
            request.setGrantType("authorization_code");

            AlipaySystemOauthTokenResponse oauthTokenResponse = alipayClient.execute(request);
            String body = oauthTokenResponse.getBody();
            log.info("获取到的值为:{}"+body);

            SocialAlipayEntity alipayEntity = new SocialAlipayEntity();

            alipayEntity.setAccessToken(oauthTokenResponse.getAccessToken());
            alipayEntity.setUserId(oauthTokenResponse.getAlipayUserId());
            alipayEntity.setExpiresTn(oauthTokenResponse.getExpiresIn());
            alipayEntity.setReExpiresTn(oauthTokenResponse.getReExpiresIn());
            alipayEntity.setRefreshToken(oauthTokenResponse.getRefreshToken());

            if (StringUtils.isNotEmpty(oauthTokenResponse.getAccessToken())){
                //知道当前那个用户
                //1)、当前用户如果第一次进入网站，自动注册进来(为当前社交用户生成一个会员信息账号，以后这个社交账户就对应指定的会员)
                //登录或者注册这个社交用户
                Result oauthLogin = loginService.oauthAlipayLogin(alipayEntity);
                if (oauthLogin.getCode() == 200){
                    Object data = oauthLogin.getData();
                    Object userInfo = JSONObject.toJSON(data);
                    redisTemplate.opsForValue().set("userInfo",userInfo);
                    log.info("登录成功:用户:{}",data.toString());
                    //3.登录成功后跳转到首页
                    return  "success";
                }else {
                    return  "login";
                }
            }

        }else{
            return  "login";
        }

        return "login";

    }

    @RequestMapping("/oauth2.0/qq/success")
    public String qqLogin(@RequestParam(value = "code") String code) throws Exception{
        if(StringUtils.isNotEmpty(code)) {
            Map<String,String> header = new HashMap<>();
            Map<String,String> query = new HashMap<>();

            Map<String,String> map = new HashMap<>();
            map.put("client_id",oAuth2Config.getQQ_CLIENT_ID());
            map.put("client_secret",oAuth2Config.getQQ_CLIENT_SECRET());
            map.put("grant_type",oAuth2Config.getQQ_GRANT_TYPE());
            map.put("redirect_uri",oAuth2Config.getQQ_REDIRECT_URI());
            map.put("code",code);
            //1.根剧code换去access_Token:
            HttpResponse response = HttpUtils.doPost("https://graph.qq.com", "/oauth2.0/token", "get", header, query, map);
            //2.处理access_Token
            if(response.getStatusLine().getStatusCode() == 200){
                //得到响应体内容
                String json = EntityUtils.toString(response.getEntity());
                String[] split = json.split("&");

                SocialQQEntity socialQQEntity = new SocialQQEntity();

                socialQQEntity.setAccess_token(split[0].split("=")[1]);
                socialQQEntity.setExpires_in(split[1].split("=")[1]);
                socialQQEntity.setRefresh_token(split[2].split("=")[1]);
                log.info("实体结果:{}"+socialQQEntity.toString());

                // //1.根基access_Token换去openid:
                if (StringUtils.isNotEmpty(socialQQEntity.getAccess_token())){

                    Map<String,String> reqheader = new HashMap<>();
                    Map<String,String> reqquery = new HashMap<>();

                    Map<String,String> reqmap = new HashMap<>();
                    reqmap.put("access_token",socialQQEntity.getAccess_token());

                    HttpResponse resp= HttpUtils.doPost("https://graph.qq.com", "/oauth2.0/me", "get", reqheader, reqquery, reqmap);
                    if(resp.getStatusLine().getStatusCode() == 200){
                        //得到响应体内容
                        String respOpenId = EntityUtils.toString(resp.getEntity());
                        String replace = respOpenId.replace("callback( ", "");
                        String cutOpenId = replace.replace(" );", "");

                        OpenIdEntity openIdEntity = JSONObject.parseObject(cutOpenId, OpenIdEntity.class);
                        String openid = openIdEntity.getOpenid();

                        openIdEntity.setAccess_token(socialQQEntity.getAccess_token());
                        openIdEntity.setExpires_in(socialQQEntity.getExpires_in());
                        openIdEntity.setRefresh_token(socialQQEntity.getRefresh_token());
                        if (openid !=null){
                            //知道当前那个用户
                            //1)、当前用户如果第一次进入网站，自动注册进来(为当前社交用户生成一个会员信息账号，以后这个社交账户就对应指定的会员)
                            //登录或者注册这个社交用户
                            Result oauthLogin =  loginService.oauthQQLogin(openIdEntity);
                            if (oauthLogin.getCode() == 200){
                                Object data = oauthLogin.getData();
                                Object userInfo = JSONObject.toJSON(data);
                                redisTemplate.opsForValue().set("userInfo",userInfo);
                                log.info("登录成功:用户:{}",data.toString());
                                //3.登录成功后跳转到首页
                                return  "success";
                            }else {
                                return  "login";
                            }

                        }else {
                            return  "login";
                        }
                    }
                }else {
                    return  "login";
                }
            }
        }else {
            return "login";
        }

            return  "login";
    }


    @RequestMapping("/oauth2.0/wechat/success")
    public String wechatLogin(@RequestParam(value = "code") String code) throws Exception{
        if(StringUtils.isNotEmpty(code)) {
            Map<String,String> header = new HashMap<>();
            Map<String,String> query = new HashMap<>();

            Map<String,String> map = new HashMap<>();
            map.put("appid",oAuth2Config.getWECHAT_APP_ID());
            map.put("secret",oAuth2Config.getWECHAT_APP_SECRET());
            map.put("grant_type",oAuth2Config.getWECHAT_GRANT_TYPE());
            map.put("code",code);
            //1.根剧code换去access_Token:
            HttpResponse response = HttpUtils.doPost("https://api.weixin.qq.com", "/sns/oauth2/access_token", "get", header, query, map);
            //2.处理access_Token
            if(response.getStatusLine().getStatusCode() == 200){
                //得到响应体内容
                String json = EntityUtils.toString(response.getEntity());
                SocialWeChatEntity socialWeChatEntity = JSON.parseObject(json, SocialWeChatEntity.class);
                log.info("得到的结果为:"+socialWeChatEntity.toString());

                if (StringUtils.isNotEmpty(socialWeChatEntity.getAccess_token())){
                    //知道当前那个用户
                    //1)、当前用户如果第一次进入网站，自动注册进来(为当前社交用户生成一个会员信息账号，以后这个社交账户就对应指定的会员)
                    //登录或者注册这个社交用户
                    Result weChatLogin = loginService.oauthWeChatLogin(socialWeChatEntity);
                    if (weChatLogin.getCode() == 200){
                        Object data = weChatLogin.getData();
                        Object userInfo = JSONObject.toJSON(data);
                        redisTemplate.opsForValue().set("userInfo",userInfo);
                        log.info("登录成功:用户:{}",data.toString());
                        //3.登录成功后跳转到首页
                        return  "success";
                    }else {
                        return  "login";
                    }

                }else {
                    return  "login";
                }
            }
        }else {
            return "login";
        }

        return  "login";
    }


    @RequestMapping("/login/searchface")
    @ResponseBody
    public String searchface(@RequestBody @RequestParam(name = "imagebast64") StringBuffer imagebast64, Model model) throws IOException {
        Map<String, Object> searchface = faceLoginService.searchface(imagebast64);
        if(searchface==null||searchface.get("user_id")==null){
            log.info("我进来了");
            String flag="fail";
            String s = GsonUtils.toJson(flag);
            return s;
        }
        String user_id = searchface.get("user_id").toString();
        String score=searchface.get("score").toString().substring(0,2);
        int i = Integer.parseInt(score);
        if(i>80){
            model.addAttribute("userinf",user_id);
        }


        System.out.println(searchface);
        String s = GsonUtils.toJson(searchface);
        return s;


    }


    @PostMapping("/login/registerface")
    @ResponseBody
    public String registerface(@RequestBody@RequestParam(name = "imagebast64") StringBuffer imagebast64) throws IOException {
        Map<String, Object> searchface = faceLoginService.registerface(imagebast64);
        if(searchface!=null && searchface.get("error_msg").equals("SUCCESS")){
            System.out.println("我进来了");
            String flag="fail";
            String s = GsonUtils.toJson(flag);
            return s;
        }

        return null;

    }


}
