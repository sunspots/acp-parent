/**
  * Copyright 2020 bejson.com 
  */
package com.zx.auth.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Auto-generated: 2020-09-21 15:11:53
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SocialWeiBoEntity {

    private String access_token;
    private String remind_in;
    private long expires_in;
    private String uid;
    private String isRealName;


}