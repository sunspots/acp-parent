package com.zx.auth.feign.fallback;

import com.zx.auth.entity.*;
import com.zx.auth.feign.LoginFeignService;
import com.zx.auth.res.Result;
import com.zx.auth.res.ResultCode;
import org.springframework.stereotype.Component;

/**
 * @create 2020-09-24 14:49
 * @description:
 */
@Component
public class LoginFeignFallback implements LoginFeignService {

    @Override
    public Result oauthWeiboLogin(SocialWeiBoEntity socialWeiBoEntity) throws Exception {
       return new Result(ResultCode.SERVER_ERROR);
    }

    @Override
    public Result oauthAlipayLogin(SocialAlipayEntity socialAlipayEntity) throws Exception {
        return new Result(ResultCode.SERVER_ERROR);
    }

    @Override
    public Result oauthQQLogin(OpenIdEntity openIdEntity) throws Exception {
        return new Result(ResultCode.SERVER_ERROR);
    }

    @Override
    public Result oauthWeChatLogin(SocialWeChatEntity socialWeChatEntity) throws Exception {
        return new Result(ResultCode.SERVER_ERROR);
    }
}
