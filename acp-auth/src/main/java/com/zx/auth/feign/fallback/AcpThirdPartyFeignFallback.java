package com.zx.auth.feign.fallback;

import com.zx.auth.feign.AcpThirdPartyFeign;
import com.zx.auth.res.Result;
import com.zx.auth.res.ResultCode;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * 服务降级
 */
@Component
public class AcpThirdPartyFeignFallback implements AcpThirdPartyFeign {
    @Override
    public Result idCard(MultipartFile file, String idCardSide) {
        return new Result(ResultCode.SERVER_ERROR);
    }

    @Override
    public Result bankCard(MultipartFile file) {
        return new Result(ResultCode.SERVER_ERROR);
    }

    @Override
    public Result businessLicense(MultipartFile file) {
        return new Result(ResultCode.SERVER_ERROR);
    }

    @Override
    public void sendSms(String mobile, String code) {

    }


}
