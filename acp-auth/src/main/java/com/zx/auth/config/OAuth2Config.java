package com.zx.auth.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @create 2020-09-27 14:12
 * @description:
 */
@Component
public class OAuth2Config {
    @Value("${alipay.server_url}")
    private String AL_SERVER_URL;
    @Value("${alipay.app_id}")
    private String AL_APP_ID;
    @Value("${alipay.private_key}")
    private String AL_PRIVATE_KEY;
    @Value("${alipay.public_key}")
    private String AL_PUBLIC_KEY;


    @Value("${weibo.client_id}")
    private String WB_CLIENT_ID;
    @Value("${weibo.client_secret}")
    private String WB_CLIENT_SECRET;
    @Value("${weibo.grant_type}")
    private String WB_GRANT_TYPE;
    @Value("${weibo.redirect_uri}")
    private String WB_REDIRECT_URI;


    @Value("${qq.client_id}")
    private String QQ_CLIENT_ID;
    @Value("${qq.client_secret}")
    private String QQ_CLIENT_SECRET;
    @Value("${qq.grant_type}")
    private String QQ_GRANT_TYPE;
    @Value("${qq.redirect_uri}")
    private String QQ_REDIRECT_URI;

    @Value("${weChat.app_id}")
    private String WECHAT_APP_ID;
    @Value("${weChat.app_secret}")
    private String WECHAT_APP_SECRET;
    @Value("${weChat.grant_type}")
    private String WECHAT_GRANT_TYPE;
    @Value("${weChat.redirect_url}")
    private String WECHAT_REDIRECT_URL;

    public String getAL_SERVER_URL() {
        return AL_SERVER_URL;
    }

    public String getAL_APP_ID() {
        return AL_APP_ID;
    }

    public String getAL_PRIVATE_KEY() {
        return AL_PRIVATE_KEY;
    }

    public String getAL_PUBLIC_KEY() {
        return AL_PUBLIC_KEY;
    }

    public String getWB_CLIENT_ID() {
        return WB_CLIENT_ID;
    }

    public String getWB_CLIENT_SECRET() {
        return WB_CLIENT_SECRET;
    }

    public String getWB_GRANT_TYPE() {
        return WB_GRANT_TYPE;
    }

    public String getWB_REDIRECT_URI() {
        return WB_REDIRECT_URI;
    }

    public String getQQ_CLIENT_ID() {
        return QQ_CLIENT_ID;
    }

    public String getQQ_CLIENT_SECRET() {
        return QQ_CLIENT_SECRET;
    }

    public String getQQ_GRANT_TYPE() {
        return QQ_GRANT_TYPE;
    }

    public String getQQ_REDIRECT_URI() {
        return QQ_REDIRECT_URI;
    }

    public String getWECHAT_APP_ID() {
        return WECHAT_APP_ID;
    }

    public String getWECHAT_APP_SECRET() {
        return WECHAT_APP_SECRET;
    }

    public String getWECHAT_GRANT_TYPE() {
        return WECHAT_GRANT_TYPE;
    }

    public String getWECHAT_REDIRECT_URL() {
        return WECHAT_REDIRECT_URL;
    }
}
