package com.zx.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.zx.auth.feign")
public class AcpAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(AcpAuthApplication.class, args);
    }

    @Bean
    public TomcatServletWebServerFactory mbeddedServletContainerFactory() {
        TomcatServletWebServerFactory tomcatEmbeddedServletContainerFactory = new TomcatServletWebServerFactory ();

        tomcatEmbeddedServletContainerFactory.addConnectorCustomizers(connector ->{
            connector.setMaxParameterCount(100000000);
            connector.setMaxPostSize(100000000);

        });

        return tomcatEmbeddedServletContainerFactory;
    }




}
