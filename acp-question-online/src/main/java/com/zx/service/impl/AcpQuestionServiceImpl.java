package com.zx.service.impl;

import com.zx.model.AcpQuestion;
import com.zx.mapper.AcpQuestionMapper;
import com.zx.service.AcpQuestionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author MyBatisPlusGenerater
 * @since 2020-09-17
 */
@Service
public class AcpQuestionServiceImpl extends ServiceImpl<AcpQuestionMapper, AcpQuestion> implements AcpQuestionService {

    @Autowired
    private AcpQuestionMapper acpQuestionMapper;
    @Override
    public List<AcpQuestion> findAll() {
        return acpQuestionMapper.selectList(null);
    }

    @Override
    public Integer comeUpWith(AcpQuestion acpQuestion) {
        return acpQuestionMapper.insert(acpQuestion);
    }

    @Override
    public AcpQuestion getAnswerByQuestionId(Integer id) {
        return acpQuestionMapper.selectById(id);
    }
}
