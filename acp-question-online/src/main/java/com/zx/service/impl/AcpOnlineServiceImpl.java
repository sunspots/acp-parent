package com.zx.service.impl;

import com.zx.model.AcpOnline;
import com.zx.mapper.AcpOnlineMapper;
import com.zx.service.AcpOnlineService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author MyBatisPlusGenerater
 * @since 2020-09-17
 */
@Service
public class AcpOnlineServiceImpl extends ServiceImpl<AcpOnlineMapper, AcpOnline> implements AcpOnlineService {

    @Autowired
    private AcpOnlineMapper acpOnlineMapper;
    @Override
    public AcpOnline getContact() {
        return  acpOnlineMapper.selectList(null).get(0);
    }
}
