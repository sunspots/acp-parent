package com.zx.service;

import com.zx.model.AcpQuestion;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author MyBatisPlusGenerater
 * @since 2020-09-17
 */
public interface AcpQuestionService extends IService<AcpQuestion> {

    List<AcpQuestion> findAll();

    Integer comeUpWith(AcpQuestion acpQuestion);

    AcpQuestion getAnswerByQuestionId(Integer id);
}
