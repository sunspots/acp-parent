package com.zx.mapper;

import com.zx.model.AcpQuestion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author MyBatisPlusGenerater
 * @since 2020-09-17
 */
@Repository
public interface AcpQuestionMapper extends BaseMapper<AcpQuestion> {

    //List<AcpQuestion> findAll();
}
