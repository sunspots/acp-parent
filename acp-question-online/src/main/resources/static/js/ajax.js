function ajaxUtil(url, method, data) {
    let result = {}
    $.ajax({
        url: "http://localhost:56789/" + url,
        type: method,
        data: data,
        dataType: "json",
        success(data) {
            result.error = ""
            result.data = data
        },
        error(e) {
            result.error = "请求异常"
            result.data = ""
        }
    })
    return result
}
