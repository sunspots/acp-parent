package com.zx.user.center;

import com.zx.user.center.entity.UserPersonalCenterEntity;
import com.zx.user.center.service.PersonalUserCenterService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author l-xin
 * @create 2020-09-18 14:42
 * @description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserCenterTest {

    @Resource
    PersonalUserCenterService personalUserCenterService;

    @Test
    public void testPersonal(){

    List<UserPersonalCenterEntity>  list = personalUserCenterService.findAll();
    list.forEach(System.out::println);
    }
}
