package com.zx.user.center.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyTotalDaily {
    private Integer total;
    private String days;
    private String start;
    private String end;
}
