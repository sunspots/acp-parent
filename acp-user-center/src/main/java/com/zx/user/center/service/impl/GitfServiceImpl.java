package com.zx.user.center.service.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zx.user.center.dao.GitfDao;
import com.zx.user.center.dao.UserGitfDao;
import com.zx.user.center.entity.GitfEntity;
import com.zx.user.center.entity.UserGitfEntity;
import com.zx.user.center.service.GitfService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service("gitfService")
@Slf4j
public class GitfServiceImpl extends ServiceImpl<GitfDao, GitfEntity> implements GitfService {

    @Autowired
    private GitfDao gitfDao;

    @Autowired
    private UserGitfDao userGitfDao;

    @Override
    public IPage<GitfEntity> getGitfLIst(Page<GitfEntity> page, Integer status) {
        // 礼品列表查询
        IPage gitfList = gitfDao.getGitfList(page, status);
        return gitfList;
    }

    @Override
    public GitfEntity getGitf(Long gitf_id) {
        GitfEntity gitf = gitfDao.getGitf(gitf_id);
        return gitf;
    }

    @Override
    @Transactional
    public String exchangeGitf(Long gitf_id,Long userId,Integer version) {
        // 兑换礼品，减少礼品库存
        Integer integer = gitfDao.putGitfById(gitf_id,version);
        if (integer <1){
            return "库存数量不足";
        }
        // 获取礼品信息
        GitfEntity gitf = gitfDao.getGitf(gitf_id);
        UserGitfEntity userGitfEntity = new UserGitfEntity()
                .setUserId(userId)
                .setUserGitfName(gitf.getGitfName())
                .setUserGitfImg(gitf.getGitfImg())
                .setUserGitfIntegral(gitf.getGitfIntegral().toString())
                .setUserGitfStatus(1);
        // 生成用户兑换礼品信息
//        int insert = userGitfDao.insert(userGitfEntity);
        userGitfDao.postUserGitf(userGitfEntity);
        return "兑换成功";
    }

    @Override
    public String postGitf(GitfEntity gitfEntity) {
        // 添加礼品
        Integer integer = gitfDao.postGitf(gitfEntity);
        return null;
    }

    @Override
    public String deleteGitfById(Long gitf_id) {
        // 判断当前礼品是否已经下架 或不存在
        GitfEntity gitf = gitfDao.getGitf(gitf_id);
        if (gitf == null){
            return "该礼品不存在";
        }
        if (gitf.getStatus() == 1){
            return "该礼品已下架";
        }
        // 将该礼品设置为下架状态
        gitfDao.deleteGitfById(gitf_id);
        return "下架成功";
    }

    @Override
    public String putGitfStock(Long gitf_id, Integer gitf_stock) {
        Integer integer = gitfDao.putGitfStockById(gitf_id, gitf_stock);
        if (integer<1) return "库存数量更新失败";
        return "库存数量更新成功";
    }
}