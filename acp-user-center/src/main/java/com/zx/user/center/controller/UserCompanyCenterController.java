package com.zx.user.center.controller;

import com.zx.user.center.common.CommonResult;
import com.zx.user.center.entity.*;
import com.zx.user.center.res.Result;
import org.checkerframework.checker.units.qual.C;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.zx.user.center.service.UserCompanyCenterService;
import org.thymeleaf.standard.expression.Each;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;


/**
 * @author
 * @email
 * @date 2020-09-16 18:40:42
 */
@RestController
@RequestMapping("/company")
public class UserCompanyCenterController {
    @Autowired
    private UserCompanyCenterService companyUserCenterService;

    @PostMapping("/saveCompany")
    @ResponseBody
    public Result saveCompany(@RequestBody UserCompanyCenterEntity userCompanyCenterEntity) {
        return companyUserCenterService.saveCompany(userCompanyCenterEntity);
    }





    //查询公司用户会员日增长量
    @GetMapping("QueryMemberDayGrowthC")
    public CommonResult QueryMemberDailyGrowthC(@RequestParam LocalDateTime start, @RequestParam LocalDateTime end) {
        Map<String, List> map = new HashMap<>();
        List<String> monthList = new ArrayList<>();
        List<Integer> totalList = new ArrayList<>();
        List<CompanyTotalDaily> companyTotalDailies = companyUserCenterService.QueryMemberDailyGrowthC(start,end);
        companyTotalDailies.forEach(companyTotalDailie -> {
            monthList.add(companyTotalDailie.getDays());
            totalList.add(companyTotalDailie.getTotal());
        });
        map.put("xAxisList", monthList);
        map.put("yAxisList", totalList);
        return CommonResult.success(map);
    }

//    //查询公司用户会员月增长量
    @GetMapping("QueryMemberMonthGrowthC")
    public CommonResult QueryMemberMonthGrowthC(String year) {
        Map<String, List> map = new HashMap<>();
        List<String> monthList = new ArrayList<>();
        List<Integer> totalList = new ArrayList<>();
        System.out.println(year);
        List<CompanyTotalMonth> companyTotalMonth = companyUserCenterService.QueryMemberMonthGrowthC(year);
        companyTotalMonth.forEach(companyTotalMonths -> {
            monthList.add(companyTotalMonths.getMonths());
            totalList.add(companyTotalMonths.getTotal());
        });
        map.put("xAxisList", monthList);
        map.put("yAxisList", totalList);
        return CommonResult.success(map);
    }



    //查询公司用户会员年增长量
    @GetMapping("/QueryMemberYearGrowthC")
    public CommonResult QueryMemberYearGrowthC() {
        Map<String, List> map = new HashMap<>();
        List<String> yearList = new ArrayList<>();
        List<Integer> totalList = new ArrayList<>();
        List<CompanyTotalYear> companyTotalYears = companyUserCenterService.QueryMemberYearGrowthC();
        companyTotalYears.forEach(companyTotalYear -> {
            yearList.add(companyTotalYear.getYears());
            totalList.add(companyTotalYear.getTotal());
        });
        map.put("xAxisList", yearList);
        map.put("yAxisList", totalList);
        return CommonResult.success(map);
    }


}
