package com.zx.user.center.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.zx.user.center.common.CommonResult;
import com.zx.user.center.entity.*;
import com.zx.user.center.res.Result;
import org.checkerframework.checker.units.qual.C;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @author
 * @email
 * @date 2020-09-16 18:40:42
 */
public interface UserCompanyCenterService extends IService<UserCompanyCenterEntity> {

    // 企业注册
    Result saveCompany(UserCompanyCenterEntity userCompanyCenterEntity);

    /**
     * 修改账号余额
     *
     * @param loginname
     * @param balance
     * @return
     */
    void putBalance(String loginname, BigDecimal balance);



    //查询公司用户会员的日增长数
    List<CompanyTotalDaily> QueryMemberDailyGrowthC(LocalDateTime start, LocalDateTime end);

    //查询公司用户会员的月增长数
    List<CompanyTotalMonth> QueryMemberMonthGrowthC(String year);

    //查询公司用户会员的年增长数
    List<CompanyTotalYear> QueryMemberYearGrowthC();
}

