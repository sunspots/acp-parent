package com.zx.user.center.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zx.user.center.dao.GitfDao;
import com.zx.user.center.entity.GitfEntity;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.omg.CORBA.Current;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import com.zx.user.center.service.GitfService;

import java.util.List;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2020-09-16 18:40:42
 */
@RestController
@RequestMapping("center/gitf")
public class GitfController {

    @Autowired
    private GitfService gitfService;

    /**
     * 礼品列表
     * @param current 当前页
     * @param status 状态
     * @param model
     * @return
     */
    @GetMapping("/list")
    public IPage<GitfEntity> getGGitfList(Integer current,Integer status, Model model){

        IPage<GitfEntity> gitfLIst = gitfService.getGitfLIst(new Page<GitfEntity>(current, 6), status);
        model.addAttribute("gitfList",gitfLIst);
//        return "usercentry/usercentry";
        return gitfLIst;
    }

    /**
     * 根据id 查看礼品
     * @return
     */
    @GetMapping("/get/{gitf_id}")
    public GitfEntity getGitf(@PathVariable("gitf_id") Long gitf_id,Model model){

        GitfEntity gitfEntity =  gitfService.getGitf(gitf_id);
        model.addAttribute("gitfEntity",gitfEntity);
        return gitfEntity;
    }

    /**
     * 兑换礼品
     * 根据id 减少库存
     * 生成用户兑换礼品信息
     * @param gitf_id 礼品id
     * @param version 乐观锁版本号
     * @param user_id 兑换礼品的用户id
     * @return
     */
    @PutMapping("/exchange/gitf")
    public String exchangeGitf(@RequestParam("gitf_id") Long gitf_id,@RequestParam("version") Integer version,@RequestParam("userId") Long user_id){
        String message = gitfService.exchangeGitf(gitf_id,user_id,version);
        return message;
    }


    /**
     * 上架新礼品
     * @param gitfEntity
     * @return
     */
    @PostMapping("/post/gitf")
    public String postGitf(@RequestBody GitfEntity gitfEntity){
        String message = gitfService.postGitf(gitfEntity);
        return null;
    }

    /**
     * 下架礼品
     * @param gitf_id
     * @return
     */
    @DeleteMapping("/delete/gitf/{gitf_id}")
    public String deleteGitf(@PathVariable("gitf_id")Long gitf_id){

        String message = gitfService.deleteGitfById(gitf_id);
        return message;
    }

    /**
     * 修改礼品库存数量
     * @param gitf_stock
     * @param gitf_id
     * @return
     */
    @PutMapping("/put/stock")
    public String putGitfStock( @RequestParam("gitf_stock")Integer gitf_stock,@RequestParam("gitf_id")Long gitf_id){

        String message = gitfService.putGitfStock(gitf_id,gitf_stock);
        return "";

    }



}











