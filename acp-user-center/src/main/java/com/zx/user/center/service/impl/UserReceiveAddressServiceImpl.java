package com.zx.user.center.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zx.user.center.dao.UserReceiveAddressDao;
import com.zx.user.center.entity.UserReceiveAddressEntity;
import com.zx.user.center.service.UserReceiveAddressService;
import org.springframework.stereotype.Service;

@Service("userReceiveAddressService")
public class UserReceiveAddressServiceImpl extends ServiceImpl<UserReceiveAddressDao, UserReceiveAddressEntity> implements UserReceiveAddressService {



}