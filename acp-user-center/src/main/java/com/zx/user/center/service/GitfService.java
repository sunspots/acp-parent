package com.zx.user.center.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zx.user.center.dao.GitfDao;
import com.zx.user.center.entity.GitfEntity;
import org.springframework.beans.factory.annotation.Autowired;
import sun.misc.Version;

import java.util.List;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2020-09-16 18:40:42
 */
public interface GitfService extends IService<GitfEntity> {


    IPage<GitfEntity> getGitfLIst(Page<GitfEntity> page, Integer status);

    GitfEntity getGitf(Long gitf_id);

    String exchangeGitf(Long gitf_id, Long userId, Integer version);

    String postGitf(GitfEntity gitfEntity);

    String deleteGitfById(Long gitf_id);

    String putGitfStock(Long gitf_id, Integer gitf_stock);
}

