package com.zx.user.center.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.zx.user.center.entity.UserGitfEntity;

import java.util.List;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2020-09-16 18:40:42
 */
public interface UserGitfService extends IService<UserGitfEntity> {


    List<UserGitfEntity> getUserGitfList(Long user_id,Integer status);

    String deleteUserGitf(Long user_id, Long user_gitf_id);

    String putUserGitfStatus(Long user_gitf_id,Integer user_gitf_status);
}

