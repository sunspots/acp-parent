package com.zx.user.center.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zx.user.center.dao.UserGitfDao;
import com.zx.user.center.entity.UserGitfEntity;
import com.zx.user.center.service.UserGitfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userGitfService")
public class UserGitfServiceImpl extends ServiceImpl<UserGitfDao, UserGitfEntity> implements UserGitfService {

    @Autowired
    private UserGitfDao userGitfDao;

    @Override
    public List<UserGitfEntity> getUserGitfList(Long user_id,Integer status) {
        // 获取用户兑换礼品的列表
        List<UserGitfEntity> list = userGitfDao.getUserGitfList(user_id,status);
        if (list.size()==0){  // 没有数据
            return null;
        }
        return list;
    }

    @Override
    public String deleteUserGitf(Long user_id, Long user_gitf_id) {
        // 删除用户的兑换礼品记录
        Integer integer = userGitfDao.deleteUserGitf(user_id,user_gitf_id);
        return null;
    }

    @Override
    public String putUserGitfStatus(Long user_gitf_id,Integer user_gitf_status) {
        Integer integer = userGitfDao.putUserGitfStatus(user_gitf_id,user_gitf_status);
        return null;
    }
}