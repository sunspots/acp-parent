package com.zx.user.center.service;

import com.zx.user.center.entity.PayOrderEntity;

public interface PayOrderService {

    void createOrder(PayOrderEntity payOrderEntity);

    String getLoginname(String outTradeNo,String getPayment);
}
