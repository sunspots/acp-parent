package com.zx.user.center.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyTotalMonth {
    private Integer total;
    private String months;
    private String year;
}

