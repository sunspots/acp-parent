package com.zx.user.center.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zx.user.center.common.CommonResult;
import com.zx.user.center.dao.UserCompanyCenterDao;
import com.zx.user.center.entity.CompanyTotalDaily;
import com.zx.user.center.entity.CompanyTotalMonth;
import com.zx.user.center.entity.CompanyTotalYear;
import com.zx.user.center.entity.UserCompanyCenterEntity;
import com.zx.user.center.res.Result;
import com.zx.user.center.res.ResultCode;
import com.zx.user.center.service.UserCompanyCenterService;
import org.checkerframework.checker.units.qual.C;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service("companyUserCenterService")
@Transactional
public class UserCompanyCenterServiceImpl extends ServiceImpl<UserCompanyCenterDao, UserCompanyCenterEntity> implements UserCompanyCenterService {

    @Resource
    private UserCompanyCenterDao userCompanyCenterDao;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * 企业注册
     *
     * @param userCompanyCenterEntity
     * @return
     */
    @Override
    public Result saveCompany(UserCompanyCenterEntity userCompanyCenterEntity) {
        // 对密码进行加密
        userCompanyCenterEntity.setPassword(bCryptPasswordEncoder.encode(userCompanyCenterEntity.getPassword()));
        userCompanyCenterEntity.setIntegration(10L);
        userCompanyCenterEntity.setGrowth(10L);
        userCompanyCenterEntity.setLevelId(1L);
        if (userCompanyCenterDao.saveCompany(userCompanyCenterEntity) > 0)
            return new Result(ResultCode.SUCCESS, "注册成功");
        else
            return new Result(ResultCode.FAIL, "注册失败");
    }

    /**
     * 修改用户余额
     *
     * @param loginname
     * @param balance
     * @return
     */
    @Override
    public void putBalance(String loginname, BigDecimal balance) {
        userCompanyCenterDao.putBalance(loginname, balance);
    }



    @Override
    public List<CompanyTotalDaily> QueryMemberDailyGrowthC(LocalDateTime start, LocalDateTime end) {
        List<CompanyTotalDaily> companyTotalDailies = userCompanyCenterDao.QueryMemberDailyGrowthC(start, end);
        return companyTotalDailies;
    }

    @Override
    public List<CompanyTotalMonth> QueryMemberMonthGrowthC(String year) {
        List<CompanyTotalMonth> companyTotalMonth = userCompanyCenterDao.QueryMemberMonthGrowthC(year);
        return companyTotalMonth;
    }


    @Override
    public List<CompanyTotalYear> QueryMemberYearGrowthC() {
        List<CompanyTotalYear> companyTotalYears = userCompanyCenterDao.QueryMemberYearGrowthC();
        return companyTotalYears;

    }
}