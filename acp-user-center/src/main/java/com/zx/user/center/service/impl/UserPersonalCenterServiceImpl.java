package com.zx.user.center.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zx.user.center.dao.UserCompanyCenterDao;
import com.zx.user.center.dao.UserPersonalCenterDao;
import com.zx.user.center.entity.*;
import com.zx.user.center.res.Result;
import com.zx.user.center.res.ResultCode;
import com.zx.user.center.service.UserPersonalCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Service("personalUserCenterService")
@Transactional
public class UserPersonalCenterServiceImpl extends ServiceImpl<UserPersonalCenterDao, UserPersonalCenterEntity> implements UserPersonalCenterService {

    @Resource
    private UserPersonalCenterDao personalUserCenterDao;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Result getById(Long id) {
        if (null==id||id<0)
            return Result.notFind();
        UserPersonalCenterEntity userForDB = personalUserCenterDao.selectById(id);
        if (null!=userForDB){
            return Result.SUCCESS(userForDB);
        }
      return Result.notFind();
    }

    @Override
    public List<UserPersonalCenterEntity> findAll() {

        return personalUserCenterDao.findAll();
    }

    /**
     * 个人注册
     *
     * @param userPersonalCenterEntity
     * @return
     */
    @Override
    public Result savePersonal(UserPersonalCenterEntity userPersonalCenterEntity) {
        // 对密码进行加密
        userPersonalCenterEntity.setPassword(bCryptPasswordEncoder.encode(userPersonalCenterEntity.getPassword()));
        userPersonalCenterEntity.setSourceType(1);
        userPersonalCenterEntity.setIntegration(10L);
        userPersonalCenterEntity.setGrowth(10L);
        userPersonalCenterEntity.setLevelId(1L);
        if (personalUserCenterDao.savePersonal(userPersonalCenterEntity) > 0)
            return new Result(ResultCode.SUCCESS, "注册成功");
        else
            return new Result(ResultCode.FAIL, "注册失败");
    }

    @Override
    public void putBalance(String loginname, BigDecimal balance) {
        personalUserCenterDao.putBalance(loginname, balance);
    }

    //查询个人用户数量
    @Override
    public Result QueryNumberOfPersonalUsers(Long userId) {
        return personalUserCenterDao.QueryNumberOfPersonalUsers(userId);
    }

    //查询个人用户日增长量
    @Override
    public Result QueryDailyGrowthP(Long userId) {
        return personalUserCenterDao.QueryDailyGrowthP(userId);
    }

    //查询个人用户月增长量
    @Override
    public Result QueryMonthGrowthP(Long userId) {
        return personalUserCenterDao.QueryMonthGrowthP(userId);
    }

    //查询个人用户年增长量
    @Override
    public Result QueryYearGrowthP(Long userId) {
        return personalUserCenterDao.QueryYearGrowthP(userId);
    }

    @Override
    public Result QueryPersonalMembers(Long userId) {
        return personalUserCenterDao.QueryPersonalMembers(userId);
    }

    @Override
    public List<CompanyTotalDaily> QueryMemberDailyGrowthP(LocalDateTime start, LocalDateTime end) {
        return personalUserCenterDao.QueryMemberDailyGrowthP(start, end);
    }

    @Override
    public List<CompanyTotalMonth> QueryMemberMonthGrowthP(String year) {
        return personalUserCenterDao.QueryMemberMonthGrowthP(year);
    }

    @Override
    public List<CompanyTotalYear> QueryMemberYearGrowthP() {
        return personalUserCenterDao.QueryMemberYearGrowthP();
    }




}