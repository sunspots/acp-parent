package com.zx.user.center.controller;


import com.zx.user.center.common.CommonResult;
import com.zx.user.center.entity.CompanyTotalDaily;
import com.zx.user.center.entity.CompanyTotalMonth;
import com.zx.user.center.entity.CompanyTotalYear;
import com.zx.user.center.entity.UserPersonalCenterEntity;
import com.zx.user.center.res.Result;
import com.zx.user.center.service.UserPersonalCenterService;
import org.checkerframework.checker.units.qual.C;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author
 * @email
 * @date 2020-09-16 18:40:42
 */
@RestController
@RequestMapping("/personal")
public class UserPersonalCenterController {
    @Autowired
    private UserPersonalCenterService userPersonalCenterService;

    /**
     * 添加 /注册
     *
     * @param userPersonalCenterEntity
     * @return
     */
    @PostMapping("/savePersonal")
    @ResponseBody
    public Result savePersonal(@RequestBody UserPersonalCenterEntity userPersonalCenterEntity) {
        return userPersonalCenterService.savePersonal(userPersonalCenterEntity);
    }

    /**
     *
     * @param id
     * @return
     */
    @GetMapping("/getById")
    @ResponseBody
    public Result getById(Long id) {
        return userPersonalCenterService.getById(id);
    }


    //查询个人用户的数量
    @GetMapping("QueryNumberOfPersonalUsers")
    public Result QueryNumberOfCompanyUsers(@RequestParam Long userId) {
        return userPersonalCenterService.QueryNumberOfPersonalUsers(userId);
    }

    //查询个人用户日增长量
    @GetMapping("QueryDailyGrowthP")
    public Result QueryDailyGrowthP(@RequestParam Long userId){
        return userPersonalCenterService.QueryDailyGrowthP(userId);
    }

    //查询个人用户月增长量
    @GetMapping("QueryMonthGrowthP")
    public Result QueryMonthGrowthP(@RequestParam Long userId){
        return userPersonalCenterService.QueryMonthGrowthP(userId);
    }

    //查询个人用户年增长量
    @GetMapping("QueryYearGrowthP")
    public Result QueryYearGrowthP(@RequestParam Long userId){
        return userPersonalCenterService.QueryYearGrowthP(userId);
    }

    //查询个人会员用户的数量
    @GetMapping("QueryPersonalMembers")
    public Result QueryPersonalMembers(@RequestParam Long userId) {
        return userPersonalCenterService.QueryPersonalMembers(userId);
    }

    //查询个人会员用户日增长量
    @GetMapping("/QueryMemberDayGrowthP")
    public CommonResult QueryMemberDailyGrowthP(@RequestParam LocalDateTime start, @RequestParam LocalDateTime end){
        Map<String, List> map = new HashMap<>();
        List<String> monthList = new ArrayList<>();
        List<Integer> totalList = new ArrayList<>();
        List<CompanyTotalDaily> companyTotalDailies = userPersonalCenterService.QueryMemberDailyGrowthP(start,end);
        companyTotalDailies.forEach(totalDaily -> {
            monthList.add(totalDaily.getDays());
            totalList.add(totalDaily.getTotal());
        });
        map.put("xAxisList", monthList);
        map.put("yAxisList", totalList);
        return CommonResult.success(map);
    }

    //查询个人会员用户月增长量
    @GetMapping("/QueryMemberMonthGrowthP")
    public CommonResult QueryMemberMonthGrowthP(String year){
        Map<String, List> map = new HashMap<>();
        List<String> monthList = new ArrayList<>();
        List<Integer> totalList = new ArrayList<>();
        List<CompanyTotalMonth> totalMonth = userPersonalCenterService.QueryMemberMonthGrowthP(year);
        totalMonth.forEach(totalMonths -> {
            monthList.add(totalMonths.getMonths());
            totalList.add(totalMonths.getTotal());
        });
        map.put("xAxisList", monthList);
        map.put("yAxisList", totalList);
        return CommonResult.success(map);
    }

    //查询个人会员用户年增长量
    @GetMapping("/QueryMemberYearGrowthP")
    public CommonResult QueryMemberYearGrowthP(){
        Map<String,List> map=new HashMap<>();
        List<String> yearList = new ArrayList<>();
        List<Integer> totalList = new ArrayList<>();
        List<CompanyTotalYear> totalYears = userPersonalCenterService.QueryMemberYearGrowthP();
        totalYears.forEach(totalMonths -> {
            yearList.add(totalMonths.getYears());
            totalList.add(totalMonths.getTotal());
        });
        map.put("xAxisList", yearList);
        map.put("yAxisList", totalList);
        return CommonResult.success(map);
    }
}
