package com.zx.user.center.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Accessors(chain = true)
public class PayOrderEntity {

    /**
     * 支付订单id
     */
    private Long alipayId;
    /**
     * 商户订单号
     */
    private String outTradeNo;
    /**
     * 订单金额
     */
    private BigDecimal totalAmount;
    /**
     * 订单标题
     */
    private String subject;
    /**
     * 付款时间
     */
    private Date gmtPayment;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 订单状态
     */
    private Integer alipayStatus;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 订单用户登录名
     */
    private String loginname;



}
