package com.zx.user.center.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zx.user.center.entity.PayOrderEntity;
import com.zx.user.center.entity.UserCompanyCenterEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
public interface PayOrderDao extends BaseMapper<UserCompanyCenterEntity> {

    void createOrder(PayOrderEntity payOrderEntity);

    void putPayStatus(String outTradeNo,@Param("gmtPayment")String gmtPayment);

    String getLoginname(@Param("outTradeNo") String outTradeNo);

}
