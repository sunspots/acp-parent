package com.zx.user.center.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.xml.crypto.Data;
import java.util.Date;
import java.util.Random;

@Component
public class AlipayOrderIdUtils {


    @Autowired
    private RedisTemplate redisTemplate;
    /**
     * 生成支付订单号
     * @return
     */
    public String alipayOrderCreate() throws InterruptedException {
        Long time = new Date().getTime();
        Integer random = (int) ((Math.random() * 9 + 1) * 10000);
        Integer random1 = random;
        return ""+time+random1;
    }
}
