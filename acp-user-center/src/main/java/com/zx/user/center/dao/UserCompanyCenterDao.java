package com.zx.user.center.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zx.user.center.entity.CompanyTotalDaily;
import com.zx.user.center.entity.CompanyTotalMonth;
import com.zx.user.center.entity.CompanyTotalYear;
import com.zx.user.center.entity.UserCompanyCenterEntity;
import com.zx.user.center.res.Result;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author
 * @email
 * @date 2020-09-16 18:40:42
 */
@Mapper
@Repository
public interface UserCompanyCenterDao extends BaseMapper<UserCompanyCenterEntity> {

    // 企业注册
    int saveCompany(UserCompanyCenterEntity userCompanyCenterEntity);

    // 根据昵称查询信息
    UserCompanyCenterEntity findLoginName(String loginname);

    // 根据昵称查询数量
    int loginNameCount(String loginname);

    void putBalance(@Param("loginname") String loginname, @Param("balance") BigDecimal balance);



    //查询企业用户会员日增长数
    List<CompanyTotalDaily> QueryMemberDailyGrowthC(LocalDateTime start, LocalDateTime end);

//    //查询企业用户会员月增长数
    List<CompanyTotalMonth> QueryMemberMonthGrowthC(String year);

    //查询企业用户会员年增长数
    List<CompanyTotalYear> QueryMemberYearGrowthC();
}
