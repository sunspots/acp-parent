package com.zx.user.center.entity;


import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2020-09-16 18:40:42
 */
@Data
@Accessors(chain = true)
@TableName("acp_user_gitf")
public class UserGitfEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户兑换礼品id
	 */
	@TableId(type = IdType.ASSIGN_ID)
	private Long userGitfId;
    /**
     * 兑换礼品的用户的id
     */
	private Long userId;
	/**
	 * 用户兑换礼品名字
	 */
	private String userGitfName;
	/**
	 * 用户兑换礼品图片
	 */
	private String userGitfImg;
	/**
	 * 消耗积分
	 */
	private String userGitfIntegral;
	/**
	 * 礼品状态（1：代发货 2：待收货 3：已收货）
	 */
	private Integer userGitfStatus;
	/**
	 * 状态（0：删除 1：未删除） 默认0
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateTime;
	/**
	 * 预留字段1
	 */
	private String text1;
	/**
	 * 预留字段2
	 */
	private String text2;
	/**
	 * 预留字段3
	 */
	private String text3;

}
