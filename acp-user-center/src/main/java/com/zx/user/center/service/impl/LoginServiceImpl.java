package com.zx.user.center.service.impl;

import com.zx.user.center.dao.UserCompanyCenterDao;
import com.zx.user.center.dao.UserPersonalCenterDao;
import com.zx.user.center.entity.UserCompanyCenterEntity;
import com.zx.user.center.entity.UserPersonalCenterEntity;
import com.zx.user.center.res.Result;
import com.zx.user.center.res.ResultCode;
import com.zx.user.center.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional
public class LoginServiceImpl implements LoginService {

    @Resource
    private UserPersonalCenterDao personalUserCenterDao;

    @Resource
    private UserCompanyCenterDao userCompanyCenterDao;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    /**
     * 登录
     *
     * @param loginname
     * @param password
     * @return
     */
    @Override
    public Result login(String loginname, String password) {
        // 个人
        UserPersonalCenterEntity userPersonalCenterEntity = personalUserCenterDao.findLoginName(loginname);
        if (userPersonalCenterEntity != null) {
            // 密码对比
            if (!bCryptPasswordEncoder.matches(password, userPersonalCenterEntity.getPassword()))
                return new Result(ResultCode.FAIL, "密码错误");
        } else {
            // 公司
            UserCompanyCenterEntity userCompanyCenterEntity = userCompanyCenterDao.findLoginName(loginname);
            if (userCompanyCenterEntity == null)
                return new Result(ResultCode.FAIL, "登录名不存在");
            // 密码比对
            if (!bCryptPasswordEncoder.matches(password, userCompanyCenterEntity.getPassword()))
                return new Result(ResultCode.FAIL, "密码错误");
        }

        return new Result(ResultCode.SUCCESS, "登录成功");
    }

    /**
     * 判断昵称是否存在
     *
     * @param loginname
     * @return
     */
    @Override
    public Result loginNameExist(String loginname) {

        if (personalUserCenterDao.loginNameCount(loginname) > 0 ||
                userCompanyCenterDao.loginNameCount(loginname) > 0)
            return new Result(ResultCode.FAIL, "昵称已存在");

        return new Result(ResultCode.SUCCESS, "昵称不存在，通过");
    }
}
