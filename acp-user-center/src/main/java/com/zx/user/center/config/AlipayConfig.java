package com.zx.user.center.config;

import java.io.FileWriter;
import java.io.IOException;

public class AlipayConfig {


//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

        // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
        public static String app_id = "2021000116670426";

        // 商户私钥，您的PKCS8格式RSA2私钥
        public static String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCMEtJmTLuAakwBJGZhJL0Mth+wbvWX1UrZDbejBio9Jm+u+/2YU2yW9rO7Rxg6R03+0vcH72BOo14wo0ZEEyDPHHDMQSbdwoYpwVMy5f9R9e3ctJneWmJ0aKhjuOSZO9p1jehSxlCd2xBVp+FhHwRR4xjxnSWVVlXngVWp79JPVSmBPsfbfRH05pCYBjrbUsvWWg1UGinmvUGA6c8lZojNLpIsuoLKJ0gx+CNtbcvwQGiMtHlWbCrIz1t3GVveXUwW3vapVnBCkjX783Atbm2mdrRslR6fdhbRsEO9mlW8FCBAXbeGbxxDFIczGUWMlkzxEvJTjfK11lLOr1xFiQebAgMBAAECggEAOz12mGdZYV0TWeRQHEOSZL7dthunaJh/eD7R7HOeB5Cq6UGPpg2aPlqF9u2BdJxVgzEr4sI9Y+S54C4deIrydEVd+zSOwHkVjrFBg6Q8ECjHeXE0JhLp0ru0g8BdheUHZnIm0GMpEynjxaYjrpSUmiFGmmDQ1VDXMfrEBQP1WTFVOle7QUmmWXsKqPVVkqu4jftgji43cP52yIQJbCo9rSzRp/ajsHQqrEBpn2eOHEFyW9QofV70pVP44K8MGDbYs5y98Lb1sOfRqXpyXcTNbD+RBa2WX++KpSwj+ZW5e7XKTJrUb4bo9IcsB1nMCurNOaGc8hEpWR36/r6LHah1iQKBgQDSInV19RpHf0aGVFToi+3awHo+xmsOvfCoxOCrTtU4I//v1F5nP+y5rF/0fhK5h+bUCsPgXOMFYZdNT5lTUpM7ieNRcEtYUx+P3hGo8MtYo6HFbUdrCoagmQOeoqoZdLnvqjZlIAw2/aQ5WgYUDwkJtgNBWc+08HJIaLy1QbsG3QKBgQCqpZwq9bzDnT6wbH+PqDBBnmLTpY1vMpvjDJrVIBTsXGy4tUE+HpUBeMrTnkdWrvShxKqq0s3fYYFaGPtD0lYFji36RlzlFP9gMiUqxgPNSD3yYB7+VbbdvlMGT0OqXszuXlaHG5HgL/HiCcBb/4QUnI9p3BWtOMjx9jZkXVEU1wKBgQC5c730l5dUyEgGn4C8J1qI99wrcNPlE87EM9UQeyeEP4lOMUALoYgbqbyZu3ZtFTqrosCST4fwJNvyWDC6L6NPJXdgVVm5nsq5HsS5M6JTHT7VTYd9ObHtJH3kUC0NAE5k7PTKtMjy81EvaHr60vYoAuHGmCvOxXIm44C/+z3DZQKBgCWVZ/nGOZbTsRTmMIoRPt+U5Ks8kRPmj5LIGenKsREFoiRkN0RZM/QpCHLnL0sWKWbfi78nwpPqQ7T2P5DGaI+8qr+uc5M4tq4XHTx4dCnoOUyCgxD9C9N2sY54Mqz5SCVPvbrwBYbcyGdCQo2AjzSC7ssf395Q3WD9Gymrq8ntAoGBAJeaXAFB/u+Q5zkbF7hgB77DtXrSi/NA2Vz9ECN7ZmguejhFCgL+73+TuOaLk0jalWRWmOzrKd83/dS02fuVsxz415YNjdJcYXGI4hiuu9BxBQwZ9NTiHamlaRdOJUuYJZTaQ2fm8Rv7zwi/jaVJCHOV8yMNAAJxC4mMFFwg1TEr";

        // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
        public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzWap+ydJP5VDiDHhMiUgJ97f8eMR+AGQbcBWc4EeLmlKPMQgaGMv5LsIX1G02nj0ySanr4ByltVIQdVmElMPGq9ryAy2aByp1Gz1Jve9mY+C8ggzq5JITjO1eImEqWtU2+ioSIWVl7Kez69B6zoxN/4qA9CUudi05Bzx5re9BtwqJ9q9iNNNyh5kUJ4disptAEuhJAyQMRDtK+xQjcTZ5ERn3e3vXJTo2ZPLCH5oFbbJok4BdeFLxxrs2Er60aoWtaCQ0+GMu8O+ypFj3f279UIeLbtnb8u0uiIt4Icn2ne5zcNDLWjyQq6sZOtYQgScU3IVguucAvIV171vN/Y8AwIDAQAB";

        // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
        public static String notify_url = "http://d2jqc2.natappfree.cc/notifyUrl";

        // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
        public static String return_url = "http://d2jqc2.natappfree.cc/returnUrl";

        // 签名方式
        public static String sign_type = "RSA2";

        // 字符编码格式
        public static String charset = "utf-8";

        // 支付宝网关
        public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

        // 支付宝网关
        public static String log_path = "D:";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

        /**
         * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
         * @param sWord 要写入日志里的文本内容
         */
        public static void logResult(String sWord) {
            FileWriter writer = null;
            try {
                writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
                writer.write(sWord);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
}
