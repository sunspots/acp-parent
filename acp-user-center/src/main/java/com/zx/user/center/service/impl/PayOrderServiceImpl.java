package com.zx.user.center.service.impl;

import com.zx.user.center.dao.PayOrderDao;
import com.zx.user.center.entity.PayOrderEntity;
import com.zx.user.center.service.PayOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PayOrderServiceImpl implements PayOrderService {

    @Autowired
    private PayOrderDao payOrderDao;

    /**
     * 支付生成支付订单
     * @param payOrderEntity
     */
    @Override
    public void createOrder(PayOrderEntity payOrderEntity) {
        payOrderDao.createOrder(payOrderEntity);
    }

    /**
     *  修改订单支付状态为已支付，并且返回该订单的用户id
     * @param outTradeNo
     * @return
     */
    @Transactional
    @Override
    public String getLoginname(String outTradeNo,String gmtPayment) {
        // 修改订单支付状态
        payOrderDao.putPayStatus(outTradeNo,gmtPayment);
        // 修改订单的付款时间
        String loginname = payOrderDao.getLoginname(outTradeNo);
        return loginname;
    }
}
