package com.zx.user.center.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zx.user.center.entity.UserIntegrationChangeEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2020-09-16 18:40:42
 */
@Mapper
public interface UserIntegrationChangeDao extends BaseMapper<UserIntegrationChangeEntity> {
	
}
