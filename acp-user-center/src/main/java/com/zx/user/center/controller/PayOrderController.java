package com.zx.user.center.controller;

import com.zx.user.center.entity.PayOrderEntity;
import com.zx.user.center.service.PayOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 支付订单
 */
@RestController
@RequestMapping("/order")
public class PayOrderController {

    @Autowired
    private PayOrderService payOrderService;

    @PostMapping("/post/order")
    public void createOrder(PayOrderEntity payOrderEntity){

        payOrderService.createOrder(payOrderEntity);
    }
}
