//年报表
function getYearsData() {
    $.ajax({
        type: "GET",
        async: false, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
        url: "http://localhost:16000/personal/QueryMemberYearGrowthP",    //请求发送到TestServlet处
        data: {},
        headers: {"Authorization": sessionStorage.getItem("Bearer")},
        dataType: "json",//返回数据形式为json
        success: function (result) {
            //请求成功时执行该函数内容，result即为服务器返回的json对象
            if (result.code == 200) {
                generateChart(result.data);
            } else {
                alert("查询错误")
            }
        }
    })
}

// 渲染默认年份的数据
function defaultGetMonthDates() {
    $.ajax({
        type: "GET",
        async: false, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
        url: "http://localhost:16000/personal/QueryMemberMonthGrowthP",    //请求发送到TestServlet处
        data: {year: new Date().getFullYear()},
        headers: {"Authorization": sessionStorage.getItem("Bearer")},
        dataType: "json",//返回数据形式为json
        success: function (result) {
            console.log(result);
            //请求成功时执行该函数内容，result即为服务器返回的json对象
            if (result.code == 200) {
                generateChart(result.data);
            } else {
                alert("查询错误")
            }
        }
    })
}


//获取默认天的时间
function defaultGetDaysData() {
    $.ajax({
        type: "GET",
        async: false, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
        url: "http://localhost:16000/personal/QueryMemberDayGrowthP",    //请求发送到TestServlet处
        data: {
            start: getDay(-10),
            end: new Date().format(date_format_string)
        },

        headers: {"Authorization": sessionStorage.getItem("Bearer")},
        dataType: "json",//返回数据形式为json
        success: function (result) {
            //请求成功时执行该函数内容，result即为服务器返回的json对象
            if (result.code == 200) {
                generateChart(result.data);
            } else {
                alert("查询错误")
            }
        }
    })
}

//月报表时的laydata组件
function layDataMonths() {
    layui.use(['laydate'], function () {
        var laydate = layui.laydate;
        //执行一个laydate实例
        laydate.render({
            elem: '#UserYearsDate', //指定元素
            range: false,
            isInitValue: true, //是否允许填充初始值，默认为 true
            value: new Date().getFullYear(),
            type: 'year',
            max: 'new Date().getFullYear()',
            change: function (date1) {
                getMonthsData(date1);
            },
        });
    })
}

//月报表(选择时间后加载数据)
function getMonthsData(date) {
    $.ajax({
        type: "GET",
        async: false, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
        url: "http://localhost:16000/personal/QueryMemberMonthGrowthP",    //请求发送到TestServlet处
        data: {year: date},
        headers: {"Authorization": sessionStorage.getItem("Bearer")},
        dataType: "json",//返回数据形式为json
        success: function (result) {
            //请求成功时执行该函数内容，result即为服务器返回的json对象
            if (result.code == 200) {
                generateChart(result.data);
            } else {
                alert("查询错误")
            }
        }
    })
}


//日报表时的laydata组件
function layDataDays() {
    layui.use(['laydate'], function () {
        var laydate = layui.laydate;
        //执行一个laydate实例
        laydate.render({
            elem: '#UserYearsDate', //指定元素
            range: true,
            isInitValue: true, //是否允许填充初始值，默认为 true
            type: 'date',
            max: 'new Date().toLocaleDateString()',
            format: 'yyyy-MM-dd',
            change: function (value, date, endDate) {
                getDaysData(value)
            },
        });
    });
}

//日报表(选择时间后加载数据)
function getDaysData(dates) {
    var dateArray = dates.split(" - ");
    var date1 = dateArray[0]
    var endDate1 = dateArray[1]
    $.ajax({
        type: "GET",
        async: false, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
        url: "http://localhost:16000/personal/QueryMemberDayGrowthP",    //请求发送到TestServlet处
        data: {
            start: date1,
            end: endDate1,
        },
        headers: {"Authorization": sessionStorage.getItem("Bearer")},
        dataType: "json",//返回数据形式为json
        success: function (result) {
            //请求成功时执行该函数内容，result即为服务器返回的json对象
            if (result.code == 200) {
                generateChart(result.data);
            } else {
                alert("查询错误")
            }
        }
    })
}


