let date_format_string = 'yyyy-MM-dd hh:mm:ss';

// 渲染默认年份的数据
function defaultGetMonthData() {
    $.ajax({
        type: "GET",
        async: false, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
        url: "http://localhost:16000/company/QueryMemberMonthGrowthC",    //请求发送到TestServlet处
        data: {year: new Date().getFullYear()},
        headers: {"Authorization": sessionStorage.getItem("Bearer")},
        dataType: "json",//返回数据形式为json
        success: function (result) {
            console.log("*****")
            console.log(result);
            //请求成功时执行该函数内容，result即为服务器返回的json对象
            if (result.code == 200) {
                console.log(result);
                generateChart(result.data);
            } else {
                alert("查询错误")
            }
        }
    })
}

//获取默认天的时间
function defaultGetDayData() {
    $.ajax({
        type: "GET",
        async: false, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
        url: "http://localhost:16000/company/QueryMemberDayGrowthC",    //请求发送到TestServlet处
        data: {
            start: getDay(-10),
            end: new Date().format(date_format_string)
        },
        headers: {"Authorization": sessionStorage.getItem("Bearer")},
        dataType: "json",//返回数据形式为json
        success: function (result) {
            //请求成功时执行该函数内容，result即为服务器返回的json对象
            if (result.code == 200) {
                generateChart(result.data);
            } else {
                alert("查询错误")
            }
        }
    })
}

//获取近几天时间的方法
function getDay(day) {
    var today = new Date();
    var targetday_milliseconds = today.getTime() + 1000 * 60 * 60 * 24 * day;
    today.setTime(targetday_milliseconds); //注意，这行是关键代码
    var tYear = today.getFullYear();
    var tMonth = today.getMonth();
    var tDate = today.getDate();
    var tHour = today.getHours();
    var tMinutes = today.getMinutes();
    var tSeconds = today.getSeconds();
    tMonth = doHandleMonth(tMonth + 1);
    return tYear + "-" + tMonth + "-" + tDate + " " + tHour + ":" + tMinutes + ":" + tSeconds;
}

function doHandleMonth(month) {
    var m = month;
    if (month.toString().length == 1) {
        m = "0" + month;
    }
    return m;
}

// * 时间格式化处理
Date.prototype.format = function dateFtt(fmt) {
    var o = {
        "M+": this.getMonth() + 1,     //月份
        "d+": this.getDate(),     //日
        "h+": this.getHours(),     //小时
        "m+": this.getMinutes(),     //分
        "s+": this.getSeconds(),     //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds()    //毫秒
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

// echarts数据渲染
function generateChart(data) {

    //指定图表的配置项和数据
    myChart.clear();

    //饼图数据
    var pieData = []
    for (var i = 0; i <= data.xAxisList.length; i++) {
        pieData[i] = {
            name: data.xAxisList[i],
            value: data.yAxisList[i]
        };
    }

    var option = {
        title: {
            text: '公司用户会员年增长图',
            left: "center",
            top: 2,
        },
        //提示框组件
        tooltip: {
            trigger: 'axis', //折线图坐标轴触发
            trigger: 'item'//饼图 触发
        },
        //横轴
        xAxis: {
            name: '时间',
            nameLocation: 'end',
            data: data.xAxisList,
            axisTick: {
                show: false // 去除刻度线
            },
            axisLine: {
                show: true, // 去除轴线
                onZero: false //y轴是负数时也能显示
            },
            axisLabel: {
                color: "#4c9bfd", // 文本颜色
            },
        },
        //纵轴
        yAxis: {
            name: '增长量',
            nameLocation: 'end'
        },
        axisLabel: {
            color: "#4c9bfd" // 文本颜色
        },
        grid: [//指定坐标轴位置，大小
            {x: '10%', y: '10%', width: '50%', height: '30%'},
            {x: '60%', bottom: '2%', height: '90%', width: '35%', contianLabel: true}//关系图位置
        ],

        //系列列表。每个系列通过type决定自己的图表类型
        series: [
            {
                name: '增长量',
                //折线图
                type: 'line',
                data: data.yAxisList//处理小数点数据
            },
            {
                name: '增长量',
                //饼图
                type: 'pie',
                radius: [30, 80],
                center: ['30%', '75%'],//位置确定：左下角
                data: pieData
            }
        ]
    };
    //使用刚指定的配置项和数据显示图表
    // myChart.setOption(option, true);
    myChart.setOption(option);
}

//月报表时的laydata组件
function layDataMonth() {
    layui.use(['laydate'], function () {
        var laydate = layui.laydate;
        //执行一个laydate实例
        laydate.render({
            elem: '#yearsDate', //指定元素
            range: false,
            isInitValue: true, //是否允许填充初始值，默认为 true
            value: new Date().getFullYear(),
            type: 'year',
            max: 'new Date().getFullYear()',
            change: function (date1) {
                getMonthData(date1);
            },
        });
    })
}

//日报表时的laydata组件
function layDataDay() {
    layui.use(['laydate'], function () {
        var laydate = layui.laydate;
        //执行一个laydate实例
        laydate.render({
            elem: '#yearsDate', //指定元素
            range: true,
            isInitValue: true, //是否允许填充初始值，默认为 true
            type: 'date',
            max: 'new Date().toLocaleDateString()',
            format: 'yyyy-MM-dd',
            change: function (value, date, endDate) {
                getDayData(value)
            },
        });
    });
}

// 年报表
function getYearData() {
    console.log(sessionStorage.getItem("Bearer"));
    $.ajax({
        type: "GET",
        async: false, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
        url: "http://localhost:16000/company/QueryMemberYearGrowthC",    //请求发送到TestServlet处
        data: {},
        headers: {"Authorization": sessionStorage.getItem("Bearer")},
        dataType: "json",//返回数据形式为json
        success: function (result) {
            //请求成功时执行该函数内容，result即为服务器返回的json对象
            if (result.code == 200) {
                generateChart(result.data);
            } else {
                alert("查询错误")
            }
        }
    })
}

//月报表(选择时间后加载数据)
function getMonthData(date) {
    console.log(date);
    $.ajax({
        type: "GET",
        async: false, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
        url: "http://localhost:16000/company/QueryMemberMonthGrowthC",    //请求发送到TestServlet处
        data: {year: date},
        headers: {"Authorization": sessionStorage.getItem("Bearer")},
        dataType: "json",//返回数据形式为json
        success: function (result) {
            //请求成功时执行该函数内容，result即为服务器返回的json对象
            if (result.code == 200) {
                generateChart(result.data);
            } else {
                alert("查询错误")
            }
        }
    })
}

//日报表(选择时间后加载数据)
function getDayData(dates) {
    var dateArray = dates.split(" - ");
    var date1 = dateArray[0]
    var endDate1 = dateArray[1]
    $.ajax({
        type: "GET",
        async: false, //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
        url: "http://localhost:16000/company/QueryMemberDayGrowthC",    //请求发送到TestServlet处
        data: {
            start: date1,
            end: endDate1,
        },
        headers: {"Authorization": sessionStorage.getItem("Bearer")},
        dataType: "json",//返回数据形式为json
        success: function (result) {
            //请求成功时执行该函数内容，result即为服务器返回的json对象
            if (result.code == 200) {
                generateChart(result.data);
            } else {
                alert("查询错误")
            }
        }
    })
}
