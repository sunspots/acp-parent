package com.news.center.mapper;

import com.news.center.entity.AcpNewsSupport;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
public interface AcpNewsSupportMapper extends BaseMapper<AcpNewsSupport> {

}
