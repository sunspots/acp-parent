package com.news.center.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.news.center.dto.NewsDto;
import com.news.center.entity.AcpNewsDetails;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
public interface AcpNewsDetailsMapper extends BaseMapper<AcpNewsDetails> {

    // 分页查询
    List<NewsDto> findAllPage(Page<NewsDto> page, NewsDto newsDto);

    // 根据新闻ID修改新闻状态
    int updateNewsStatus(AcpNewsDetails acpNewsDetails);
}
