package com.news.center.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PageDto {
    // 页码
    private int page;

    // 显示条数
    private int pageSize;
}
