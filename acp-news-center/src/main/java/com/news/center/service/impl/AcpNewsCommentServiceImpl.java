package com.news.center.service.impl;

import com.news.center.entity.AcpNewsComment;
import com.news.center.mapper.AcpNewsCommentMapper;
import com.news.center.service.IAcpNewsCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
@Service
public class AcpNewsCommentServiceImpl extends ServiceImpl<AcpNewsCommentMapper, AcpNewsComment> implements IAcpNewsCommentService {

}
