package com.news.center.service;

import com.news.center.entity.AcpNewsCommentSupport;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
public interface IAcpNewsCommentSupportService extends IService<AcpNewsCommentSupport> {

}
