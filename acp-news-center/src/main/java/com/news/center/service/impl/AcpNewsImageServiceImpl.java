package com.news.center.service.impl;

import com.news.center.entity.AcpNewsImage;
import com.news.center.mapper.AcpNewsImageMapper;
import com.news.center.service.IAcpNewsImageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
@Service
public class AcpNewsImageServiceImpl extends ServiceImpl<AcpNewsImageMapper, AcpNewsImage> implements IAcpNewsImageService {

}
