package com.news.center.service;

import com.news.center.common.Result;
import com.news.center.dto.PageDto;
import com.news.center.entity.AcpNewsType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
public interface IAcpNewsTypeService extends IService<AcpNewsType> {

    // 添加
    Result saveNewsType(AcpNewsType acpNewsType);

    // 分页查询
    Result findPageNewType(String newsTypeName, PageDto pageDto);

    // 删除
    Result deleteNewsType(Long newsTypeId);

    // 查询所有
    Result findAllNewsType(String newsTypeName);

    // 修改
    Result modifyNewsType(AcpNewsType acpNewsType);
}
