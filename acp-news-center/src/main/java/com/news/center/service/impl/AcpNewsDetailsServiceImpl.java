package com.news.center.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.news.center.common.Result;
import com.news.center.common.ResultCode;
import com.news.center.dto.NewsDto;
import com.news.center.dto.PageDto;
import com.news.center.entity.AcpNewsDetails;
import com.news.center.entity.SysUser;
import com.news.center.feign.SysUserFeign;
import com.news.center.mapper.AcpNewsDetailsMapper;
import com.news.center.service.IAcpNewsDetailsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
@Service
@Transactional
public class AcpNewsDetailsServiceImpl extends ServiceImpl<AcpNewsDetailsMapper, AcpNewsDetails> implements IAcpNewsDetailsService {

    @Resource
    private AcpNewsDetailsMapper acpNewsDetailsMapper;

    @Autowired
    private SysUserFeign sysUserFeign;

    /**
     * 保存
     * @param acpNewsDetails
     * @return
     */
    @Override
    public Result saveNews(AcpNewsDetails acpNewsDetails) {
        if (acpNewsDetailsMapper.insert(acpNewsDetails) > 0)
            return new Result(ResultCode.SUCCESS,"保存成功");
        else
            return new Result(ResultCode.FAIL,"保存失败");
    }

    /**
     * 发布
     * @param acpNewsDetails
     * @return
     */
    @Override
    public Result releaseNews(AcpNewsDetails acpNewsDetails) {
       if (acpNewsDetailsMapper.insert(acpNewsDetails) > 0)
           return new Result(ResultCode.SUCCESS,"发布成功");
       else
           return new Result(ResultCode.FAIL,"发布失败");
    }

    /**
     * 分页查询
     * @param pageDto
     * @return
     */
    @Override
    public Result findAllPage(PageDto pageDto , NewsDto newsDto) {
        Page<NewsDto> page = new Page<>(pageDto.getPage(),pageDto.getPageSize());
        Page<NewsDto> newsDtoPage = page.setRecords(acpNewsDetailsMapper.findAllPage(page,newsDto));
        List<NewsDto> list = newsDtoPage.getRecords();
        for (NewsDto dto : list) {
            // 调用用户服务获取用户昵称
            SysUser userById = sysUserFeign.findUserById(dto.getAuthorId());
            dto.setAuthName(userById.getNickname());
        }
        return new Result(ResultCode.SUCCESS, "查询成功", newsDtoPage.getTotal(), list);
    }

    /**
     * 发布与下架
     * @param newsStatus
     * @param newsId
     * @return
     */
    @Override
    public Result newsStatus(Long newsStatus, Long newsId) {
        AcpNewsDetails acpNewsDetails = new AcpNewsDetails();
        acpNewsDetails.setNewsStatus(newsStatus);
        acpNewsDetails.setNewsId(newsId);
        if (acpNewsDetailsMapper.updateNewsStatus(acpNewsDetails) > 0)
            return new Result(ResultCode.SUCCESS,"修改成功");
        else
            return new Result(ResultCode.FAIL,"修改失败");
    }

    /**
     * 逻辑删除
     * @param newsId
     * @return
     */
    @Override
    public Result deleteNews(Long newsId) {
        if (acpNewsDetailsMapper.deleteById(newsId) > 0)
            return new Result(ResultCode.SUCCESS,"修改成功");
        else
            return new Result(ResultCode.FAIL,"修改失败");
    }


}
