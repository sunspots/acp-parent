package com.news.center.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.news.center.common.Result;
import com.news.center.common.ResultCode;
import com.news.center.dto.PageDto;
import com.news.center.entity.AcpNewsType;
import com.news.center.mapper.AcpNewsTypeMapper;
import com.news.center.service.IAcpNewsTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author
 * @since 2020-09-17
 */
@Service
@Transactional
public class AcpNewsTypeServiceImpl extends ServiceImpl<AcpNewsTypeMapper, AcpNewsType> implements IAcpNewsTypeService {

    @Resource
    private AcpNewsTypeMapper acpNewsTypeMapper;

    /**
     * 新闻类型添加
     *
     * @param acpNewsType
     * @return
     */
    @Override
    public Result saveNewsType(AcpNewsType acpNewsType) {
        if (acpNewsTypeMapper.saveNewsType(acpNewsType) > 0)
            return new Result(ResultCode.SUCCESS, "添加成功");
        else
            return new Result(ResultCode.FAIL, "添加失败");
    }

    /**
     * 修改新闻类型
     * @param acpNewsType
     * @return
     */
    @Override
    public Result modifyNewsType(AcpNewsType acpNewsType) {
        if (acpNewsTypeMapper.modifyNewsType(acpNewsType) > 0)
            return new Result(ResultCode.SUCCESS, "修改成功");
        else
            return new Result(ResultCode.FAIL, "修改失败");
    }

    /**
     * 分页查询
     *
     * @param newsTypeName
     * @param pageDto
     * @return
     */
    @Override
    public Result findPageNewType(String newsTypeName, PageDto pageDto) {
        Page<AcpNewsType> page = new Page(pageDto.getPage(), pageDto.getPageSize());
        Page<AcpNewsType> userPage = acpNewsTypeMapper.selectPage(page, new QueryWrapper<AcpNewsType>()
                .like(StringUtils.isNotBlank(newsTypeName), "news_type_name", newsTypeName));
        List<AcpNewsType> list = userPage.getRecords();
        return new Result(ResultCode.SUCCESS, "查询成功", userPage.getTotal(), list);
    }

    /**
     * 删除
     *
     * @param newsTypeId
     * @return
     */
    @Override
    public Result deleteNewsType(Long newsTypeId) {
        if (acpNewsTypeMapper.deleteById(newsTypeId) > 0)
            return new Result(ResultCode.SUCCESS, "删除成功");
        else
            return new Result(ResultCode.FAIL, "删除失败");
    }

    /**
     * 查询所有
     *
     * @param newsTypeName
     * @return
     */
    @Override
    public Result findAllNewsType(String newsTypeName) {
        List<AcpNewsType> list = acpNewsTypeMapper.selectList(new QueryWrapper<AcpNewsType>()
                .like(StringUtils.isNotBlank(newsTypeName), "news_type_name", newsTypeName));
        if (list.size() > 0)
            return new Result(ResultCode.SUCCESS,"查询成功",list);
        else
            return new Result(ResultCode.FAIL,"暂无数据");
    }

}
