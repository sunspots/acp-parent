package com.news.center.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AcpNewsSupport extends Model<AcpNewsSupport> {

    private static final long serialVersionUID = 1L;

    /**
     * 新闻id
     */
    private Long newsId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 是否点赞(0未点赞，1已点赞)
     */
    private Integer supportStatus;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
