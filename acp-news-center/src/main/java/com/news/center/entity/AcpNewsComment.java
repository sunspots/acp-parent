package com.news.center.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AcpNewsComment extends Model<AcpNewsComment> {

    private static final long serialVersionUID = 1L;

    /**
     * 评论id
     */
    @TableId(value = "comment_id", type = IdType.AUTO)
    private Long commentId;

    /**
     * 新闻id
     */
    private Long newsId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 评论内容
     */
    private String commentContent;

    /**
     * 评论点赞数
     */
    private Long commentSupportNumber;

    /**
     * 评论树（1一及评论，2一级评论的子评论）
     */
    private Long commentLevel;

    /**
     * 2级评论的父级评论，comment_level=2时，字段显示上级评论的id
     */
    private Long parentId;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 用于逻辑删除（0未删除，1删除）
     */
    private Integer statue;

    /**
     * 预留字段1
     */
    private String text1;

    /**
     * 预留字段2
     */
    private String text2;

    /**
     * 预留字段3
     */
    private String text3;


    @Override
    protected Serializable pkVal() {
        return this.commentId;
    }

}
