package com.news.center.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AcpNewsCommentSupport extends Model<AcpNewsCommentSupport> {

    private static final long serialVersionUID = 1L;

    /**
     * 评论id
     */
    private Long commentId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 评论是否点赞(0未点赞，1已点赞)
     */
    private Integer commentStatus;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
