package com.news.center.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AcpNewsDetails extends Model<AcpNewsDetails> {

    private static final long serialVersionUID = 1L;

    /**
     * 新闻id
     */
    @TableId(value = "news_id", type = IdType.AUTO)
    private Long newsId;

    /**
     * 新闻标题
     */
    private String newsTitle;

    /**
     * 作者（后台管理发布者id）
     */
    private Long authorId;

    /**
     * 新闻来源
     */
    private String newsSource;

    /**
     * 新闻类型
     */
    private Long newsType;
    /**
     * 新闻浏览量
     */
    private Long newsViews;

    /**
     * 新闻内容(富文本)
     */
    private String newsContent;

    /**
     * 点赞数
     */
    private Long newsSupportNumber;

    /**
     * 收藏数
     */
    private Long newsCollectionsNumber;

    /**
     * 新闻状态（1发布，2保存，3下架）
     */
    private Long newsStatus;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 逻辑删除(0，未删除，1，删除)
     */
    @TableLogic(value = "0",delval = "1")
    private Integer status;

    /**
     * 预留字段1
     */
    private String text1;

    /**
     * 预留字段2
     */
    private String text2;

    /**
     * 预留字段3
     */
    private String text3;


    @Override
    protected Serializable pkVal() {
        return this.newsId;
    }

}
