package com.news.center.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AcpNewsCollections extends Model<AcpNewsCollections> {

    private static final long serialVersionUID = 1L;

    /**
     * 新闻id
     */
    private Long newsId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 是否收藏(0未收藏，1已收藏)
     */
    private Integer collectionsStatus;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
