package com.news.center.controller;


import com.news.center.common.Result;
import com.news.center.dto.NewsDto;
import com.news.center.dto.PageDto;
import com.news.center.entity.AcpNewsDetails;
import com.news.center.entity.AcpNewsType;
import com.news.center.service.IAcpNewsDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2020-09-17
 */
@Controller
@RequestMapping("/acpNewsDetails")
@ResponseBody
public class AcpNewsDetailsController {

    @Autowired
    private IAcpNewsDetailsService iAcpNewsDetailsService;

    @GetMapping("list")
    private List<AcpNewsDetails> list(){
        return iAcpNewsDetailsService.list();
    }

    /**
     * 保存
     * @param acpNewsDetails
     * @return
     */
    @PostMapping("/saveNews")
    public Result saveNews(@RequestBody AcpNewsDetails acpNewsDetails){
        return iAcpNewsDetailsService.saveNews(acpNewsDetails);
    }

    /**
     * 发布
     * @param acpNewsDetails
     * @return
     */
    @PostMapping("/releaseNews")
    public Result releaseNews(@RequestBody AcpNewsDetails acpNewsDetails){
        return iAcpNewsDetailsService.releaseNews(acpNewsDetails);
    }

    /**
     * 发布与下架 改变状态
     * @param newsStatus
     * @param newsId
     * @return
     */
    @PutMapping("/releaseOrShelf/{newsStatus}/{newsId}")
    public Result newsStatus(@PathVariable("newsStatus") Long newsStatus ,@PathVariable("newsId") Long newsId ){
        return iAcpNewsDetailsService.newsStatus(newsStatus,newsId);
    }

    @DeleteMapping("/deleteNews/{newsId}")
    public Result deleteNews(@PathVariable("newsId") Long newsId){
        return iAcpNewsDetailsService.deleteNews(newsId);
    }


    /**
     * 分页查询
     * @param pageDto
     * @return
     */
    @PostMapping("/findAllPage")
    public Result findAllPage(PageDto pageDto, NewsDto newsDto){
        return iAcpNewsDetailsService.findAllPage(pageDto,newsDto);
    }
}
