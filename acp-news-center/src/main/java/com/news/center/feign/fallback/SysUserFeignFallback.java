package com.news.center.feign.fallback;

import com.news.center.entity.SysUser;
import com.news.center.feign.SysUserFeign;
import org.springframework.stereotype.Component;

@Component
public class SysUserFeignFallback implements SysUserFeign {

    @Override
    public SysUser findUserById(Long id) {
        return null;
    }
}
