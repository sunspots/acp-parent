package com.news.center.feign;

import com.news.center.entity.SysUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;

/**
 * 用户中心服务
 */
@Component
@FeignClient(name = "user-center")
public interface SysUserFeign {
    @GetMapping("/users/{id}")
    SysUser findUserById(@PathVariable Long id);

}
