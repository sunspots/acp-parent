package com.zx;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.news.center.mapper")
//@EnableFeignClients(basePackages = "com.zx.report.feign")
public class ReportApplication {
    public static void main(String[] args){
        SpringApplication.run(ReportApplication.class,args);
    }

}
