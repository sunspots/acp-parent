package com.zx.acpactivity.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-22
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Accessors(chain = true)
public class AcpActivityNextNode extends Model<AcpActivityNextNode> {

    private static final long serialVersionUID=1L;

    private Long nodeTypeId;

    private Long nextNodeTypeId;

}
