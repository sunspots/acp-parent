package com.zx.acpactivity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Accessors(chain = true)
public class AcpActivityTemplate extends Model<AcpActivityTemplate> {

    private static final long serialVersionUID=1L;

    /**
     * 活动模板id
     */
    @TableId(value = "activity_template_id", type = IdType.AUTO)
    private Long activityTemplateId;

    /**
     * 活动类型id
     */
    private Long activityTypeId;

    /**
     * 活动类型名称
     */
    private String activityTypeName;

    /**
     * 空活动名称
     */
    private String name;

    /**
     * 活动主题，简述
     */
    private String topic;

    /**
     * 活动描述
     */
    private String description;

    /**
     * 活动海报
     */
    private String poster;

    /**
     * 活动广告
     */
    private String ad;

    /**
     * 允许参与人数
     */
    private Integer number;

    /**
     * 活动时长，单位：天
     */
    private BigDecimal duration;

    /**
     * 活动节点模板状态：0=>提交待审核，1=>审核未通过，2=>审核通过
     */
    private Integer activityTemplateStatus;

    /**
     * 审核人id
     */
    private Long staffId;

    /**
     * 审核人名称
     */
    private String staffName;

    /**
     * 审核未通过理由
     */
    private String reviewedInfo;

    /**
     * 模板价格
     */
    private BigDecimal price;

    /**
     * 活动模板收藏数量
     */
    private Integer favoritesNum;

    /**
     * 活动模板被使用次数
     */
    private Integer usedNum;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除的状态，0=>未删除，1=>删除
     */
    private Integer status;

    /**
     * 预留字段1
     */
    private String text1;

    /**
     * 预留字段2
     */
    private String text2;

    /**
     * 预留字段3
     */
    private String text3;


}
