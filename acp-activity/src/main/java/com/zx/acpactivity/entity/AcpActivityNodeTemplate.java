package com.zx.acpactivity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.zx.acpactivity.controller.valid.NodeTemSave;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Accessors(chain = true)
public class AcpActivityNodeTemplate extends Model<AcpActivityNodeTemplate> {

    private static final long serialVersionUID = 1L;

    /**
     * 活动节点模板id
     */
    @TableId(value = "node_template_id", type = IdType.AUTO)
    private Long nodeTemplateId;

    /**
     * 活动节点名称
     */
    @NotBlank(message = "活动节点名称不能为空", groups = {NodeTemSave.class})
    private String name;

    /**
     * 活动模板id
     */
    private Long activityTemplateId;

    /**
     * 活动模板name
     */
    private String activityTemplateName;

    /**
     * 活动节点类型id
     */
    @NotNull(message = "活动节点类型id不能为空", groups = {NodeTemSave.class})
    private Long nodeTypeId;

    /**
     * 活动节点类型name
     */
    private String nodeTypeName;


    /**
     * 活动节点内容
     */
    @NotBlank(message = "活动节点内容不能为空", groups = {NodeTemSave.class})
    private String content;

    /**
     * 被使用次数，默认0
     */
    private Integer usedNum;

    /**
     * 活动节点模板状态：0=>提交待审核，1=>审核未通过，2=>审核通过
     */
    private Integer nodeStatus;

    /**
     * 审核人id
     */
    private Long staffId;

    /**
     * 审核人名字
     */
    private String staffName;

    /**
     * 审核（不通过）理由
     */
    private String reviewedInfo;

    /**
     * 此节点可能关联的节点id集合，用逗号隔开
     */
    private String nextNodeIds;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 逻辑删除的状态，0=>未删除，1=>删除
     */
    private Integer status;

    /**
     * 预留字段1
     */
    private String text1;

    /**
     * 预留字段2
     */
    private String text2;

    /**
     * 预留字段3
     */
    private String text3;

}
