package com.zx.acpactivity.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 活动excel导入活动成员
 * Created by IntelliJ IDEA.
 * com.zx.acpactivity.entity
 * name：AcpUploadMember
 * user：pengpeng
 * date:2020/10/26
 * time:22:11
 */
@Data
@Accessors(chain = true)
public class AcpUploadMember {

    private String name;
    private String number;
    private String address;
    private Integer age;
}
