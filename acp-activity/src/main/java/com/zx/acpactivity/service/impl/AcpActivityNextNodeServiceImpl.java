package com.zx.acpactivity.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zx.acpactivity.entity.AcpActivityNextNode;
import com.zx.acpactivity.mapper.AcpActivityNextNodeMapper;
import com.zx.acpactivity.service.AcpActivityNextNodeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-22
 */
@Service
public class AcpActivityNextNodeServiceImpl extends ServiceImpl<AcpActivityNextNodeMapper, AcpActivityNextNode> implements AcpActivityNextNodeService {

    @Autowired
    private AcpActivityNextNodeMapper acpActivityNextNodeMapper;


    @Override
    @Transactional
    public boolean mySave(Long nodeTypeId, List<Long> nextNodeTypeIds) {
        acpActivityNextNodeMapper.delete(new UpdateWrapper<AcpActivityNextNode>().lambda().eq(AcpActivityNextNode::getNodeTypeId, nodeTypeId));
        if (nextNodeTypeIds != null && nextNodeTypeIds.size() > 0)
            return saveBatch(nextNodeTypeIds.stream()
                    .map(id -> new AcpActivityNextNode().setNodeTypeId(nodeTypeId).setNextNodeTypeId(id))
                    .collect(Collectors.toList()));
        return true;
    }

    @Override
    public List<Long> findByNodeTypeId(Long nodeTypeId) {
        return acpActivityNextNodeMapper.selectList(new QueryWrapper<AcpActivityNextNode>().lambda().eq(AcpActivityNextNode::getNodeTypeId, nodeTypeId))
                .stream()
                .map(AcpActivityNextNode::getNextNodeTypeId)
                .collect(Collectors.toList());
    }

}
