package com.zx.acpactivity.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zx.acpactivity.entity.AcpActivityNodeTemplate;
import com.zx.acpactivity.entity.AcpActivityTemplate;
import com.zx.acpactivity.mapper.AcpActivityTemplateMapper;
import com.zx.acpactivity.service.AcpActivityNodeTemplateService;
import com.zx.acpactivity.service.AcpActivityTemplateService;
import com.zx.acpactivity.utils.PageResult;
import com.zx.acpactivity.utils.Result;
import com.zx.acpactivity.vo.AcpActivityTemplateVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */
@Service
public class AcpActivityTemplateServiceImpl extends ServiceImpl<AcpActivityTemplateMapper, AcpActivityTemplate> implements AcpActivityTemplateService {

    @Resource
    private AcpActivityTemplateMapper templateMapper;

    @Resource
    private AcpActivityNodeTemplateService nodeTemplateService;


    @Override
    public PageResult<AcpActivityTemplate> findActTemps(AcpActivityTemplateVO activityTemplateVO) {
        Page<AcpActivityTemplate> page = new Page<>(activityTemplateVO.getPage(), activityTemplateVO.getLimit());
        QueryWrapper<AcpActivityTemplate> queryWrapper = new QueryWrapper<>();
        String name = activityTemplateVO.getName();
        if (!StringUtils.isBlank(name)) {
            queryWrapper.like("name", name);
        }
        Integer number = activityTemplateVO.getNumber();
        if (number != null && number != 0) {
            queryWrapper.eq("number", number);
        }
        BigDecimal duration = activityTemplateVO.getDuration();
        if (duration != null) {
            queryWrapper.eq("duration", duration);
        }
        BigDecimal price = activityTemplateVO.getPrice();
        if (price != null) {
            queryWrapper.eq("price", price);
        }
        Page<AcpActivityTemplate> templatePage = templateMapper.selectPage(page, queryWrapper);
        List<AcpActivityTemplate> list = templatePage.getRecords();
        long total = page.getTotal();
        return PageResult.<AcpActivityTemplate>builder().data(list).code(0).count(total).build();
    }

    @Override
    @Transactional
    public void addActTemp(AcpActivityTemplateVO activityTemplateVO) {
        //添加 activityTemplate
        templateMapper.insert(activityTemplateVO);

        //操作活动节点表
        Long activityTemplateId = activityTemplateVO.getActivityTemplateId();
        String name = activityTemplateVO.getName();
        activityTemplateVO.getNodeList()
                .forEach(node -> {
                    node.setActivityTemplateId(activityTemplateId);
                    node.setActivityTemplateName(name);
                });

        nodeTemplateService.saveBatch(activityTemplateVO.getNodeList());
    }

    @Override
    public Result<List<AcpActivityNodeTemplate>> getNodeTemplateByActTempById(Long id) {
        List<AcpActivityNodeTemplate> nodeList = nodeTemplateService
                .list(new QueryWrapper<AcpActivityNodeTemplate>()
                        .eq("activity_template_id", id));
        return Result.succeed(nodeList);
    }

    @Override
    @Transactional
    public void updateActTemp(AcpActivityTemplateVO activityTemplateVO) {
        //更新活动模板表
        this.updateById(activityTemplateVO);

        //更新活动节点模板表
        //操作活动节点表
        Long activityTemplateId = activityTemplateVO.getActivityTemplateId();
        String name = activityTemplateVO.getName();
        activityTemplateVO.getNodeList()
                .forEach(node -> {
                    node.setActivityTemplateId(activityTemplateId);
                    node.setActivityTemplateName(name);
                });
        nodeTemplateService.saveOrUpdateBatch(activityTemplateVO.getNodeList());
    }

    @Override
    @Transactional
    public void deleteActTempById(Long id) {
        //根据活动模板id删除活动模板表
        this.removeById(id);
        //根据活动模板id删除它的节点模板
        nodeTemplateService.remove(
                new UpdateWrapper<AcpActivityNodeTemplate>()
                        .eq("activity_template_id", id));
    }
}
