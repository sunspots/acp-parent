package com.zx.acpactivity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zx.acpactivity.entity.AcpActivityParticipant;

/**
 * 操作 活动——参与者 关联表的服务类
 */
public interface AcpActivityParticipantService extends IService<AcpActivityParticipant> {

    boolean insert(AcpActivityParticipant acpActivityParticipant);
}
