package com.zx.acpactivity.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zx.acpactivity.entity.*;
import com.zx.acpactivity.feign.AliMessageFeignService;
import com.zx.acpactivity.feign.UserFeignService;
import com.zx.acpactivity.mapper.AcpActivityMapper;
import com.zx.acpactivity.mapper.AcpActivityNodeMapper;
import com.zx.acpactivity.mapper.AcpActivityParticipantMapper;
import com.zx.acpactivity.mapper.AcpActivityTypeMapper;
import com.zx.acpactivity.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zx.acpactivity.utils.PageResult;
import com.zx.acpactivity.utils.Result;
import com.zx.acpactivity.vo.AcpActivityVO;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ClassUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */
@Service

public class AcpActivityServiceImpl extends ServiceImpl<AcpActivityMapper, AcpActivity> implements AcpActivityService {

    private static final Logger log = LoggerFactory.getLogger(AcpActivityServiceImpl.class);
    @Resource
    AcpActivityMapper acpActivityMapper;
    @Autowired
    AcpActivityNodeService acpActivityNodeService;
    @Autowired
    AcpActivityHistoryService acpActivityHistoryService;
    @Autowired
    AcpActivityNodeHistoryService acpActivityNodeHistoryService;
    @Autowired
    AcpActivityParticipantService acpActivityParticipantService;
    @Autowired
    RedisTemplate redisTemplate;
    @Resource
    AcpActivityParticipantMapper acpActivityParticipantMapper;
    @Resource
    AcpActivityNodeMapper acpActivityNodeMapper;
    @Autowired
    AliMessageFeignService aliMessageFeignService;
    @Autowired
    UserFeignService userFeignService;
    @Autowired
    AcpActivityMemberInfoService acpActivityMemberInfoService;
    @Autowired
    AcpActivityTypeMapper acpActivityTypeMapper;
    //活动成员excel模板
    private static final String EXCEL_NUMBER_TEMPLATE = "excel/用户成员模板.xlsx";
    //活动成员上传临时路径
    private static final String EXCEL_NUMBER_DIR = "/excel/upload_temp/";

    private static final byte PRIVATEAC = 0;

    /**
     * 活动发布/创建
     *
     * @param acpActivity
     * @param acpActivityNodeList
     * @return
     */
    @Transactional
    @Override
    public boolean creat(AcpActivity acpActivity, List<AcpActivityNode> acpActivityNodeList) throws Exception {
        //保存acpActivity成功后插入acpActivityNode
        if (0 != acpActivityMapper.insert(acpActivity)) {
            acpActivityNodeList.stream().forEach(node -> {
                //活动节点设置活动id
                node.setActivityId(acpActivity.getActivityId());
                //设置 node 状态
                //上传开始时间是否在当前时间之后，若是：已经开始，判断结束时间
                //结束时间在当前时间之后，已经结束，这是不合法的，暂时不进行活动逻辑是否合法检验
                boolean afterBegin = node.getStartTime().isAfter(LocalDateTime.now());
                boolean afterEnd = node.getEndTime().isAfter(LocalDateTime.now());
                if (afterEnd)
                    node.setNodeStatus(2);//活动结束
                if (afterBegin && !afterEnd)
                    node.setNodeStatus(1);//活动正在进行中
                if (!afterBegin)
                    node.setNodeStatus(0);//活动未结束
                /**
                 * 这里之进行了节点状态、活动id的设置
                 */
                if (acpActivityNodeService.insert(node)) {
                    throw new RuntimeException("活动节点保存失败");
                }
            });
        } else {
            throw new RuntimeException("活动保存失败");
        }
        return true;
    }

    @Transactional
    @Override
    public boolean creat(AcpActivityVO acpActivityVo) throws Exception {
        List<AcpActivityNode> acpActivityNodeList = acpActivityVo.getNodeList();
        AcpActivity acpActivity = new AcpActivity();
        BeanUtils.copyProperties(acpActivityVo, acpActivity);
        //保存acpActivity成功后插入acpActivityNode
        if (0 != acpActivityMapper.insert(acpActivity)) {

            acpActivityNodeList.stream().forEach(node -> {
                //活动节点设置活动id
                node.setActivityId(acpActivity.getActivityId());
                //设置 node 状态
                //上传开始时间是否在当前时间之后，若是：已经开始，判断结束时间
                //结束时间在当前时间之后，已经结束，这是不合法的，暂时不进行活动逻辑是否合法检验
                boolean afterBegin = node.getStartTime().isAfter(LocalDateTime.now());
                boolean afterEnd = LocalDateTime.now().isAfter(node.getEndTime());
                if (!afterBegin)
                    node.setNodeStatus(0);//活动未结束
                if (afterEnd)
                    node.setNodeStatus(2);//活动结束
                if (afterBegin && !afterEnd)
                    node.setNodeStatus(1);//活动正在进行中

                /**
                 * 顺序未进行解决，这个留给查询的时间根据 node 时间进行排序吧
                 * 这里之进行了节点状态、活动id的设置
                 */
                if (!acpActivityNodeService.insert(node)) {
                    throw new RuntimeException("活动节点保存失败");
                }
            });
        } else {
            throw new RuntimeException("活动保存失败");
        }
        return true;
    }

    /**
     * 审核活动
     * 上传数据：活动id(activityId)，审核理由(activityInfo)，审核人id(staffId)
     *
     * @param acpActivity
     * @return
     */
    @Transactional
    @Override
    public boolean checked(AcpActivity acpActivity) {
        if (null == acpActivity.getActivityId())
            return false;
        if (null == acpActivity.getStaffId())
            return false;
        AcpActivity acpActivityById = acpActivityMapper.selectById(acpActivity.getActivityId());
        if (null == acpActivityById)
            return false;
        if (PRIVATEAC == acpActivityById.getShowStatus()) {
            //设置邀请码
            String codestring = UUID.randomUUID().toString().replace("-", "").substring(0, 5);
            acpActivityById.setCreateTime(LocalDateTime.now()).setCode(codestring + acpActivity.getActivityId());
        }
        acpActivityById
                .setUpdateTime(LocalDateTime.now())
                .setStaffId(acpActivity.getStaffId())
                .setActivityStatus(acpActivity.getActivityStatus())
                .setStatusInfo(acpActivity.getStatusInfo());
        if (0 != acpActivityMapper.updateById(acpActivityById))
            return true;
        return false;
    }

    @Transactional
    @Override
    public boolean deleteById(Long id) throws Exception {
        if (null == id || id < 0)
            return false;
        AcpActivity acpActivity = acpActivityMapper.selectById(id);
        if (null == acpActivity)
            return false;
        acpActivity.setStatus(1).setUpdateTime(LocalDateTime.now());
        AcpActivityHistory acpActivityHistory = new AcpActivityHistory();
        BeanUtils.copyProperties(acpActivity, acpActivityHistory);
        //删除活动表中活动
        if (0 != acpActivityMapper.deleteById(id)) {
            //转移到历史表中
            if (!acpActivityHistoryService.insert(acpActivityHistory)) {
                throw new RuntimeException("acpActivityHistoryService 在迁移已被删除数据时失败");
            }
            //删除活动的节点，转移到历史表中
            List<AcpActivityNode> acpActivityNodeList = acpActivityNodeService.getByActivityId(acpActivity.getActivityId());
            if (!CollectionUtils.isEmpty(acpActivityNodeList)) {
                //活动节点不为空，将节点转移到历史表中，并且删除节点
                acpActivityNodeList.stream().forEach(node -> {
                    node.setNodeStatus(3).setUpdateTime(LocalDateTime.now()).setStatus(1);
                    if (!acpActivityNodeService.deleteById(node.getNodeId())) {
                        throw new RuntimeException("acpActivityNodeService 在删除数据时失败");
                    }
                    AcpActivityNodeHistory nodeHistory = new AcpActivityNodeHistory();
                    BeanUtils.copyProperties(node, nodeHistory);
                    //转移到node 历史版本中
                    if (!acpActivityNodeHistoryService.insert(nodeHistory)) {
                        throw new RuntimeException("acpActivityNodeHistoryService 在插入数据时失败");
                    }
                });
            }
        } else {
            throw new RuntimeException("acpActivityHistoryService 在迁移已被删除数据时失败");
        }
        return true;
    }

    /**
     * 下架活动，直接放入到历史版本中，删除状态不设置
     *
     * @param id
     * @param code
     * @return
     */
    @Override
    public Result revokeByIdAndCode(Long id, String code) throws Exception {
        String s = redisTemplate.opsForValue().get("revokeAcpCode" + id).toString();
        if (s == null || code == null || s != code)
            return Result.notFound("验证码错误，请重试");
        if (null == id || id < 0)
            return Result.notFound("活动信息错误");
        AcpActivity acpActivity = acpActivityMapper.selectById(id);
        if (null == acpActivity)
            return Result.notFound("活动信息错误");
        acpActivity.setUpdateTime(LocalDateTime.now());
        AcpActivityHistory acpActivityHistory = new AcpActivityHistory();
        BeanUtils.copyProperties(acpActivity, acpActivityHistory);
        //删除活动表中活动
        if (0 != acpActivityMapper.deleteById(id)) {
            //转移到历史表中
            if (!acpActivityHistoryService.insert(acpActivityHistory)) {
                throw new RuntimeException("acpActivityHistoryService 在迁移已被删除数据时失败");
            }
            //删除活动的节点，转移到历史表中
            List<AcpActivityNode> acpActivityNodeList = acpActivityNodeService.getByActivityId(acpActivity.getActivityId());
            if (!CollectionUtils.isEmpty(acpActivityNodeList)) {
                //活动节点不为空，将节点转移到历史表中，并且删除节点
                acpActivityNodeList.stream().forEach(node -> {
                    node.setNodeStatus(3).setUpdateTime(LocalDateTime.now());
                    if (!acpActivityNodeService.deleteById(node.getNodeId())) {
                        throw new RuntimeException("acpActivityNodeService 在删除数据时失败");
                    }
                    AcpActivityNodeHistory nodeHistory = new AcpActivityNodeHistory();
                    BeanUtils.copyProperties(node, nodeHistory);
                    //转移到node 历史版本中
                    if (!acpActivityNodeHistoryService.insert(nodeHistory)) {
                        throw new RuntimeException("acpActivityNodeHistoryService 在插入数据时失败");
                    }
                });
            }
        } else {
            throw new RuntimeException("acpActivityHistoryService 在迁移已被删除数据时失败");
        }
        /**
         * todo 记录执行人信息，活动撤销时间
         */
        log.info("{} {}将活动 {}下架成功", LocalDateTime.now(), null, id);
        return Result.succeed();
    }

    /**
     * 获取下架验证码
     *
     * @param id
     * @return
     */
    @Override
    public Result getRevokeCodeById(Long id) {

        AcpActivity acpActivity = acpActivityMapper.selectById(id);
        if (null == acpActivity)
            return Result.notFound();
        //生成6位数随机验证码
        String msg = RandomStringUtils.random(6, true, true);
        /**
         *  活动下架验证码  验证码存储于 redis中,并发送验证码
         */
        redisTemplate.opsForValue().set("revokeAcpCode" + id, msg);
        aliMessageFeignService.sendMsg(acpActivity.getUserPhone(), msg);
        return Result.succeed("验证码发送成功");
    }

    /**
     * 修改活动 ：
     * 审核中的活动可以修改活动的全部内容，或者撤销此次活动，重新发布新的活动。撤销使用单独接口
     *
     * @param acpActivityVo
     * @return
     * @throws Exception
     */
    @Transactional
    @Override
    public Result updateByVo(AcpActivityVO acpActivityVo) throws Exception {
        AcpActivity acpActivity = selectById(acpActivityVo.getActivityId());
        if (null == acpActivity)
            return Result.notFound();
        switch (acpActivity.getActivityStatus()) {
            case 0:
            case 1:
            case 3:
                //保存未提交、提交未审核的活动，修改全部内容，撤销
                //审核未通过的活动，修改全部内容
                if (updateAllInfo(acpActivityVo))
                    return Result.succeed();
                return Result.notFound();
            case 2:
                //修改审核通过的活动，修改部分内容
                return updateCheckedAC(acpActivityVo);

            case 4:
                //活动中的活动，仅能修改部分参数
                return updateUngoing(acpActivityVo);

            case 5:
            case 6:
                //结束的活动不允许修改内容，可对活动进行评价
                Result<Object> failed = Result.failed();
                failed.setMsg("不允许修改已经结束的活动");
                return failed;

        }

       /* acpActivity.setUpdateTime(LocalDateTime.now())
                .setDescription(acpActivityVo.getDescription())
                .setEndTime(acpActivityVo.getEndTime())
                .setLocation(acpActivityVo.getLocation())
                .setStartTime(acpActivityVo.getStartTime())
                .setPoster(acpActivityVo.getPoster())
                .setName(acpActivityVo.getName())
                .setLocation(acpActivityVo.getLocation())
                .setAd(acpActivityVo.getAd())
                .setActivityTypeName(acpActivityVo.getActivityTypeName());
        List<AcpActivityNode> nodeByActivityId = acpActivityNodeService.getByActivityId(acpActivity.getActivityId());
        List<AcpActivityNode> nodeByActivityVO = acpActivityVo.getNodeList();
        //修改节点
        nodeByActivityId.stream().forEach(node -> {
            nodeByActivityVO.stream().forEach(nodeByVO -> {
                if (node.getNodeId() == nodeByVO.getNodeId()) {
                    node.setNodeTypeName(nodeByVO.getNodeTypeName())
                            .setStartTime(nodeByVO.getStartTime())
                            .setSite(nodeByVO.getSite())
                            .setPrincipalPhone(nodeByVO.getPrincipalPhone())
                            .setNodeTypeName(nodeByVO.getNodeTypeName())
                            .setName(nodeByVO.getName())
                            .setEndTime(nodeByVO.getEndTime())
                            .setContent(nodeByVO.getContent())
                            .setUpdateTime(LocalDateTime.now());
                }
            });
        });
        if (0 != acpActivityMapper.updateById(acpActivity))
            if (acpActivityNodeService.updateBatchById(nodeByActivityId))
                return Result.succeed();*/
        /**
         * todo 修改活动，短信通知活动人员
         */
        return Result.notFound();
    }

    /**
     * 进行中的活动，仅允许修改部分信息，例如：奖品信息，活动结束信息
     *
     * @param acpActivityVo
     * @return
     */
    @Transactional
    @Override
    public Result updateUngoing(AcpActivityVO acpActivityVo) {
        /**
         *  修改正在进行中的活动
         *  暂时允许修改结束时间与联系人电话
         */
        AcpActivity acpActivity = acpActivityMapper.selectById(acpActivityVo.getActivityId());
        if (null == acpActivity)
            return Result.notFound();
        acpActivity.setUpdateTime(LocalDateTime.now())
                .setUserPhone(acpActivityVo.getUserPhone())
                .setEndTime(acpActivityVo.getEndTime());
       /* acpActivity.setUpdateTime(LocalDateTime.now())
                .setDescription(acpActivityVo.getDescription())
                .setEndTime(acpActivityVo.getEndTime())
                .setLocation(acpActivityVo.getLocation())
                .setStartTime(acpActivityVo.getStartTime())
                .setPoster(acpActivityVo.getPoster())
                .setName(acpActivityVo.getName())
                .setLocation(acpActivityVo.getLocation())
                .setAd(acpActivityVo.getAd())
                .setActivityTypeName(acpActivityVo.getActivityTypeName())
                .setTopic(acpActivityVo.getTopic())
                .setText1(acpActivityVo.getText1())
        ;
*/
        List<AcpActivityNode> nodeByActivityId = acpActivityNodeService.getByActivityId(acpActivity.getActivityId());
        List<AcpActivityNode> nodeByActivityVO = acpActivityVo.getNodeList();
        //修改节点
        nodeByActivityId.stream().forEach(node -> {
            nodeByActivityVO.stream().forEach(nodeByVO -> {
                if (node.getNodeId() == nodeByVO.getNodeId()) {
                    if (nodeByVO.getEndTime().isAfter(LocalDateTime.now())) {
                        //结束时间必须在当前时间之后
                        node.setEndTime(nodeByVO.getEndTime())
                                .setUpdateTime(LocalDateTime.now());
                    }
                }
            });
        });
        if (0 != acpActivityMapper.updateById(acpActivity))
            if (acpActivityNodeService.updateBatchById(nodeByActivityId))
                return Result.succeed();
        /**
         * todo 修改活动，短信通知活动人员
         */
        return Result.succeed();
    }

    /**
     * 通过活动id查询活动详细信息
     * @param id
     * @return
     */
    @Override
    public Result<AcpActivityVO> getAcDetailById(Long id) {
        if (null == id || id < 0)
            return Result.validateFailed();

        AcpActivity acpActivity = acpActivityMapper.selectById(id);
        AcpActivityVO acpActivityVO = new AcpActivityVO();
        BeanUtils.copyProperties(acpActivity, acpActivityVO);

        if (null == acpActivity)
            return Result.notFound();
        List<AcpActivityNode> byActivityId = acpActivityNodeService.getByActivityId(id);
        if (!CollectionUtils.isEmpty(byActivityId))
            acpActivityVO.setNodeList(byActivityId);
        return Result.succeed(acpActivityVO);
    }


    /**
     * 修改通过审核的的活动
     * 仅能修改活动开始结束时间，活动地点，负责人电话
     *
     * @param acpActivityVo
     * @return
     */
    @Transactional
    @Override
    public Result updateCheckedAC(AcpActivityVO acpActivityVo) {
        AcpActivity acpActivity = acpActivityMapper.selectById(acpActivityVo.getActivityId());
        if (null == acpActivity)
            return Result.notFound();
        acpActivity.setUpdateTime(LocalDateTime.now())
                .setLocation(acpActivityVo.getLocation())
                .setEndTime(acpActivityVo.getEndTime())
                .setCreateTime(acpActivityVo.getCreateTime())
                .setUserPhone(acpActivityVo.getUserPhone());

       /* acpActivity.setUpdateTime(LocalDateTime.now())
                .setDescription(acpActivityVo.getDescription())
                .setEndTime(acpActivityVo.getEndTime())
                .setLocation(acpActivityVo.getLocation())
                .setStartTime(acpActivityVo.getStartTime())
                .setPoster(acpActivityVo.getPoster())
                .setName(acpActivityVo.getName())
                .setLocation(acpActivityVo.getLocation())
                .setAd(acpActivityVo.getAd())
                .setActivityTypeName(acpActivityVo.getActivityTypeName())
                .setTopic(acpActivityVo.getTopic())
                .setText1(acpActivityVo.getText1())
        ;
*/
        List<AcpActivityNode> nodeByActivityId = acpActivityNodeService.getByActivityId(acpActivity.getActivityId());
        List<AcpActivityNode> nodeByActivityVO = acpActivityVo.getNodeList();
        //修改节点
        nodeByActivityId.stream().forEach(node -> {
            nodeByActivityVO.stream().forEach(nodeByVO -> {
                if (node.getNodeId() == nodeByVO.getNodeId()) {
                    node.setNodeTypeName(nodeByVO.getNodeTypeName())
                            .setStartTime(nodeByVO.getStartTime())
                            .setSite(nodeByVO.getSite())
                            .setPrincipalPhone(nodeByVO.getPrincipalPhone())
                            .setEndTime(nodeByVO.getEndTime())
                            .setUpdateTime(LocalDateTime.now());
                }
            });
        });
        if (0 != acpActivityMapper.updateById(acpActivity))
            if (acpActivityNodeService.updateBatchById(nodeByActivityId))
                return Result.succeed();
        /**
         * todo 修改活动，短信通知活动人员
         */
        return Result.succeed();
    }

    /**
     * 保存未提交、提交未审核的活动，修改全部内容
     * 审核未通过的活动，修改全部内容
     *
     * @param acpActivityVo
     * @return
     */
    @Transactional
    @Override
    public boolean updateAllInfo(AcpActivityVO acpActivityVo) {
        AcpActivity acpActivity = acpActivityMapper.selectById(acpActivityVo.getActivityId());
        if (null == acpActivity)
            return false;
        acpActivityVo.setCreateTime(acpActivity.getCreateTime())
                .setUserId(acpActivity.getUserId())
                .setUserType(acpActivity.getUserType())
                .setUserName(acpActivity.getUserName())
                .setActivityStatus(acpActivity.getActivityStatus())
                .setCode(acpActivity.getCode())
                .setSignUpNum(acpActivity.getSignUpNum())
                .setStaffId(acpActivity.getStaffId())
                .setStaffId(acpActivity.getStaffId())
                .setStaffName(acpActivity.getStaffName())
                .setStatus(acpActivity.getStatus())
                .setStatusInfo(acpActivity.getStatusInfo());
        AcpActivity acpActivityByUpdate = new AcpActivity();
        BeanUtils.copyProperties(acpActivityVo, acpActivityByUpdate);

       /* acpActivity.setUpdateTime(LocalDateTime.now())
                .setDescription(acpActivityVo.getDescription())
                .setEndTime(acpActivityVo.getEndTime())
                .setLocation(acpActivityVo.getLocation())
                .setStartTime(acpActivityVo.getStartTime())
                .setPoster(acpActivityVo.getPoster())
                .setName(acpActivityVo.getName())
                .setLocation(acpActivityVo.getLocation())
                .setAd(acpActivityVo.getAd())
                .setActivityTypeName(acpActivityVo.getActivityTypeName())
                .setTopic(acpActivityVo.getTopic())
                .setText1(acpActivityVo.getText1())
        ;
*/
        List<AcpActivityNode> nodeByActivityId = acpActivityNodeService.getByActivityId(acpActivity.getActivityId());
        List<AcpActivityNode> nodeByActivityVO = acpActivityVo.getNodeList();
        //修改节点
        nodeByActivityId.stream().forEach(node -> {
            nodeByActivityVO.stream().forEach(nodeByVO -> {
                if (node.getNodeId() == nodeByVO.getNodeId()) {
                    node.setNodeTypeName(nodeByVO.getNodeTypeName())
                            .setStartTime(nodeByVO.getStartTime())
                            .setSite(nodeByVO.getSite())
                            .setPrincipalPhone(nodeByVO.getPrincipalPhone())
                            .setNodeTypeName(nodeByVO.getNodeTypeName())
                            .setName(nodeByVO.getName())
                            .setEndTime(nodeByVO.getEndTime())
                            .setContent(nodeByVO.getContent())
                            .setUpdateTime(LocalDateTime.now())
                            .setText1(nodeByVO.getText1())
                            .setText2(nodeByVO.getText2())
                            .setText3(nodeByVO.getText3());
                }
            });
        });
        if (0 != acpActivityMapper.updateById(acpActivityByUpdate))
            if (acpActivityNodeService.updateBatchById(nodeByActivityId))
                return true;
        /**
         * todo 修改活动，短信通知活动人员
         */
        return false;
    }

    @Override
    public Result<AcpActivity> selectActivityById(Integer id) {
        AcpActivity activity = acpActivityMapper.selectById(id);
        if (null == activity)
            return Result.notFound();
        return Result.succeed(activity);
    }

    @Override
    public AcpActivity selectById(Long id) {
        if (null == id || id < 0)
            return null;
        return acpActivityMapper.selectById(id);
    }


    /**
     * 条件查询活动
     * 可按：活动状态，创建人，创建时间，活动人数，活动类型
     */
    @Override
    public PageResult<AcpActivity> findActivities(AcpActivityVO acpActivityVO) {
        //创建一个条件构造器
        QueryWrapper<AcpActivity> queryWrapper = new QueryWrapper<>();
        //获取分页数据
        if (acpActivityVO.getPage() == null || acpActivityVO.getPage() <= 0) {
            acpActivityVO.setPage(1);
        }
        if (acpActivityVO.getLimit() == null || acpActivityVO.getLimit() <= 0) {
            acpActivityVO.setLimit(10);
        }
        Page<AcpActivity> page = new Page<>(acpActivityVO.getPage(), acpActivityVO.getLimit());
        //添加活动状态（公开/私有）查询条件
        Integer showStatus = acpActivityVO.getShowStatus();
        if (showStatus != null) {
            if (showStatus == 0 || showStatus == 1) {
                queryWrapper.eq("show_status", showStatus);
            }
        }
        //添加活动创建人姓名查询条件
        if (acpActivityVO.getUserName() != null) {
            String userName = acpActivityVO.getUserName();
            queryWrapper.like("user_name", userName);
        }
        if (!StringUtils.isEmpty(acpActivityVO.getName()))
            queryWrapper.like("name", acpActivityVO.getUserName());
        //添加活动创建时间查询条件
        if (acpActivityVO.getEarlyCreateTime() != null) {
            queryWrapper.gt("create_time", acpActivityVO.getEarlyCreateTime());
        }
        if (acpActivityVO.getLateCreateTime() != null) {
            queryWrapper.lt("create_time", acpActivityVO.getLateCreateTime());
        }
        //添加活动人数查询条件
        if (acpActivityVO.getMinNum() != null) {
            queryWrapper.gt("size", acpActivityVO.getMinNum());
        }
        if (acpActivityVO.getMaxNum() != null) {
            queryWrapper.lt("size", acpActivityVO.getMaxNum());
        }
        //添加活动类型查询条件
        if (acpActivityVO.getActivityTypeName() != null) {
            queryWrapper.eq("activity_type_name", acpActivityVO.getActivityTypeName());
        }
        //添加活动类型id查询条件
        if (acpActivityVO.getActivityTypeId() != null) {
            queryWrapper.eq("activity_type_id", acpActivityVO.getActivityTypeId());
        }
        //添加活动状态（提交/审核/进行/结束/...）查询条件
        if (acpActivityVO.getActivityStatus() != null) {
            Integer activityStatus = acpActivityVO.getActivityStatus();
            queryWrapper.eq("activity_status", activityStatus);
        }
        //添加活动名称模糊搜索
        if (acpActivityVO.getName() != null && !"".equals(acpActivityVO.getName())) {
            queryWrapper.like("name", acpActivityVO.getName());
        }
        //添加排序条件，按时间降序（最新创建的展示在最前面）
        queryWrapper.orderByDesc("create_time");
        //按以上条件进行查询
        Page<AcpActivity> activityPage = acpActivityMapper.selectPage(page, queryWrapper);
        List<AcpActivity> list = activityPage.getRecords();
        long total = activityPage.getTotal();
        return PageResult.<AcpActivity>builder().data(list).code(0).count(total).build();
    }

    /**
     *
     * 查询活动的所有节点
     */
    @Override
    public Result<List<AcpActivityNode>> findNodesOfActivity(Long activityId) {
        //匹配活动Id
        HashMap<String, Object> map = new HashMap<>();
        map.put("activity_id", activityId);
        //按以上条件查询
        List<AcpActivityNode> nodes = acpActivityNodeMapper.selectByMap(map);
        if (CollectionUtils.isEmpty(nodes)) {
            return Result.notFound("没有任何节点数据");
        }
        return Result.succeed(nodes);
    }

    /**
     * 根据id查询活动
     */
    @Override
    public Result selectActivityById(Long id) {
        if (id == null || id < 0)
            return Result.notFound();
        AcpActivity activity = acpActivityMapper.selectById(id);
        return Result.succeed(activity);
    }

/**
 * 展示活动，不带节点,需要对活动进行条件查询，默认进行按时间排序
 * 条件：
 * 1. 公开，未公开
 * 2，是否在进行
 * 3，展示个数
 *
 * @param showStatus     1公开，0私有
 * @param activityStatus 0=>保存未提交审核，1=>提交待审核，2=>审核通过，发布状态，活动未开始，3=>审核未通过，4=>活动进行中，5=>活动以结束，6=>活动异常终止
 * @param limit          默认值20条
 * @return
 */
    /*
    @Override
    public Result<List<AcpActivity>> display(Integer showStatus, Integer activityStatus, Integer limit) {
        List<AcpActivity> acpActivityList = acpActivityMapper.display(showStatus, activityStatus, limit);
        if (CollectionUtils.isEmpty(acpActivityList))
            return Result.notFound();
        return Result.succeed(acpActivityList);
    }*/


    /**
     * 展示活动，不带节点,需要对活动进行条件查询，默认进行按时间排序
     * 条件：
     * 1. 公开，未公开
     * 2，是否在进行
     * 3，展示个数
     *
     * @param showStatus     1公开，0私有
     * @param activityStatus 0=>保存未提交审核，1=>提交待审核，2=>审核通过，发布状态，活动未开始，3=>审核未通过，4=>活动进行中，5=>活动以结束，6=>活动异常终止
     * @param limit          默认值20条
     * @return
     */
    @Override
    public Result<List<AcpActivity>> display(Integer showStatus, Integer activityStatus, Integer limit) {
        //添加匹配条件
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("show_status", showStatus);
        queryWrapper.eq("activity_status", activityStatus);
        queryWrapper.orderByDesc("create_time");
        //添加limit条件，使用分页去做
        Page<AcpActivity> page = new Page<>(1, limit);
        //查询
        Page selectPage = acpActivityMapper.selectPage(page, queryWrapper);
        List<AcpActivity> acpActivityList = selectPage.getRecords();
        //判断并返回
        if (CollectionUtils.isEmpty(acpActivityList))
            return Result.notFound();
        return Result.succeed(acpActivityList);
    }

    /**
     * 查询用户发布的活动
     */
    @Override
    public List<AcpActivity> myPolishedActivities(Long userId) {
        QueryWrapper<AcpActivity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        List<AcpActivity> list = acpActivityMapper.selectList(queryWrapper);
        return list;
    }

    /**
     * 查询用户参与的活动
     */
    @Override
    public List<AcpActivity> myEngagedActivities(Long userId) {
        //先在活动参与者关联表中查询出用户参与的活动
        HashMap map = new HashMap();
        map.put("participant_id", userId);
        List<AcpActivityParticipant> acpActivityParticipantList = acpActivityParticipantMapper.selectByMap(map);
        //之后根据查询到的活动集合当中每个活动的id去查询每个活动的详细信息
        List<AcpActivity> list = new ArrayList<>();
        acpActivityParticipantList.stream().forEach(AcpActivityParticipant -> {
            list.add(selectById(AcpActivityParticipant.getActivityId()));
        });
        return list;
    }

    /**
     * 用户根据用户id与活动id参加活动,邀请码可选
     *
     * @param userId
     * @param id
     * @param code
     * @return
     */
    @Transactional
    @Override
    public Result joinAcpByIdAndUserId(Long userId, Long id, String code) {


        if (userId == null || id == null || userId < 0 || id < 0)
            return Result.notFound("活动参与失败，请检查活动信息参与条件");

        // 验证用户是否存在
        Result result = userFeignService.getUserById(id);
        if (null == result || result.getCode() != 200)
            return Result.notFound();

        AcpActivity acpActivity = acpActivityMapper.selectById(id);
        if (null == acpActivity)
            return Result.notFound("活动参与失败，请检查活动信息参与条件");

        //私有活动需要提供邀请码
        if (acpActivity.getShowStatus() == 0) {
            // 邀请码比对,这里假定未固定，且保存于数据库code字段
            if (StringUtils.isEmpty(code) || !code.equals(acpActivity.getCode()))
                return Result.notFound("请检查邀请码");
        }

        synchronized (this) {
            //todo 使用分布式锁，暂时将其关闭，使用synchronized(个人觉得性能足够了)，要使用时再开启
           /*
            //redis 分布式锁key： 格式为 ：distributed_lock:业务逻辑
            String redisKey = "distributed_lock:" + "acp_activity_joinAcp";
            String redisKeyValue = UUID.randomUUID().toString();
           Boolean redisKeyResult = redisTemplate.opsForValue().setIfAbsent(redisKey, redisKeyValue, 30, TimeUnit.SECONDS);
            if (redisKeyResult){
                //业务逻辑
            }*/
            //参加人数达到上限
            if (acpActivity.getSignUpNum() >= acpActivity.getSize())
                return Result.notFound("参与人数达到上限，无法参与活动，请联系活动主办方");
            acpActivity.setSignUpNum(acpActivity.getSignUpNum() + 1);
            if (0 != acpActivityMapper.updateById(acpActivity)) {
                //用户参加信息表加入信息
                if (!acpActivityParticipantService.insert(new AcpActivityParticipant(id, userId)))
                    return Result.notFound("参与活动失败");
                log.info("用户{}参与活动{}成功", userId, id);
                return Result.succeed("参与活动成功");
            }
        }
        return Result.notFound("参与活动失败");
    }

    /**
     * 导入活动成员列表
     * 将页面缓存到本地服务器
     *
     * @param file
     * @return
     */
    @Override
    public String uploadNumberExcel(MultipartFile file) {
        if (file == null)
            return null;
//        String path = ClassUtils.getDefaultClassLoader().getResource(EXCEL_NUMBER_DIR).getPath();
        try {
            //默认保存到资源文件夹下
            String path = ResourceUtils.getURL("classpath:").getPath();
            String realPath = path.replace("/target/classes/", "/src/main/resources/excel/upload_temp/");
            String fileTempName = realPath + UUID.randomUUID().toString().replaceAll("-", "") + file.getOriginalFilename();
            File file1 = new File(fileTempName);
            if (!file1.exists())
                file1.mkdirs();
            file.transferTo(file1);
            return fileTempName;
        } catch (IOException e) {
            log.error("导入用户文件失败");
            e.printStackTrace();
        }
        return null;
    }

    @Transactional
    @Override
    public Result releaseWithExcel(AcpActivityVO acpActivityVO) {
        try {
            //文件链接出错
            if (null == acpActivityVO.getExcelPath())
                return Result.notFound("文件上传失败，请重试");
            XSSFWorkbook xssfWorkbook = new XSSFWorkbook(
                    new FileInputStream(new File(acpActivityVO.getExcelPath())));
            if (creat(acpActivityVO)) {
                //获取工作表
                List<AcpActivityMemberInfo> memberList = parseMember(xssfWorkbook, acpActivityVO.getActivityId());
                if (memberList.size() > acpActivityVO.getSize())
                    throw new RuntimeException("导入人数大于参加活动的人数了！");
                acpActivityMemberInfoService.saveBatch(memberList);
                return Result.succeed();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return Result.notFound("活动发布出错咯~~，请稍后重试~");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.notFound("发布活动错处了哦，请稍后再试");
    }

    @Override
    public List<AcpActivityStatus> countStatus() {
        return acpActivityMapper.countStatus();
    }

    /**
     * 解析上传excel文件的用户信息
     * todo 数据格式未确定是暂时使用资源文件夹下的excel模板
     *
     * @param xssfWorkbook
     * @return
     */
    private List<AcpActivityMemberInfo> parseMember(XSSFWorkbook xssfWorkbook, Long acpId) {
        ArrayList<AcpActivityMemberInfo> list = new ArrayList<>();
        try {
            //得到工作表格对象
            XSSFSheet sheetAt = xssfWorkbook.getSheetAt(0);
            //循环读取表格数数据
            for (Row row : sheetAt) {
                //行首不读
                if (row.getRowNum() == 0) {
                    continue;
                }
                //读取数据从0开始
                String cell;

                String name = row.getCell(0).getStringCellValue();
                CellType cellType = row.getCell(1).getCellType();
                if (cellType == CellType.NUMERIC) {
//                    DataFormatter dataFormatter = new DataFormatter();
//                    dataFormatter.addFormat("###########", null);
//                    cell = dataFormatter.formatCellValue(row.getCell(1));
                    Cell cell1 = row.getCell(1);
                    cell1.setCellType(CellType.STRING);
                    cell = cell1.getStringCellValue();
                } else {
                    cell = row.getCell(1).toString();
                }
                String phone = cell;


                AcpActivityMemberInfo memberInfo = new AcpActivityMemberInfo();
                memberInfo.setName(name).setPhone(phone).setActivityId(acpId);
                list.add(memberInfo);
            }
            //关闭流
            xssfWorkbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }
    /**
     * 根据idPath查询活动
     * @param idPath
     * @return
     */
    @Override
    public Result<List<AcpActivity>> findByIdPath(String idPath) {
        //查询所有活动类型
        List<AcpActivityType> activityTypes = acpActivityTypeMapper.selectList(null);
        //遍历找出所有在idPath下的所有活动id
        List<Long> list = new ArrayList<>();
        for (AcpActivityType activityType : activityTypes) {
            System.out.println(activityType.getIdPath());
            String[] split = activityType.getIdPath().split(",");
            if (split[0].equals(idPath)) {
                list.add(activityType.getActivityTypeId());
            }
        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.in("activity_type_id", list);
        List<AcpActivity> list1 = acpActivityMapper.selectList(queryWrapper);

        Result<List<AcpActivity>> result = new Result<>();
        result.setData(list1);
        result.setCode(200);
        return result;
    }
}
