package com.zx.acpactivity.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zx.acpactivity.entity.AcpActivityNodeTemplate;
import com.zx.acpactivity.entity.AcpActivityTemplate;
import com.zx.acpactivity.utils.PageResult;
import com.zx.acpactivity.utils.Result;
import com.zx.acpactivity.vo.AcpActivityTemplateVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */
public interface AcpActivityTemplateService extends IService<AcpActivityTemplate> {


    PageResult<AcpActivityTemplate> findActTemps(AcpActivityTemplateVO activityTemplateVO);

    void addActTemp(AcpActivityTemplateVO activityTemplateVO);

    Result<List<AcpActivityNodeTemplate>> getNodeTemplateByActTempById(Long id);

    void updateActTemp(AcpActivityTemplateVO activityTemplateVO);

    void deleteActTempById(Long id);
}
