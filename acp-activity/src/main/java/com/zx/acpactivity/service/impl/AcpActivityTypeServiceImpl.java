package com.zx.acpactivity.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zx.acpactivity.entity.AcpActivityTemplate;
import com.zx.acpactivity.entity.AcpActivityType;
import com.zx.acpactivity.entity.FormSelectTree;
import com.zx.acpactivity.mapper.AcpActivityTemplateMapper;
import com.zx.acpactivity.mapper.AcpActivityTypeMapper;
import com.zx.acpactivity.service.AcpActivityTemplateService;
import com.zx.acpactivity.service.AcpActivityTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */
@Service
public class AcpActivityTypeServiceImpl extends ServiceImpl<AcpActivityTypeMapper, AcpActivityType> implements AcpActivityTypeService {

    @Resource
    private AcpActivityTypeMapper activityTypeMapper;

    @Resource
    private AcpActivityTemplateMapper activityTemplateMapper;


    @Override
    @Transactional
    public void mySaveOrUpDate(AcpActivityType acpActivityType) {
        //设置创建时间，修改时间


        Long activityTypeId = acpActivityType.getActivityTypeId();
        Long parentId = acpActivityType.getParentId();
        // 判断传入的值是否有活动类型的主键，没有就是执行添加操作，存在就是执行修改操作
        if (activityTypeId == null) {
            //初始化idPath
            String idPath = "";

            //判断是否为顶级分类，顶级分类就设置层级为1
            if (parentId == null) {
                acpActivityType.setLevel(1);
            } else {  //不是顶级分类，就是查询父节点分类的层级 +1
                AcpActivityType activityType = activityTypeMapper.selectById(parentId);
                acpActivityType.setLevel(activityType.getLevel() + 1);
                idPath = activityType.getIdPath();
            }

            activityTypeMapper.insert(acpActivityType);
            //添加完之后，获取此条数据的主键activityTypeId
            Long id = acpActivityType.getActivityTypeId();

            activityTypeMapper.updateById(new AcpActivityType()
                    .setActivityTypeId(id)
                    .setIdPath("".equals(idPath) ? id.toString() : (idPath + ',' + id)));
        } else { //修改操作
            activityTypeMapper.updateById(acpActivityType);

            //同步修改活动模板关联的活动类型名称（暂时，后面可以考虑定时同步，或者消息队列）
            activityTemplateMapper.update(new AcpActivityTemplate()
                            .setActivityTypeName(acpActivityType.getName()),
                    new UpdateWrapper<AcpActivityTemplate>()
                            .eq("activity_type_id", acpActivityType.getActivityTypeId()));
        }
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        QueryWrapper<AcpActivityType> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id", id);
        List<AcpActivityType> acpActivityTypeList = activityTypeMapper.selectList(queryWrapper);
        if (acpActivityTypeList == null || acpActivityTypeList.size() == 0) {
            activityTypeMapper.deleteById(id);
        }
    }

    @Override
    public List<FormSelectTree> findTypeTree() {

        //创建一个map用于存放 <活动类型id，将活动类型对象封装成的FormSelectTree对象>
        HashMap<Long, FormSelectTree> map = new HashMap<>();
        List<FormSelectTree> trees = new ArrayList<>();
        this.list()
                .stream()
                .peek(type -> {
                    FormSelectTree node = new FormSelectTree().setId(type.getActivityTypeId())
                            .setName(type.getName())
                            .setOpen(true);
                    map.put(type.getActivityTypeId(), node);
                })
                .forEach(type -> {
                    Long parentId = type.getParentId();
                    Long id = type.getActivityTypeId();
                    //判断map中是否存在此parentid
                    if (map.containsKey(parentId)) {
                        //存在，将此父id的孩子取出来，如果孩子为空，进行初始化
                        List<FormSelectTree> children = map.get(parentId).getChildren();
                        if (children == null) { //初始化
                            children = new ArrayList<>();
                            map.get(parentId).setChildren(children);
                        }
                        //将此节点放入到父亲下
                        children.add(map.get(id));
                    } else {  //没有这个parentid，直接添加到trees中
                        trees.add(map.get(id));
                    }
                });
        return trees;
    }

    @Override
    public List<AcpActivityType> findByLevel() {
        return activityTypeMapper.findByLevel();
    }
}
