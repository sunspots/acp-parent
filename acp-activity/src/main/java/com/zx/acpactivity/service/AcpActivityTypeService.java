package com.zx.acpactivity.service;

import com.zx.acpactivity.entity.AcpActivityType;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zx.acpactivity.entity.FormSelectTree;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */
public interface AcpActivityTypeService extends IService<AcpActivityType> {

    void mySaveOrUpDate(AcpActivityType acpActivityType);

    void deleteById(Long id);

    List<FormSelectTree> findTypeTree();

    List<AcpActivityType> findByLevel();
}
