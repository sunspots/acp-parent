package com.zx.acpactivity.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zx.acpactivity.entity.AcpActivityNodeTemplate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */
public interface AcpActivityNodeTemplateService extends IService<AcpActivityNodeTemplate> {

    IPage<AcpActivityNodeTemplate> findBy(AcpActivityNodeTemplate acpActivityNodeTemplate, int page, int limit);

    boolean mySave(AcpActivityNodeTemplate acpActivityNodeTemplate);

    boolean reviewed(Long id,String reviewedInfo);

    String getReviewedInfoById(Long id);
}
