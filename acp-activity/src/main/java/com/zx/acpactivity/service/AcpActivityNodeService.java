package com.zx.acpactivity.service;

import com.zx.acpactivity.entity.AcpActivityNode;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */
public interface AcpActivityNodeService extends IService<AcpActivityNode> {
    /**
     * 通过活动id获取活动的节点
     * @param activityId
     * @return
     */
    List<AcpActivityNode> getByActivityId(Long activityId);

    boolean deleteById(Long nodeId);

    boolean insert(AcpActivityNode node);
}
