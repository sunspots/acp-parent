package com.zx.acpactivity.service;

import com.zx.acpactivity.entity.AcpActivityNextNode;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-22
 */
public interface AcpActivityNextNodeService extends IService<AcpActivityNextNode> {

    boolean mySave(Long nodeTypeId, List<Long> nextNodeTypeIds);

    List<Long> findByNodeTypeId(Long nodeTypeId);
}
