package com.zx.acpactivity.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zx.acpactivity.entity.AcpActivityNodeTemplate;
import com.zx.acpactivity.mapper.AcpActivityNodeTemplateMapper;
import com.zx.acpactivity.service.AcpActivityNodeTemplateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */
@Service
public class AcpActivityNodeTemplateServiceImpl extends ServiceImpl<AcpActivityNodeTemplateMapper, AcpActivityNodeTemplate> implements AcpActivityNodeTemplateService {

    @Autowired
    private AcpActivityNodeTemplateMapper acpActivityNodeTemplateMapper;


    @Override
    public IPage<AcpActivityNodeTemplate> findBy(AcpActivityNodeTemplate acpActivityNodeTemplate, int page, int limit) {
        IPage<AcpActivityNodeTemplate> iPage = new Page<>(page, limit);
        QueryWrapper<AcpActivityNodeTemplate> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(AcpActivityNodeTemplate::getActivityTemplateId, 0)
                .like(StringUtils.isNotBlank(acpActivityNodeTemplate.getName()), AcpActivityNodeTemplate::getName, acpActivityNodeTemplate.getName())
                .like(StringUtils.isNotBlank(acpActivityNodeTemplate.getContent()), AcpActivityNodeTemplate::getContent, acpActivityNodeTemplate.getContent())
                .eq(acpActivityNodeTemplate.getNodeTypeId() != null, AcpActivityNodeTemplate::getNodeTypeId, acpActivityNodeTemplate.getNodeTypeId())
                .eq(acpActivityNodeTemplate.getNodeStatus() != null, AcpActivityNodeTemplate::getNodeStatus, acpActivityNodeTemplate.getNodeStatus())
                .orderByDesc(AcpActivityNodeTemplate::getUpdateTime);
        return acpActivityNodeTemplateMapper.selectPage(iPage, queryWrapper);
    }

    @Override
    @Transactional
    public boolean mySave(AcpActivityNodeTemplate acpActivityNodeTemplate) {
        boolean flag = false;
        if (acpActivityNodeTemplate.getNodeTemplateId() == null) {
            flag = acpActivityNodeTemplateMapper.insert(new AcpActivityNodeTemplate()
                    .setName(acpActivityNodeTemplate.getName())
                    .setContent(acpActivityNodeTemplate.getContent())
                    .setNodeTypeId(acpActivityNodeTemplate.getNodeTypeId())
                    .setNodeTypeName(acpActivityNodeTemplate.getNodeTypeName())
            ) > 0;
        } else {
            //查询要修改的数据能否被修改，修改后，节点模板默认变为待审核状态
            AcpActivityNodeTemplate oldData = acpActivityNodeTemplateMapper.selectById(acpActivityNodeTemplate.getNodeTemplateId());
            if (oldData != null && oldData.getNodeStatus() < 2)
                flag = acpActivityNodeTemplateMapper.updateById(new AcpActivityNodeTemplate()
                        .setNodeTemplateId(acpActivityNodeTemplate.getNodeTemplateId())
                        .setName(acpActivityNodeTemplate.getName())
                        .setContent(acpActivityNodeTemplate.getContent())
                        .setNodeTypeId(acpActivityNodeTemplate.getNodeTypeId())
                        .setNodeTypeName(acpActivityNodeTemplate.getNodeTypeName())
                        .setNodeStatus(0)
                ) > 0;
        }
        return flag;
    }

    @Override
    @Transactional
    public boolean reviewed(Long id, String reviewedInfo) {
        AcpActivityNodeTemplate nodeTem = new AcpActivityNodeTemplate()
                .setNodeTemplateId(id);
//                .setStaffId(StaffId())
//                .setStaffName(StaffName())
        if (StringUtils.isEmpty(reviewedInfo)) {
            nodeTem.setNodeStatus(2);
        } else {
            nodeTem.setNodeStatus(1);
            nodeTem.setReviewedInfo(reviewedInfo);
        }
        UpdateWrapper<AcpActivityNodeTemplate> updateWrapper = new UpdateWrapper<>();
        updateWrapper.lambda().eq(AcpActivityNodeTemplate::getNodeTemplateId, id)
                .eq(AcpActivityNodeTemplate::getNodeStatus, 0);
        return acpActivityNodeTemplateMapper.update(nodeTem, updateWrapper) > 0;
    }

    @Override
    public String getReviewedInfoById(Long id) {
        return acpActivityNodeTemplateMapper.selectById(id).getReviewedInfo();
    }
}
