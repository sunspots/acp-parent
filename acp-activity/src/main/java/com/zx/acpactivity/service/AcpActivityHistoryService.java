package com.zx.acpactivity.service;

import com.zx.acpactivity.entity.AcpActivityHistory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-22
 */
public interface AcpActivityHistoryService extends IService<AcpActivityHistory> {
    boolean insert(AcpActivityHistory acpActivityHistory);

}
