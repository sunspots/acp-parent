package com.zx.acpactivity.service.impl;

import com.zx.acpactivity.entity.AcpActivityMemberInfo;
import com.zx.acpactivity.mapper.AcpActivityMemberInfoMapper;
import com.zx.acpactivity.service.AcpActivityMemberInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */
@Service
public class AcpActivityMemberInfoServiceImpl extends ServiceImpl<AcpActivityMemberInfoMapper, AcpActivityMemberInfo> implements AcpActivityMemberInfoService {

}
