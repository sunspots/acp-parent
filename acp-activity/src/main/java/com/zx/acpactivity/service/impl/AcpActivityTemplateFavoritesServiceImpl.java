package com.zx.acpactivity.service.impl;

import com.zx.acpactivity.entity.AcpActivityTemplateFavorites;
import com.zx.acpactivity.mapper.AcpActivityTemplateFavoritesMapper;
import com.zx.acpactivity.service.AcpActivityTemplateFavoritesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */
@Service
public class AcpActivityTemplateFavoritesServiceImpl extends ServiceImpl<AcpActivityTemplateFavoritesMapper, AcpActivityTemplateFavorites> implements AcpActivityTemplateFavoritesService {

}
