package com.zx.acpactivity.service;

import com.zx.acpactivity.entity.AcpActivityMemberInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */
public interface AcpActivityMemberInfoService extends IService<AcpActivityMemberInfo> {

}
