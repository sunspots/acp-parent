package com.zx.acpactivity.feign;

import com.zx.acpactivity.feign.impl.UserFeignServiceImpl;
import com.zx.acpactivity.utils.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by IntelliJ IDEA.
 * com.zx.acpactivity.feign
 * name：UserFeignService
 * user：pengpeng
 * date:2020/10/22
 * time:9:46
 */
@FeignClient(value = "acp-user-center",fallback = UserFeignServiceImpl.class)
public interface UserFeignService {
    @GetMapping("/personal/getById")
    Result getUserById(@RequestParam Long id);
}
