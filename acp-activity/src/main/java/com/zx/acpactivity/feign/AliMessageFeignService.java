package com.zx.acpactivity.feign;

import com.zx.acpactivity.feign.impl.AliMessageFeignServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by IntelliJ IDEA.
 * com.zx.acpactivity.service
 * name：AliMessageFeignService
 * user：pengpeng
 * date:2020/10/15
 * time:17:53
 */
@FeignClient(value = "acp-thirdparty",fallback = AliMessageFeignServiceImpl.class )
public interface AliMessageFeignService {
    @PostMapping("/ali/sendSms")
    public void sendMsg(@RequestParam String mobile, @RequestParam String code);
}
