package com.zx.acpactivity.mapper;

import com.zx.acpactivity.entity.AcpActivityNodeType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-22
 */
public interface AcpActivityNodeTypeMapper extends BaseMapper<AcpActivityNodeType> {

}
