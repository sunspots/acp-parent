package com.zx.acpactivity.mapper;

import com.zx.acpactivity.entity.AcpActivity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zx.acpactivity.entity.AcpActivityStatus;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hulinbo
 * @since 2020-09-17
 */
public interface AcpActivityMapper extends BaseMapper<AcpActivity> {
    List<AcpActivityStatus> countStatus();
    /**
     * 展示活动，不带节点,需要对活动进行条件查询，默认进行按时间排序
     * 条件：
     * 1. 公开，未公开
     * 2，是否在进行
     * 3，展示个数
     *
     * @param showStatus     1公开，0私有
     * @param activityStatus 0=>保存未提交审核，1=>提交待审核，2=>审核通过，发布状态，活动未开始，3=>审核未通过，4=>活动进行中，5=>活动以结束，6=>活动异常终止
     * @param limit          默认值20条
     * @return
     */
   /* List<AcpActivity> display(@Param("showStatus") Integer showStatus
            ,@Param("activityStatus")Integer activityStatus
            , @Param("limit")Integer limit);*/
}
