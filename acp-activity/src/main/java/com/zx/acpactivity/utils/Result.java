package com.zx.acpactivity.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: hulinbo
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> implements Serializable {

    private Integer code;
    private String msg;
    private T data;

    /**
     * 成功返回结果
     *
     */
    public static <T> Result<T> succeed() {
        return new Result<T>(CodeEnum.SUCCESS.getCode(), CodeEnum.SUCCESS.getMessage(), null);
    }
    /**
     * 成功返回结果
     *
     */
    public static <T> Result<T> succeed(String message) {
        return new Result<T>(CodeEnum.SUCCESS.getCode(), message, null);
    }

    /**
     * 成功返回结果
     *
     * @param data 获取的数据
     */
    public static <T> Result<T> succeed(T data) {
        return new Result<T>(CodeEnum.SUCCESS.getCode(), CodeEnum.SUCCESS.getMessage(), data);
    }

    /**
     * 成功返回结果
     *
     * @param data 获取的数据
     * @param  message 提示信息
     */
    public static <T> Result<T> succeed(String message, T data) {
        return new Result<T>(CodeEnum.SUCCESS.getCode(), message, data);
    }


    /**
     * 失败返回结果
     */
    public static <T> Result<T> failed() {
        return codeEnum(CodeEnum.FAILED);
    }


    /**
     * 失败返回结果
     * @param message 提示信息
     */
    public static <T> Result<T> failed(String message) {
        return new Result<T>(CodeEnum.FAILED.getCode(), message, null);
    }


    /**
     * 参数验证失败返回结果
     */
    public static <T> Result<T> validateFailed() {
        return codeEnum(CodeEnum.VALIDATE_FAILED);
    }

    /**
     * 参数验证失败返回结果
     * @param message 提示信息
     */
    public static <T> Result<T> validateFailed(String message) {
        return new Result<T>(CodeEnum.VALIDATE_FAILED.getCode(), message, null);
    }

    /**
     * 没有查询到相关数据返回结果
     */
    public static <T> Result<T> notFound() {
        return codeEnum(CodeEnum.NOT_FOUND);
    }

    /**
     * 没有查询到相关数据返回结果
     * @param message 提示信息
     */
    public static <T> Result<T> notFound(String message) {
        return new Result<T>(CodeEnum.NOT_FOUND.getCode(), message, null);
    }


    /**
     * 返回结果
     * @param codeEnum
     */
    private static <T> Result<T> codeEnum(CodeEnum codeEnum) {
        return new Result<T>(codeEnum.getCode(), codeEnum.getMessage(), null);
    }

}
