/*
package com.zx.acpactivity.utils;

import com.alibaba.druid.sql.visitor.functions.Isnull;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

*/
/**
 *
 * redis 分布式锁工具类
 * Created by IntelliJ IDEA.
 * com.zx.acpactivity.utils
 * name：RedisLockUtil
 * user：pengpeng
 * date:2020/10/22
 * time:10:30
 *//*

@Component
@Data
public class RedisLockUtil {

    public static final Logger log = LoggerFactory.getLogger(RedisLockUtil.class);

    @Autowired
    private RedisTemplate redisTemplate;
    //是否存在等待竞争锁的线程
    private volatile boolean hasWaitThread = false;

    //持有锁的线程
    private volatile Thread holdLockThread;

    //获取锁的最大次数
    private final int MAX_TRY_NUMBER = 100;

    //锁已经被获取
    private boolean acquiredLock = false;
    //尝试获取锁的次数
    private AtomicInteger atomicInteger = new AtomicInteger(0);

    */
/**
     * 获取redis 锁
     * @param key
     * @param expire
     *//*

    public void tryLock(String key,Long expire){
        this.tryLock(null,key,expire);
    }

    */
/**
     *
     * @param redisTemplate
     * @param key
     * @param expire
     *//*

    public void tryLock(RedisTemplate redisTemplate,String key,Long expire){
        boolean locked = (null==redisTemplate?getLock(key,expire):getLock(redisTemplate,key,expire));
        while (!locked){
           try {
               //todo 逻辑好像有点问题
               //已经有锁在等待，持有锁的线程不为空
               if(hasWaitThread &&holdLockThread!=null){
                   synchronized (holdLockThread){
                       holdLockThread.wait(expire);
                   }
               }else {
                   Thread.sleep(100);
               }
           }catch (InterruptedException e) {
               log.error("进行线程wait时出现错误，线程ID为{} 线程名为{}",holdLockThread.getId(),holdLockThread.getName());
               e.printStackTrace();
           }
        }
        atomicInteger.incrementAndGet();
        this.holdLockThread = Thread.currentThread();
    }

    public boolean freeLock(String key){
        return freeLock(this.redisTemplate,key);
    }

    public boolean freeLock(RedisTemplate redisTemplate,String key){
        try {
            if (null==redisTemplate ||null==key)
                return false;
            return redisTemplate.delete(key);
        }catch (Exception e){
            log.error("删除锁时出现错误，key为{}，线程ID为{} 线程名为{}",key,Thread.currentThread().getId(),Thread.currentThread().getName());
            e.printStackTrace();
        }
        return false;
    }

    */
/**
     *
     * @param key
     * @param expire
     * @return
     *//*

    private boolean getLock(String key, Long expire) {
        try {
            return getLock(this.redisTemplate,key,expire);
        }catch (Exception e){
            log.error("获取锁时引发异常{}",e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    private boolean getLock(RedisTemplate redisTemplate,String key, Long expire) {
        if (null==key||redisTemplate==null||null==expire||expire<=0)
            return false;
        if (redisTemplate.opsForValue().setIfAbsent(key, expire.toString(), expire, TimeUnit.SECONDS)){
            //设置持有锁的线程
            this.holdLockThread = Thread.currentThread();
            this.hasWaitThread = true;
            return true;
        }
        return false;
    }

}
*/
