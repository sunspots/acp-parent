package com.zx.acpactivity.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zx.acpactivity.controller.valid.NodeTemSave;
import com.zx.acpactivity.entity.AcpActivityNodeTemplate;
import com.zx.acpactivity.service.AcpActivityNodeTemplateService;
import com.zx.acpactivity.utils.PageResult;
import com.zx.acpactivity.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;


@RestController
@RequestMapping("/acpActivityNodeTemplate")
public class AcpActivityNodeTemplateController {
    @Autowired
    private AcpActivityNodeTemplateService acpActivityNodeTemplateService;

    @PostMapping("/save")
    public Result<Object> save(@Validated(NodeTemSave.class) @RequestBody AcpActivityNodeTemplate acpActivityNodeTemplate) {
        if (acpActivityNodeTemplateService.mySave(acpActivityNodeTemplate))
            return Result.succeed();
        else
            return Result.failed();
    }

    @GetMapping("/findBy")
    public PageResult<AcpActivityNodeTemplate> findBy(AcpActivityNodeTemplate acpActivityNodeTemplate, @RequestParam(defaultValue = "1") int page,
                                                      @RequestParam(defaultValue = "10") int limit) {
        IPage<AcpActivityNodeTemplate> iPage = acpActivityNodeTemplateService.findBy(acpActivityNodeTemplate, page, limit);
        return PageResult.<AcpActivityNodeTemplate>builder().code(0).count(iPage.getTotal()).data(iPage.getRecords()).build();
    }

    @PostMapping("/reviewed/{id}")
    public Result<Object> reviewed(@PathVariable Long id, @RequestBody(required = false) HashMap<String, String> reviewedInfo) {
        if (acpActivityNodeTemplateService.reviewed(id, reviewedInfo.get("reviewedInfo")))
            return Result.succeed();
        else
            return Result.failed();
    }

    @DeleteMapping("/{id}")
    public Result<Object> delete(@PathVariable Long id) {
        if (acpActivityNodeTemplateService.removeById(id))
            return Result.succeed();
        else
            return Result.failed();
    }

    @GetMapping("/reviewedInfo/{id}")
    public Result<Object> getReviewedInfo(@PathVariable Long id) {
        return Result.succeed(acpActivityNodeTemplateService.getReviewedInfoById(id));
    }

}

