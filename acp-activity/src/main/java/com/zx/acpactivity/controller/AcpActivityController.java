package com.zx.acpactivity.controller;


import com.zx.acpactivity.entity.AcpActivity;
import com.zx.acpactivity.entity.AcpActivityNode;
import com.zx.acpactivity.entity.AcpActivityStatus;
import com.zx.acpactivity.service.AcpActivityService;
import com.zx.acpactivity.utils.CodeEnum;
import com.zx.acpactivity.utils.PageResult;
import com.zx.acpactivity.utils.Result;
import com.zx.acpactivity.vo.AcpActivityVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/acpActivity")
public class AcpActivityController {

    @Autowired
    AcpActivityService acpActivityService;


    /**
     * 发布活动/创建
     *
     * @return
     */
    @PostMapping("/release")
    public Result release(@RequestBody AcpActivityVO acpActivityVo) {
        try {
            if (acpActivityService.creat(acpActivityVo))
                return Result.succeed();
        } catch (Exception e) {
            e.printStackTrace();
            return Result.failed();
        }
        return new Result(CodeEnum.FAILED.getCode(), CodeEnum.FAILED.getMessage(), null);
    }

    /**
     * 活动审核
     * 上传数据：活动id(activityId)，审核理由(activityInfo)，审核人id(staffId)
     * @return
     */
    @PostMapping("/checked")
    public Result checked(@RequestBody AcpActivity acpActivity) {
        //获取UUID 构造邀请码
        if (acpActivityService.checked(acpActivity)) {
            return Result.succeed();
        }
        return Result.notFound();
    }

    @DeleteMapping("/deleteById/{id}")
    public Result deleteById(@PathVariable Long id) {
        //获取UUID 构造邀请码
        try {
            if (acpActivityService.deleteById(id))
                return Result.succeed();
        } catch (Exception e) {
            e.printStackTrace();
            return Result.failed();
        }
        return Result.notFound();
    }

    /**
     * 修改活动，包括用户
     *
     * @param acpActivityVo
     * @return
     */
    @PostMapping("/updateByVO")
    public Result updateByVO(@RequestBody AcpActivityVO acpActivityVo) {
        try {
            Result result = acpActivityService.updateByVo(acpActivityVo);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return Result.failed();
        }
    }

    @PostMapping("/update")
    public Result update(@RequestBody AcpActivity acpActivity) {
        if (acpActivityService.updateById(acpActivity)){
            /**
             * todo 修改活动短信通知参与人员
             */
            return Result.succeed();
        }
        return Result.notFound();
    }

    /**
     * 条件查询活动
     * 传入的参数应该有：活动状态activityStatus，活动公开私有状态showStatus，分页数据
     * 按接收的参数不同，可查询：
     * 1.最新公开未开始的前20个活动
     * 2.最新私有未开始的前20个活动
     * 3.正在进行的前20个活动
     * 4.已经结束的前20个活动
     *
     * @param acpActivityVO
     * @return
     */
    @PostMapping(value = "/findAllActivities")
    public PageResult<AcpActivity> findActivities(@RequestBody AcpActivityVO acpActivityVO, HttpServletRequest req) {
        Enumeration<String> parameterNames = req.getParameterNames();
        System.out.println( req.getRequestURI());
        return acpActivityService.findActivities(acpActivityVO);
    }
    /**
     * 根据活动查询其节点
     * 传入的参数应该有：活动Id  activityId
     */
    @GetMapping(value = "/findNodes/{activityId}")
    public Result<List<AcpActivityNode>> findNodesOfActivity(@PathVariable("activityId") Long activityId) {
        return acpActivityService.findNodesOfActivity(activityId);
    }
    /**
     * 按照id查询某个活动
     */
    @GetMapping(value = "getActivityById")
    public Result selectActivityById(Long id) {
        return acpActivityService.selectActivityById(id);
    }
    /**
     * 展示活动，不带节点,需要对活动进行条件查询，默认进行按时间排序
     * 条件：
     * 1. 公开，未公开
     * 2，是否在进行
     * 3，展示个数
     *
     * @param showStatus     1公开，0私有
     * @param activityStatus 0=>保存未提交审核，1=>提交待审核，2=>审核通过，发布状态，活动未开始，3=>审核未通过，4=>活动进行中，5=>活动以结束，6=>活动异常终止
     * @param limit          默认值20条
     * @return
     */
    @PostMapping("/acpDisplay")
    public Result<List<AcpActivity>> acpDisplay(@RequestParam(value = "showStatus", required = false, defaultValue = "1") Integer showStatus
            , @RequestParam(value = "activityStatus", required = false, defaultValue = "4") Integer activityStatus
            , @RequestParam(value = "limit", required = false, defaultValue = "20") Integer limit) {
        return acpActivityService.display(showStatus, activityStatus, limit);
    }

    /**
     * 展示活动带节点
     *
     * @param id
     * @return
     */
    @PostMapping("/getAcDetailById")
    public Result<AcpActivityVO> getAcDetailById(Long id) {
        return acpActivityService.getAcDetailById(id);
    }

    /**
     * 根据活动id进行活动下架,需要携带验证码进行验证
     *
     * @param id
     * @param code 验证码
     * @return
     */
    @PostMapping("/revokeByIdAndCode")
    public Result revokeByIdAndCode(Long id, String code) {
        try {
            Result result = acpActivityService.revokeByIdAndCode(id, code);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return Result.failed();
        }
    }

    /**
     * 获取下架验证码
     *
     * @param id
     * @return
     */
    @PostMapping("/getRevokeCodeById")
    public Result getRevokeCodeById(Long id) {
        return acpActivityService.getRevokeCodeById(id);
    }

    /**
     * 供给用户模块的接口
     * 用户查看自己发布的（type=0），参与的活动(type=1)
     *
     * @param userId
     * @param type
     * @return
     */
    @GetMapping(value = "/findMyActivities")
    public Result<List<AcpActivity>> findMyActivities(Long userId, Integer type) {
        //先判断用户的查询类型，发布/参与
        if (type == 0) {
            //自己发布的（type=0）
            return Result.succeed(acpActivityService.myPolishedActivities(userId));
        } else {
            //参与的活动(type=1)
            return Result.succeed(acpActivityService.myEngagedActivities(userId));
        }
    }

    /**
     * 用户根据用户id与活动id参加活动,邀请码可选
     *
     * @param userId
     * @param id
     * @return
     */
    @PostMapping("/joinAcpByIdAndUserIdAndCode")
    public Result joinAcpByIdAndUserIdAndCode(Long userId, Long id, String code) {
        return acpActivityService.joinAcpByIdAndUserId(userId, id, code);
    }

/**
 * 统计活动不同状态的次数
 * */

    @GetMapping("/countStatus")
    public Result countStatus(){
        Map<String, List> map = new HashMap<>();
        List<String>statusList = new ArrayList<>();
        List<Integer>countList = new ArrayList<>();
        List<AcpActivityStatus> countStatusList = acpActivityService.countStatus();
        countStatusList.forEach(countStatus -> {
            statusList.add(countStatus.getStatus());
            countList.add(countStatus.getCount());
        });
        map.put("xAxisList", statusList);
        map.put("yAxisList", countList);
        return Result.succeed(map);
    }
    /**
     * 导入活动成员列表，文件暂时缓存于本地服务器
     * @param file
     * @return
     */
    @PostMapping("/uploadNumberExcel")
     public Result<String> uploadNumberExcel(MultipartFile file){
        String url = acpActivityService.uploadNumberExcel(file);
        if (null!=url){
            Result<String> result = Result.succeed();
            result.setData(url);
            return result;
        }
        return Result.notFound();
    }

    /**
     * 发布活动，带有excel上传的人员名单，暂时是用户名与电话
     * @param acpActivityVO
     * @return
     */
    @PostMapping("/releaseWithExcel")
    public Result releaseWithExcel(@RequestBody AcpActivityVO acpActivityVO){
        return acpActivityService.releaseWithExcel(acpActivityVO);
    }
    /**
     * 根据idPath查询活动
     */
    @GetMapping("/findByIdPath")
    public Result<List<AcpActivity>> findByIdPath(@RequestParam String idPath){
        return acpActivityService.findByIdPath(idPath);
    }
}

