package com.zx.acpactivity.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zx.acpactivity.entity.AcpActivityNodeType;
import com.zx.acpactivity.entity.FormSelectTree;
import com.zx.acpactivity.service.AcpActivityNodeTypeService;
import com.zx.acpactivity.utils.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/acpActivityNodeType")
public class AcpActivityNodeTypeController {
    @Resource
    private AcpActivityNodeTypeService acpActivityNodeTypeService;

    @GetMapping("/findAll")
    public Result<Object> findAll() {
        return Result.succeed(acpActivityNodeTypeService.list());
    }

    @GetMapping("/findTypeTree")
    public List<FormSelectTree> findTypeTree() {
        return acpActivityNodeTypeService.findTypeTree();
    }

    @PostMapping("/save")
    public Result<Object> save(@RequestBody AcpActivityNodeType acpActivityNodeType) {
        if (acpActivityNodeTypeService.mySaveOrUpDate(acpActivityNodeType))
            return Result.succeed();
        else
            return Result.failed();
    }

    @DeleteMapping("/{id}")
    public Result<Object> delete(@PathVariable Long id) {
        if (acpActivityNodeTypeService.deleteById(id))
            return Result.succeed();
        else
            return Result.failed();
    }

    @GetMapping("/{level}")
    public Result<List<AcpActivityNodeType>> getType(@PathVariable Integer level) {
        return Result.succeed(acpActivityNodeTypeService.list(new QueryWrapper<AcpActivityNodeType>().eq("level", level)));
    }


}

