package com.zx.acpactivity.controller;


import com.zx.acpactivity.entity.AcpActivityType;
import com.zx.acpactivity.entity.FormSelectTree;
import com.zx.acpactivity.service.AcpActivityTypeService;
import com.zx.acpactivity.utils.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/acpActivityType")
public class AcpActivityTypeController {

    @Resource
    private AcpActivityTypeService acpActivityTypeService;

    @GetMapping("/findAll")
    public Result<List<AcpActivityType>> findAll() {
        List<AcpActivityType> list = acpActivityTypeService.list();
        return Result.succeed(list);
    }
    /**
     * 查询所有父类型活动
     * @return
     */
    @GetMapping("/findParentType")
    public Result<List<AcpActivityType>> findParentType(){
        List<AcpActivityType> list = acpActivityTypeService.findByLevel();
        return Result.succeed(list);
    }

    @PostMapping("/saveOrUpdate")
    public Result<AcpActivityType> saveOrUpdate(@RequestBody AcpActivityType acpActivityType) {
        acpActivityTypeService.mySaveOrUpDate(acpActivityType);
        return Result.succeed();
    }

    @DeleteMapping("/{id}")
    public Result<AcpActivityType> delete(@PathVariable Long id) {
        acpActivityTypeService.deleteById(id);
        return Result.succeed();
    }

    @GetMapping("/findTypeTree")
    public List<FormSelectTree> findTypeTree() {
        return acpActivityTypeService.findTypeTree();
    }
}

