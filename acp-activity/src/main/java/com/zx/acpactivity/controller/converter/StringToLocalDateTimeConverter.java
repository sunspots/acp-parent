package com.zx.acpactivity.controller.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import java.time.LocalDateTime;

@Component
public class StringToLocalDateTimeConverter implements Converter<String, LocalDateTime> {
    @Override
    public LocalDateTime convert(String source) {
        try {
            return LocalDateTime.parse(source);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}