package com.zx.acpactivity.controller;


import com.zx.acpactivity.entity.AcpActivityNodeTemplate;
import com.zx.acpactivity.entity.AcpActivityTemplate;
import com.zx.acpactivity.service.AcpActivityTemplateService;
import com.zx.acpactivity.utils.PageResult;
import com.zx.acpactivity.utils.Result;
import com.zx.acpactivity.vo.AcpActivityTemplateVO;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/acpActivityTemplate")
public class AcpActivityTemplateController {

    @Resource
    private AcpActivityTemplateService activityTemplateService;

    @GetMapping("/findActTemps")
    public PageResult<AcpActivityTemplate> findActTemps(AcpActivityTemplateVO activityTemplateVO) {
        return activityTemplateService.findActTemps(activityTemplateVO);
    }

    @PostMapping("/add")
    public Result<AcpActivityTemplate> addActTemp(@RequestBody AcpActivityTemplateVO activityTemplateVO) {
        activityTemplateService.addActTemp(activityTemplateVO);
        return Result.succeed();
    }

    @PutMapping("/update")
    public Result<AcpActivityTemplate> updateActTemp(@RequestBody AcpActivityTemplateVO activityTemplateVO) {
        activityTemplateService.updateActTemp(activityTemplateVO);
        return Result.succeed();
    }

    @DeleteMapping("{id}")
    public Result<AcpActivityTemplate> deleteActTemp(@PathVariable Long id) {
        activityTemplateService.deleteActTempById(id);
        return Result.succeed();
    }

    @GetMapping("/activityTemplateId/{id}")
    public Result<List<AcpActivityNodeTemplate>> getNodeTemplateByActTempById(@PathVariable Long id) {
        return activityTemplateService.getNodeTemplateByActTempById(id);
    }

    @PutMapping("/review")
    public Result<AcpActivityTemplate> reviewActTemp(@RequestBody AcpActivityTemplate acpActivityTemplate) {
        activityTemplateService.updateById(acpActivityTemplate);
        return Result.succeed();
    }
}

