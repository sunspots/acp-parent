package com.zx.acpactivity.controller;


import com.zx.acpactivity.service.AcpActivityNextNodeService;
import com.zx.acpactivity.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/acpActivityNextNode")
public class AcpActivityNextNodeController {

    @Autowired
    private AcpActivityNextNodeService acpActivityNextTemplateService;

    @PostMapping("/save")
    public Result<Object> save(@RequestParam Long nodeTypeId, @RequestBody List<Long> nextNodeTypeIds) {
        if (acpActivityNextTemplateService.mySave(nodeTypeId, nextNodeTypeIds))
            return Result.succeed();
        else
            return Result.failed();
    }

    @GetMapping("/findByNodeTypeId")
    public Result<Object> findByNodeTypeId(@RequestParam Long nodeTypeId) {
        return Result.succeed(acpActivityNextTemplateService.findByNodeTypeId(nodeTypeId));
    }


}

