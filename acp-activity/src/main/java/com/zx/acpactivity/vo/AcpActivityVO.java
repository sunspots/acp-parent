package com.zx.acpactivity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zx.acpactivity.entity.AcpActivityNode;
import com.zx.acpactivity.entity.AcpActivityPublic;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * com.zx.acpactivity.vo
 * name：AcpActivityVo
 * user：pengpeng
 * date:2020/10/14
 * time:14:17
 */
@Data
public class AcpActivityVO extends AcpActivityPublic<AcpActivityVO> {
    //导入活动成员的excel文件在服务器的路径
    private String excelPath;
    //活动节点信息
    private List<AcpActivityNode> nodeList;
    //页数
    private Integer page;
    //每页数量
    private Integer limit;
    //最少活动人数
    private Integer minNum;
    //最多活动人数
    private Integer maxNum;
    //最早活动创建时间
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime earlyCreateTime;
    //最晚活动创建时间
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lateCreateTime;
    //最早活动结束时间
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime earlyEndTime;
    //最晚活动结束时间
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lateEndTime;
}
