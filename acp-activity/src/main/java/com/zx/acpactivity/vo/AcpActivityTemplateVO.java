package com.zx.acpactivity.vo;

import com.zx.acpactivity.entity.AcpActivityNodeTemplate;
import com.zx.acpactivity.entity.AcpActivityTemplate;
import lombok.Data;

import java.util.List;

@Data
public class AcpActivityTemplateVO extends AcpActivityTemplate {
    //页数
    private Integer page;
    //每页数量
    private Integer limit;
    //活动节点列表
    private List<AcpActivityNodeTemplate> nodeList;
}
